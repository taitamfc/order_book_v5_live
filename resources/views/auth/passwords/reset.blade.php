@extends('frontend.includes.student-layout')

@section('title', 'Wachtwoord vergeten')

@section('header')
    @include('frontend.includes.components.header')
@endsection

@section('content')
<div class="top-line">
    <div class="container">
        <div class="row">
            <div class="top-content-login">
                <div class="col-lg-5 login-top-text">
                    <h3>
                        Welkom op de boekenbestelsite van <br>
                        AERES MBO Barneveld in samenwerking <br>
                        met Totdrukwerk.
                    </h3>
                    </p>
                    <p>Via deze bestelsite kun je de benodigde lesbundels, (commerciële) boeken en eventuele keuzepakketten bestellen. Ook dit jaar hebben <br>we weer leuke schoolbenodigdheden toegevoegd aan de bestelsite. <br>Deze vind je terug onder de kantoorartikelen.
                    </p>
                    <p>Je boekenbestelling wordt bij je thuis afgeleverd. <br><b>Wel zo gemakkelijk!</b></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main inloggen-content" id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-6 box-left">
                <div class="panel panel-custom">
                    <div class="block-title"><h2>Wachtwoord Opnieuw Instellen</h2></div>
                    <div class="panel-body">
                        <form method="POST" action="{{ route('password.update') }}">
                            @csrf
                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="form-group">
                                <label for="label_name" class="control-label black">E-Mail:</label>
                                <input id="email" type="email" class="form-control form-lg @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert" style="color:red;font-weight: 500!important">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="label_name" class="control-label black">Wachtwoord:</label>
                                <input id="password" type="password" class="form-control form-lg @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert" style="color:red;font-weight: 500!important">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="label_name" class="control-label black">Bevestig Wachtwoord:</label>
                                <input id="password-confirm" type="password" class="form-control form-lg" name="password_confirmation" required autocomplete="new-password">
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-lg">
                                        <button type="submit" class="btn btn-primary">
                                            Wachtwoord Opnieuw Instellen
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6 box-right">
                <div class="panel panel-custom">
                    <div class="block-title"><h2>Actueel</h2></div>
                    <div class="panel-body panel-content panel-content-other">
                        <p>
                            <b>BESTELSITE AERES MBO Barneveld</b>
                            <br>
                            De laatste bestellingen worden geproduceerd en z.s.m. verzonden.
                        </p>
                        <address>
                            Zoals u zelf al gemerkt heeft zijn er problemen met de levering van de boeken Biologie. Mondjesmaat ontvangen wij van de uitgeverij een aantal boeken maar helaas niet de hoeveelheid die wij in bestelling hebben.
                        </address>
                        <address>
                            De uitgeverij heeft in eerste instantie aangegeven dat de boeken vandaag bij ons zouden binnenkomen maar vanmorgen is dit bijgesteld naar 29 september aanstaande.
                        </address>
                        <address>
                            Wij hebben vrijwel dagelijks contact met de uitgever om de druk erop te houden maar realiseren ons dat ook wij volledig afhankelijk van hun zijn.
                        </address>
                        <br>
                        <address>
                            Op school hebben wij melding gemaakt van de leveringsproblemen zodat ook de leraren op de hoogte zijn.
                        </address>
                        <address>
                            Wij zullen er alles aan doen om de boeken zo spoedig mogelijk bij u te leveren.
                        </address>
                        <address>
                            Excuus voor het ongemak, ook wij vinden het uiterst vervelend.
                        </address>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="blocks">
                <div class="col-md-4 form-group form-lg">
                    <a href="#">
                        <img src="/assets_fe/img/iPad-winactie.png" class="img-responsive img-bordered" alt="">
                    </a>
                </div>
                <div class="col-md-4 form-group form-lg">
                    <a href="#">
                        <img src="/assets_fe/img/login-2.png" class="img-responsive" alt="">
                    </a>
                </div>
                <div class="col-md-4 form-group form-lg">
                    <a href="#">
                        <img src="/assets_fe/img/login-3.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')


@endpush