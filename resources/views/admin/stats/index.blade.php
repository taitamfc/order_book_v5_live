@extends('admin.layouts.app')

@section('content')

    <div class="main" id="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-custom">
                        <div class="panel-heading">
                            <div class="title">
                                <h3>Statistieken</h3>
                            </div>
                        </div>
                        <div class="panel-body">
                            {!! Form::open(['route' => 'stats.index','method' => 'GET']) !!}
                                <div class="row row10">
                                    <div class="col-md-3 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                        <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control" name="date_from" value="{{ isset($_GET['date_from']) ? $_GET['date_from'] : '01/01/'.date('Y') }}">
                                            <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                        <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control" name="date_until" value="{{ isset($_GET['date_until']) ? $_GET['date_until'] : '31/12/'.date('Y') }}">
                                            <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
                                        </div>
                                    </div>

                                    <input type="submit" value="OPSLAAN" class="btn btnorangeTransp btn-sm">
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="chartValues" style="display:none">
                                            <span class="ordersSent">{!! $ordersSent !!}</span>
                                            <span class="ordersNotSent">{!! $ordersNotSent !!}</span>
                                            <span class="ordersNotInBatch">{!! $ordersNotInBatch !!}</span>
                                        </div>
                                        <div id="chartdiv"></div>
                                    </div>
                                    <div class="col-lg-6">
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <span>Totaal aantal bestellingen:</span>
                                                <br>
                                                <span class="black medium-font">{!! $ordersAmount !!}</span>
                                            </li>
                                            <li class="list-group-item">
                                                <span>Studenten nog geen bestelling gedaan:</span>
                                                <br>
                                                <span class="black medium-font">{!! $studentsWithoutOrder !!}</span>
                                            </li>
                                            <li class="list-group-item">
                                                <span>Gemiddeld bedrag per bestelling:</span>
                                                <br>
                                                <span class="black medium-font">€ {!! \App\Helpers\Custom::money_format(round($averagePerOrder[0]->orders_avg,2)) !!}</span>
                                            </li>
                                            <li class="list-group-item">
                                                <span>Totaal bedrag uit bestellingen:</span>
                                                <br>
                                                <span class="black medium-font">€ {!! \App\Helpers\Custom::money_format($ordersSum) !!}</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="row row10">

                                    <div class="col-md-3 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                        {!! Form::select('product',$products->pluck('name','product_id'),isset($_GET['product']) != '' ? $_GET['product'] : null,['class' => 'form-control jquerySelect','placeholder' => 'Product ..']) !!}
                                    </div>
                                    <div class="col-md-3 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                        <select class="form-control jquerySelect" name="provider">
                                            <option selected value>Leverancier ..</option>
                                            @foreach($providers as $provider)
                                                @if($provider->provider != "")
                                                    <option value="{!! $provider->provider !!}" @if(isset($_GET['provider']) && $_GET['provider'] == $provider->provider) selected @endif>{!! $provider->provider !!}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <input type="submit" value="OPSLAAN" class="btn btnorangeTransp btn-sm">

                                </div>
                            {!! Form::close() !!}

                            <div class="table-responsive">

                                @if(!empty($productsForTable))
                                    <table class="table table-custom small-font">
                                        <tr>
                                            <th>ID</th>
                                            <th>Product</th>
                                            <th>Categorie</th>
                                            <th>Leverancier</th>
                                            <th class="td-nowrap">Aantal besteld</th>
                                            <th class="td-nowrap">Totale opbrengst</th>
                                        </tr>

                                        @foreach($productsForTable as $product)
                                            <tr>
                                                <td>{!! $product->product_id !!}</td>
                                                <td class="td-nowrap">{!! $product->name !!}</td>
                                                <td class="td-nowrap">{!! $product->category_id != '0' ? $product->category->name : '' !!}</td>
                                                <td><a href="#" class="link">{!! $product->provider !!}</a></td>
                                                <td class="text-xs-right">{{ $product->total_items }}</td>
                                                <td class="text-xs-right">€ {{ \App\Helpers\Custom::money_format($product->total_sum) }}</td>
                                            </tr>
                                        @endforeach

                                    </table>
                                @endif

                            </div>
                        </div>
                    </div><!-- .panel -->
                </div>
            </div>
        </div>
    </div><!-- end .main -->

@endsection