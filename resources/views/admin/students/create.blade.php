@extends('admin.layouts.app')

@section('content')
    <div class="main" id="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="block-title">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2>Nieuwe student</h2>
                            </div>
                            <div class="col-sm-6 text-sm-right">
                                {{--<a href="#" class="btn btn-success">TERUG</a>--}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @if(Session::has('success'))
                                <div class="alert alert-success">{{Session::get('success')}}</div>
                            @endif
                            @if(Session::has('error'))
                                <div class="alert alert-danger">{{Session::get('error')}}</div>
                            @endif
                        </div>
                    </div>
                    {!! Form::open(['route' => 'studenten.store','method' => 'POST']) !!}
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="panel panel-custom">
                                    <div class="panel-heading">
                                        <div class="title">
                                            <h3>STUDENT</h3>
                                        </div>
                                    </div>
                                    <div class="collapse in" id="collapsePanel1">
                                        <div class="panel-body">
                                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                                <label for="label_name" class="control-label">Student Nummer:</label>
                                                <input type="text" class="form-control" name="username" value="{{old('username')}}">
                                                @if ($errors->has('username'))
                                                    <span class="text-warning">
                                                        <strong>{{ $errors->first('username') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                                                <label for="label_name" class="control-label">Voornaam:</label>
                                                <input type="text" class="form-control" name="firstname" value="{{old('firstname')}}">
                                                @if ($errors->has('firstname'))
                                                    <span class="text-warning">
                                                        <strong>{{ $errors->first('firstname') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('middlename') ? ' has-error' : '' }}">
                                                <label for="label_name" class="control-label">Tussenvoegsel:</label>
                                                <input type="text" class="form-control" name="middlename" value="{{old('middlename')}}">
                                                @if ($errors->has('middlename'))
                                                    <span class="text-warning">
                                                        <strong>{{ $errors->first('middlename') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                                                <label for="label_name" class="control-label">Achternaam:</label>
                                                <input type="text" class="form-control" name="lastname" value="{{old('lastname')}}">
                                                @if ($errors->has('lastname'))
                                                    <span class="text-warning">
                                                        <strong>{{ $errors->first('lastname') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label for="label_name" class="control-label">Wachtwoord:</label>
                                                <input type="password" class="form-control" name="password">
                                                @if ($errors->has('password'))
                                                    <span class="text-warning">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                                <label for="label_name" class="control-label">Wachtwoord herhalen:</label>
                                                <input type="password" class="form-control" name="password_confirmation">
                                                @if ($errors->has('password'))
                                                    <span class="text-warning">
                                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- .panel -->
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-custom">
                                    <div class="panel-heading">
                                        <div class="title">
                                            <h3>PERSOONLIJKE INFORMATIE</h3>
                                        </div>
                                    </div>
                                    <div class="collapse in" id="collapsePanel2">
                                        <div class="panel-body">
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="label_name" class="control-label">E-mailadres:</label>
                                                <input type="email" class="form-control" name="email" value="{{old('email')}}">
                                                @if ($errors->has('email'))
                                                    <span class="text-warning">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
                                                <label for="label_name" class="control-label">Telefoonnummer:</label>
                                                <input type="text" class="form-control" name="telephone" value="{{old('telephone')}}">
                                                @if ($errors->has('telephone'))
                                                    <span class="text-warning">
                                                        <strong>{{ $errors->first('telephone') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('group_id') ? ' has-error' : '' }}">
                                                <label for="label_name" class="control-label">Groep (Klas):</label>
                                                <select name="group_id" class="form-control filterSelect">
                                                    @foreach($groups as $group)
                                                        <option value="{{$group->customer_group_id}}">{{$group->customer_group_code}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('group_id'))
                                                    <span class="text-warning">
                                                        <strong>{{ $errors->first('group_id') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- .panel -->
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-custom">
                                    <div class="panel-heading">
                                        <div class="title">
                                            <h3>ADRES</h3>
                                        </div>
                                    </div>
                                    <div class="collapse in" id="collapsePanel3">
                                        <div class="panel-body">
                                            <div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                                                <label for="label_name" class="control-label">Adres:</label>
                                                <input type="text" class="form-control" name="street" value="{{old('street')}}">
                                                @if ($errors->has('street'))
                                                    <span class="text-warning">
                                                        <strong>{{ $errors->first('street') }}</strong>
                                                    </span>
                                                @endif
                                            </div>

                                            <div class="form-group{{ $errors->has('postcode') ? ' has-error' : '' }}">
                                                <label for="label_name" class="control-label">Postcode:</label>
                                                <input type="text" class="form-control" name="postcode" value="{{old('postcode')}}">
                                                @if ($errors->has('postcode'))
                                                    <span class="text-warning">
                                                        <strong>{{ $errors->first('postcode') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                                <label for="label_name" class="control-label">Plaats:</label>
                                                <input type="text" class="form-control" name="city" value="{{old('city')}}">
                                                @if ($errors->has('city'))
                                                    <span class="text-warning">
                                                        <strong>{{ $errors->first('city') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- .panel -->
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right">Opslaan</button>
                            </div>
                        </div>


                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div><!-- end .main -->
@endsection