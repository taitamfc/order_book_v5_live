@extends('admin.layouts.app')

@section('content')


    <div class="main" id="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-custom">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="title">
                                        <h3>Studenten</h3>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    @if(Auth::guard('admin')->user()->isAdmin())
                                        <a href="{{URL::route('studenten.create')}}" class="btn btn-info btn-xxs-block pull-right">Nieuwe student</a>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <div class="panel-body">
                            <form action="{!! URL::route('studenten.index') !!}" method="GET" id="searchFormStudents">
                                <div class="row row10">
                                    <div class="col-lg-1 col-sm-4 col-xs-2 col-xxs-12 form-group">
                                        <input type="text" class="form-control brdRadius" placeholder="ID .." name="student_id" @if(isset($_GET['student_id']) && $_GET['student_id'] != "") value=" {!! $_GET['student_id'] !!}" @endif>
                                    </div>
                                    <div class="col-lg-1 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                        <input name="student_name" type="text" class="form-control brdRadius" placeholder="Naam .." @if(isset($_GET['student_name']) && $_GET['student_name'] != "") value=" {!! $_GET['student_name'] !!}" @endif>
                                    </div>
                                    <div class="col-lg-2 col-sm-4 col-xs-4 col-xxs-12 form-group">
                                        <select class="form-control jquerySelect filterSelect" name="group">
                                            <option value="null">Groep..</option>
                                            @foreach($groups as $group)
                                                <option value="{!! $group->customer_group_id !!}" @if(isset($_GET['group']) && $_GET['group']!= 'null' && $group->customer_group_id == $_GET['group']) selected @endif>{!! $group->customer_group_code !!}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                    <div class="col-lg-5 col-sm-8 col-xs-12 form-group">
                                        <select class="form-control jquerySelect filterSelect" name="product">
                                            <option value="null">Heeft het volgende product in zijn / haar bestelling ..</option>
                                            @foreach($products as $product)
                                                <option value="{!! $product->product_id !!}" @if(isset($_GET['product']) && $_GET['product']!= 'null' && $product->product_id == $_GET['product']) selected @endif>{!! $product->name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-2 col-sm-4 col-xs-12 form-group">
                                        <select class="form-control filterSelect" name="have_orders">
                                            <option value="null" @if(isset($_GET['have_orders']) && $_GET['have_orders'] == 'null') selected @endif>Besteld? ..</option>
                                            <option value="1" @if(isset($_GET['have_orders']) && $_GET['have_orders'] == '1') selected @endif>Ja</option>
                                            <option value="0" @if(isset($_GET['have_orders']) && $_GET['have_orders'] == '0') selected @endif>Nee</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-1 col-xxs-12 form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <a href="{{ route('studenten.index') }}"><span class="glyphicon glyphicon-refresh"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" style="position: absolute;left: -9999px;"></button>
                            </form>
                                <div class="table-responsive">
                                    <table class="table table-custom">
                                        <tr>
                                            <th>ID</th>
                                            <th>Naam</th>
                                            <th class="td-nowrap">Groep (Klas)</th>
                                            <th>E-mail</th>
                                            <th>Telefoon</th>
                                            <th class="td-nowrap">Bestelling gedaan?</th>
                                            <th class="th-flex">Actie</th>
                                        </tr>
                                        @forelse($students as $student)

                                            <tr>
                                                <td>{!! $student->username !!}</td>
                                                <td class="td-nowrap">{!! $student->firstname !!} {!! $student->lastname !!}</td>
                                                <td>{!! $student->group->customer_group_code !!}</td>
                                                <td>{!! $student->email !!}</td>
                                                <td class="td-nowrap">{!! $student->telephone !!}</td>
                                                <td>{!! (count($student->order_count)) ? 'Ja' : 'Nee' !!}</td>
                                                <td>
                                                    <a href="{!! URL::route('studenten.show',$student->customer_id) !!}" class="txt-orange">Bekijken</a>
                                                </td>
                                            </tr>

                                        @empty
                                            <tr>
                                                <h3 style="text-align: center">Geen studenten</h3>
                                            </tr>
                                        @endforelse


                                    </table>

                                    <div class="pagination">
                                        {!! $students->appends($filter)->render() !!}
                                    </div>
                                </div>

                        </div>
                    </div><!-- .panel -->
                </div>
            </div>
        </div>
    </div><!-- end .main -->

@endsection