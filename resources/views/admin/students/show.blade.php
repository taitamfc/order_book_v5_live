@extends('admin.layouts.app')

@section('content')
<div class="main" id="main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="block-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>{!! $student->firstname !!} {!! $student->lastname !!}</h2>
                        </div>
                        <div class="col-sm-6 text-sm-right">
                            {{--<a href="#" class="btn btn-success">TERUG</a>--}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{Session::get('success')}}</div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">{{Session::get('error')}}</div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        {!! Form::open(['route' => ['studenten.update', $student->customer_id], 'method' => 'POST']) !!}
                        
                        <div class="col-md-4">
                        <div class="panel panel-custom">
                            <div class="panel-heading">
                                <div class="title">
                                    <h3>STUDENT</h3>
                                </div>
                                <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel1" aria-expanded="true" aria-controls="collapsePanel1"></span>
                            </div>
                            <div class="collapse in" id="collapsePanel1">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="label_name" class="control-label">Student Nummer:</label>
                                        <input type="text" class="form-control" value="{!! $student->username !!}" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label">Voornaam:</label>
                                        <input type="text" class="form-control" value="{!! $student->firstname !!}" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label">Tussenvoegsel:</label>
                                        <input type="text" class="form-control" value="{!! $student->middlename !!}" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label">Achternaam:</label>
                                        <input type="text" class="form-control" value="{!! $student->lastname !!}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div><!-- .panel -->
                        </div>
                        <div class="col-md-4">
                        <div class="panel panel-custom">
                            <div class="panel-heading">
                                <div class="title">
                                    <h3>PERSOONLIJKE INFORMATIE</h3>
                                </div>
                                <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel2" aria-expanded="true" aria-controls="collapsePanel2"></span>
                            </div>
                            <div class="collapse in" id="collapsePanel2">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="label_name" class="control-label">E-mailadres:</label>
                                        <input type="email" class="form-control" value="{!! $student->email !!}" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label">Telefoonnummer:</label>
                                        <input type="tel" class="form-control" value="{!! $student->telephone !!}" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label">Groep (Klas):</label>
                                        @if(Auth::guard('admin')->user()->isAdmin())
                                            <select name="group_id" class="form-control filterSelect">
                                                @foreach($groups as $group)
                                                    <option value="{{$group->customer_group_id}}" @if(!is_null($student->group) && $group->customer_group_id == $student->group->customer_group_id) selected @endif>{{$group->customer_group_code}}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <input type="text" class="form-control" value="{!! $student->group->customer_group_code !!}" disabled>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label">Bankrekeningnummer:</label>
                                        <input type="tel" class="form-control" value="{!! $student->bank_account !!}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div><!-- .panel -->
                        </div>
                        <div class="col-md-4">
                        <div class="panel panel-custom">
                            <div class="panel-heading">
                                <div class="title">
                                    <h3>ADRES</h3>
                                </div>
                                <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel3" aria-expanded="true" aria-controls="collapsePanel3"></span>
                            </div>
                            <div class="collapse in" id="collapsePanel3">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="label_name" class="control-label">Straat:</label>
                                        <input type="text" class="form-control" value="{!! $student->street !!}" disabled>
                                    </div>

                                    <div class="form-group">
                                        <label for="label_name" class="control-label">Postcode:</label>
                                        <input type="text" class="form-control" value="{!! $student->postcode !!}" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label">Plaats:</label>
                                        <input type="text" class="form-control" value="{!! $student->city !!}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div><!-- .panel -->
                            @if(Auth::guard('admin')->user()->isAdmin())
                                <button type="submit" class="btn btn-primary pull-right">Opslaan</button>
                            @endif
                        </div>
                        {!! Form::close() !!}


                        <div class="col-xs-12">
                            <div class="panel panel-custom">
                                <div class="panel-heading">
                                    <div class="title">
                                        <h3>Bestellingen</h3>
                                    </div>
                                </div>
                                <div class="panel-body">

                                    @if($student->orders->count() != 0)
                                        @foreach($orders_categorized as $increment_id => $categories)
                                            @foreach($categories as $category => $order_itemss)
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">{{ $category }}</div>
                                                    <div class="panel-body">

                                                        <div class="table-responsive">
                                                            <table class="table table-custom">
                                                                <tr>
                                                                    <th>ID</th>
                                                                    <th>Titel</th>
                                                                    <th>Status</th>
                                                                    <th class="align-r" style="text-align: left;width:10%">Prijs</th>
                                                                </tr>
                                                                @foreach($order_itemss as $item)
                                                                    <tr>
                                                                        <td>{!! $item->order_item_id !!}</td>
                                                                        <td style="width: 70%;">{!! $item->name !!}</td>
                                                                        <td>{!! $item->all_shipped ? 'Verzonden' : 'Niet verzonden' !!}</td>
                                                                        <td class="align-l" style="text-align: left;width:10%">&euro; {!! \App\Helpers\Custom::money_format($item->price) !!}</td>
                                                                    </tr>
                                                                @endforeach
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endforeach
                                    @else
                                        <h4 style="text-align: center">Er is nog geen bestelling geplaatst.</h4>
                                    @endif
                                </div>
                            </div><!-- .panel -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- end .main -->
@endsection