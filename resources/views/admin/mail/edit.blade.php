@extends('admin.layouts.app')

@section('content')

    <div class="main" id="main">
        <div class="container">
            @include('flash::message')
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-custom">

                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-5 col-xxs-12 text-center-xxs">
                                        <div class="title">
                                            <h3>Mail bewerken</h3>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="panel-body">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                {!! Form::open(['route' => 'mailUpdate','method' => 'POST']) !!}
                                    <div class="form-group">
                                        <label for="label_name" class="control-label">Inhoud:</label>
                                        <textarea class="form-control editor" name="content" rows="10">{{ $mail->content }}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="label_name" class="control-label">Verstuur datum:</label>
                                        <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control" value="{{ $mail->send_date }}" name="send_date">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <input type="submit" class="btn btnorangeTransp btn-sm" value="Opslaan">

                                {!! Form::close() !!}

                            </div>
                        </form>
                    </div><!-- .panel -->
                </div>
            </div>
        </div>
    </div><!-- end .main -->
@endsection