 @extends('admin.layouts.app')

 @section('content')
    	<div class="main" id="main">
    		<div class="container">
    			<div class="row">
    				<div class="col-xs-12">
						@include('flash::message')
                        <div class="panel panel-custom">
                            <div class="panel-heading">
								<div class="row">
									<div class="col-xs-5 col-xxs-12 text-center-xxs">
										<div class="title">
											<h3>Batches</h3>
										</div>
									</div>
									<div class="col-xs-7 col-xxs-12 text-xs-right">
										{!! Form::open(['route' => ['batches.sendEmailToFactory'],'method' => 'POST']) !!}
											<input type="text" hidden id="batch_ids" name="batch_ids" value="" required>
											<button type="submit" class="btn btn-info btn-xxs-block create-batch text-uppercase">Stuur E-mail Naar Uitgever</button>
										{!! Form::close() !!}
									</div>
								</div>
                            </div>
                            <div class="panel-body">
                                <form action="{!! URL::route('batches.index') !!}" method="get">
                                    <div class="row row10">
                                        <div class="col-lg-2 col-sm-4 col-xs-6 col-xxs-12 form-group">
											<div class="input-group">
                                                <div class="input-group-addon check">
                                                    <label class="checkbox-label empty">
                                                        <input type="checkbox" class="styled checkbox-custom checkAll" >
                                                    </label>
                                                </div>
                                                {!! Form::text('id', Request::input('id'),['placeholder' => 'ID ..', 'class'=> 'form-control']) !!}
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-sm-4 col-xs-5 col-xxs-12 form-group">
                                        {!! Form::select('is_complete',['' => 'Compleet?','0' => 'Nee', 1=> 'Ja'], Request::input('is_complete'), ['class' => 'form-control','onchange' => 'this.form.submit()']) !!}
                                        </div>
										<div class="col-xs-1 col-xxs-12 form-group">
											<div class="input-group">
												<div class="input-group-addon">
													<a href="{{ route('batches.index') }}"><span class="glyphicon glyphicon-refresh"></span></a>
												</div>
											</div>
										</div>
                                    </div>
                                    @if ($batches)
	                                    <div class="table-responsive">
	                                        <table class="table table-custom">
	                                            <tr>
													<th class="th-flex">&nbsp;</th>
	                                                <th>ID</th>
	                                                <th>Aantal bestellingen</th>
													<th>Status</th>
													<th>Email verzonden</th>
	                                                <th class="th-flex">Actie</th>
	                                            </tr>
												@foreach($batches as $batch)
													@php
														if($batch->is_send_email_factory){
															$batch->is_send_email_factory = "Ja";
														}
														else{
															$batch->is_send_email_factory = "Nee";
														}
													@endphp
		                                            <tr>
														<td>
															<label class="checkbox-label empty">
																<input type="checkbox" class="styled checkbox-custom batchId" name="batch_ids[]" value="{{ $batch->batch_id }}">
															</label>
														</td>
		                                                <td>{{ $batch->batch_id }}</td>
		                                                <td>{{ $batch->orders->count() }}</td>
		                                                <td class="td-nowrap"><span class="medium">{{ $batch->batchProducts->sum('qty_available') }}</span> binnen van de <span class="medium">{{  $batch->items->sum('qty_ordered') }}</span> totaal.</td>
														<td>{{$batch->is_send_email_factory}}</td>
														<td class="td-nowrap">
		                                                    <a href="{!! URL::route('batches.edit',[$batch->batch_id]) !!}" class=" txt-orange ">Bekijken</a>
		                                                </td>
		                                            </tr>
	                                            @endforeach

	                                        </table>
                                           
	                                    </div>
                                        {{ $batches->appends(Request::except('page'))->links() }}
                                    @endif
                                </form>
                            </div>
                        </div><!-- .panel -->
                    </div>
                </div>
    		</div>
    	</div><!-- end .main -->
@endsection    	

@push('scripts')
	<script>
		var batchIds = $("#batch_ids");
		
		var getbatchIds = function(){
			var lstBatchId = [];
			$(".batchId").each(function(){
				if($(this).is(":checked")){
					lstBatchId.push($(this).val());
				}
				var sData = lstBatchId.join();
                batchIds.val(sData);
			});
		}
		
		$(".checkAll").change(function() {
			if($(this).is(":checked")) {
				$('.batchId').attr('checked',true).trigger('refresh');
			}else
			{
				$('.batchId').attr('checked',false).trigger('refresh');
			}
			getbatchIds();
    	});

		$('.batchId').change(function(){
			getbatchIds();
		});




	</script>
@endpush