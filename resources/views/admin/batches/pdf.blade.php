<table>
    <tr>
        <td><strong>Batch ID:</strong> {{ $batch->batch_id }}</td>
        <td width="20%">&nbsp;</td>
    </tr>
</table>
<br>
{{--<br>
<br>
@if ($orders)
    <table width="100%">
        <tr>
            <th align="left">ID</th>
            <th align="left">Studentcode</th>
            <th align="left">Naam</th>
            <th align="left">Groep</th>
            <th align="left">Besteldatum</th>
        </tr>
        @foreach($orders as $order)
            <tr>
                <td class="td-nowrap">{{ $order->increment_id }}</td>
                <td>{{ $order->student ? $order->student->username : '' }}</td>
                <td class="td-nowrap">{{ $order->student ? $order->student->firstname : '' }} {{  $order->student ? $order->student->lastname : ''}}</td>
                <td>{{ $order->student ? $order->student->group->customer_group_code : '' }}</td>
                <td class="td-nowrap">{{ $order->updated_at }}</td>
            </tr>
        @endforeach
    </table>
@endif
<br>
<br>--}}
@foreach($items as $item)
    <table width="100%">
        <tr>
            <td valign="top">

                <strong>Product ID:</strong>
                <br>
                {{ $item->product_id }}
                <br>
                <br>

                <strong class="medium">Product:</strong>
                <br>
                {{ $item->product->name }}
                <br>
                <br>

                <strong class="medium">Categorie:</strong>
                <br>
                {{  $item->product->category->name  }}
                <br>
                <br>

                <strong class="medium">ISBN nummer:</strong>
                <br>
                {{  $item->product->isbn  }}
                <br>
                <br>

                <strong class="medium">Aantal:</strong>
                <br>
                {{  $item->in_batch  }}
                <br>
                <br>

                <strong class="medium">Leverancier:</strong>
                <br>
                {{  $item->product->provider  }}
            </td>
        </tr>
    </table>
    @if(count($items) > 1 && $items->last() != $item)
        <pagebreak>
    @endif
@endforeach
