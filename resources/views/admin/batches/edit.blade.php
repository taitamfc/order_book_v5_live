@extends('admin.layouts.app')

@section('content')

    <!-- Modal -->
        <div class="modal fade" id="edit_qty" tabindex="-1" role="dialog" aria-labelledby="edit_qtyLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="edit_qtyLabel">Modal title</h4>
              </div>
              {!! Form::open(['route' => 'batches.edit_qty']) !!}
                  <div class="modal-body">
                    <input type="hidden" name="batch_id" value="{{ $batch->batch_id }}">
                    <input type="hidden" name="product_id" class="productId">
                    <div>De volgende hoeveelbeid van dit product is binnen gekomen:</div>
                    <div class="row">
                        <div class="col-sm-3">
                             <input type="text" name="qty" class="form-control productQty">
                        </div>
                    </div>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Sluiten</button>
                    <button type="submit" class="btn btn-primary">Binnen melden</button>
                  </div>
              </form>
            </div>
          </div>
        </div>
       <!-- End modal -->
    	<div class="main" id="main">
    		<div class="container">
    			<div class="row">
    				<div class="col-xs-12">
                        @include('flash::message')
                        <div class="panel panel-custom">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-5 col-xxs-12 text-center-xxs">
                                        <div class="title">
                                            <h3>Batch</h3>
                                        </div>
                                    </div>
                                    <div class="col-xs-7 col-xxs-12 text-xs-right">
                                        <a href="{!! URL::route('batches.index') !!}" class="btn btn-info btn-xxs-block">TERUG</a>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <form action="#" method="post" class="form-horizontal">
                                    <div class="form-group">
                                        <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Batch ID:</label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" id="batch_id" value="{{ $batch->batch_id }}" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Status:</label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" value="{{ $batch->batchProducts->sum('qty_available') }} producten binnen van de  {{ $items->sum('in_batch') }} totaal." disabled>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div><!-- .panel -->
                        <div class="panel panel-custom">
                            <div class="panel-heading">
                                <div class="title">
                                    <h3>Bestellingen Overzicht</h3>
                                </div>
                            </div>
                            <div class="panel-body">
                                <form action="{!! route('batches.edit',$batch->batch_id) !!}" method="GET" id="batchFormOrders">
                                    <div class="row row10">
                                        <div class="col-lg-2 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                            <input type="text" class="form-control filter-select" placeholder="ID .." name="order_id" @if(isset($_GET['order_id'])) value="{!! $_GET['order_id'] !!}" @endif>
                                        </div>
                                        <div class="col-lg-2 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                            <input type="text" class="form-control" placeholder="Studentcode .." name="student_code" @if(isset($_GET['student_code'])) value="{!! $_GET['student_code'] !!}" @endif>
                                        </div>
                                        <div class="col-lg-3 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                            <input type="text" class="form-control" placeholder="Naam .." name="student_name" @if(isset($_GET['student_name'])) value="{!! $_GET['student_name'] !!}" @endif>
                                        </div>
                                        <div class="col-lg-2 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                            {!! Form::select('group',$groups->pluck('customer_group_code','customer_group_id'),isset($_GET['group']) ? $_GET['group'] : null,['class' => 'form-control jquerySelect batchFormOrders','placeholder' => 'Groep ..']) !!}
                                        </div>
                                        <div class="col-lg-3 col-sm-8 col-xs-12">
                                            <div class="row row10">
                                                <div class="col-xs-6 col-xxs-12 form-group">
                                                    <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                                        <input type="text" class="form-control ProductBatchDateFrom" placeholder="Van .." name="from" @if(isset($_GET['from'])) value="{!! $_GET['from'] !!}" @endif>
                                                        <div class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-xxs-12 form-group">
                                                    <div class="input-group date" data-provide="datepicker"  data-date-format="dd/mm/yyyy">
                                                        <input type="text" class="form-control ProductBatchDateUntil" placeholder="Tot .." name="until" @if(isset($_GET['until'])) value="{!! $_GET['until'] !!}" @endif>

                                                        <div class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="submit" style="display: none;">
                                </form>
                                    <div class="table-responsive">
                                        @if ($orders)
                                            <table class="table table-custom">
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Studentcode</th>
                                                    <th>Naam</th>
                                                    <th>Groep</th>
                                                    <th>Besteldatum</th>
                                                    <th class="th-flex">Actie</th>
                                                </tr>
                                                @foreach($orders as $order)
                                                    <tr>
                                                        <td class="td-nowrap">{{ $order->order_id }}</td>
                                                        <td>{{ $order->student ? $order->student->username : '' }}</td>
                                                        <td class="td-nowrap">{{ $order->student ? $order->student->firstname : '' }} {{  $order->student ? $order->student->lastname : ''}}</td>
                                                        <td>{{ $order->student->group->customer_group_code }}</td>
                                                        <td class="td-nowrap">{{ date('d/m/Y - H:i',strtotime($order->created_at)) }}</td>
                                                        <td>
                                                            <a href="{!! URL::route('orders.show',[$order->order_id]) !!}" class="txt-orange">Bekijken</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        @endif
                                    </div>
                            </div>
                        </div><!-- .panel -->
                        <div class="panel panel-custom" id="products">
                            
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-5 col-xxs-12 text-center-xxs">
                                        <div class="title">
                                            <h3>Producten Overzicht</h3>
                                        </div>
                                    </div>
                                    <div class="col-xs-7 col-xxs-12 text-xs-right">
                                        {{-- send email to factory with product of category "Commerciële boeken" (category_id = 4) --}}
                                        {{-- <a class="btn btn-info btn-xxs-block" href="{{route('batches.sendEmailToFactory',['batch_id' => $batch->batch_id ])}}" style="margin-right: 15px;">Stuur E-mail Naar Uitgever</a> --}}
                                        <a href="{!! URL::route('batches.export', ['id' => $batch->batch_id, 'provider' => Request::get('provider')]) !!}" class="btn btn-info btn-xxs-block export-bestelbon-btn" style="margin-right: 15px;">Export Bestelbon</a>
                                        <a href="{!! URL::route('batches.to_txt_file',$batch->batch_id) !!}" class="btn btn-info btn-xxs-block export-print-btn">Export Print</a>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <form action="{!! route('batches.edit',$batch->batch_id) !!}" method="GET" id="batchesForm">
                                    <div class="row row10">
                                        <div class="col-sm-2 col-xs-6 col-xxs-12 form-group">
                                            <input type="text" class="form-control productId filter-select" placeholder="ID .." name="id" @if(isset($_GET['id'])) value="{!! $_GET['id'] !!}" @endif>
                                        </div>
                                        <div class="col-sm-2  col-xs-6 col-xxs-12 form-group">
                                            {!! Form::select('product',$products->pluck('name','id'),isset($_GET['product']) ? $_GET['product'] : null,['class' => 'form-control jquerySelect selectedProductId','placeholder' => 'Product ..']) !!}
                                        </div>
                                        <div class="col-sm-2 col-sm-12 form-group">
                                            {!! Form::select('category',$categories->pluck('name','category_id'),isset($_GET['category']) ? $_GET['category'] : null,['class' => 'form-control jquerySelect selectedProductId','placeholder' => 'Categorie ..']) !!}
                                        </div>

                                        <div class="col-sm-2 form-group">
                                            <select class="form-control filter-select" name="is_complete">
                                                <option value="">Behandeld? ..</option>
                                                <option value="1" <?=isset($_GET['is_complete']) && $_GET['is_complete'] == 1 ? ' selected':'';?>>Ja</option>
                                                <option value="0" <?=isset($_GET['is_complete']) && $_GET['is_complete'] == '0' ? ' selected':'';?>>Nee</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-2 form-group">
                                            <select class="form-control filter-select" name="qty_available">
                                                <option value="">Binnen ..</option>
                                                <option value="1" <?=isset($_GET['qty_available']) && $_GET['qty_available'] == 1 ? ' selected':'';?>>Ja</option>
                                                <option value="0" <?=isset($_GET['qty_available']) && $_GET['qty_available'] == '0' ? ' selected':'';?>>Nee</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-2 col-xs-6 col-xxs-12 form-group">
                                            <select class="form-control filter-select jquerySelect" name="provider">
                                                <option value="">Leverancier ..</option>
                                                @if($providers)
                                                    @foreach($providers as $provider)
                                                        <option value="{{ $provider->provider }}" <?=isset($_GET['provider']) && $_GET['provider'] == $provider->provider ? ' selected':'';?>>{{ $provider->provider }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </form>
                                    <div class="table-responsive">
                                        <table class="table table-custom valign-top small-font batchesTable">
                                            @foreach($items as $item)
                                            <tr data-id="{{ $item->product_id }}">
                                                <td class="td-nowrap2" width="20%">
                                                    <div class="pad-bot10">
                                                        <span class="medium">PRODUCT ID:</span>
                                                        <br>
                                                        {{ $item->product_id }}
                                                    </div>
                                                    <div class="pad-bot10">
                                                        <span class="medium">PRODUCT:</span>
                                                        <br>
                                                        {{ $item->product->name }}
                                                    </div>
                                                    @if($item->product->isbn)
                                                        <div class="pad-bot10">
                                                            <span class="medium">ISBN nummer:</span>
                                                            <br>
                                                            {{ $item->product->isbn }}
                                                        </div>
                                                    @endif
                                                </td>
                                                <td class="td-nowrap">
                                                    <div class="pad-bot10">
                                                        <span class="medium">MIN. VOORRAAD:</span>
                                                        <br>
                                                        {{ $item->product->min_qty }}
                                                    </div>
                                                    <div class="pad-bot10">
                                                        <span class="medium">HUIDIGE VOORRAAD:</span>
                                                        <br>
                                                        {{ $item->product->qty }}
                                                    </div>
                                                    <div class="pad-bot10">
                                                        <span class="medium">LEVERANCIER:</span>
                                                        <br>
                                                        {{ $item->product->provider }}
                                                    </div>
                                                </td>
                                                <td class="td-nowrap">
                                                    <div class="pad-bot10">
                                                        <span class="medium">AANTAL KEER IN BATCH:</span>
                                                        <br>
                                                        {{ $item->in_batch }}
                                                    </div>
                                                    <div class="pad-bot10">
                                                        <span class="medium">BESTELLING GEDAAN?</span>
                                                        <ul class="check-list">
                                                            @if($item->product->product_type == "91")
                                                                <label class="radio-label">
                                                                    <input type="radio" name="is_complete{{ $item->product->id}}" class="styled radio-custom setPrinted" data-in_batch="{{ $item->in_batch }}" data-product_id="{{ $item->product->id }}" data-val="0" @if ($item->batchProducts->is_printed) checked @endif>
                                                                    Productie opdracht gemaakt.
                                                                </label>
                                                            @else
                                                                <li>
                                                                    <label class="radio-label">
                                                                        <input type="radio" name="is_complete{{ $item->product->id }}" class="styled radio-custom setComplete" data-in_batch="{{ $item->in_batch }}" data-product_id="{{ $item->product->id }}" data-isbn="{{ $item->product->isbn }}" data-val="-1" @if ($item->batchProducts->is_complete == '-1' || $item->batchProducts->is_complete == '') checked @endif>
                                                                        Nee.
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label class="radio-label">
                                                                        <input type="radio" name="is_complete{{ $item->product->id}}" class="styled radio-custom setComplete" data-in_batch="{{ $item->in_batch }}" data-product_id="{{ $item->product->id }}" data-isbn="{{ $item->product->isbn }}" data-val="1" @if($item->batchProducts->is_complete) checked @endif>
                                                                        De bestelling is gedaan.
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label class="radio-label">
                                                                        <input type="radio" name="is_complete{{ $item->product->id }}" class="styled radio-custom setComplete" data-in_batch="{{ $item->in_batch }}" data-product_id="{{ $item->product->id }}" data-isbn="{{ $item->product->isbn }}" data-val="0" @if ($item->batchProducts->is_complete == '0') checked @endif>
                                                                        Reserveren uit voorraad.
                                                                    </label>
                                                                </li>
                                                                
                                                            @endif
                                                        </ul>
                                                        @if($item->product->product_type == "91")
                                                            <button class="btn btn-xs btn-default deselect-print" data-product_id="{{ $item->product->id }}" data-in_batch="{{ $item->in_batch }}" data-isbn="{{ $item->product->isbn }}">Reset</button>
                                                        @else
                                                            <button class="btn btn-xs btn-default deselect-complete" data-product_id="{{ $item->product->id }}" data-in_batch="{{ $item->in_batch }}" data-isbn="{{ $item->product->isbn }}">Reset</button>
                                                        @endif
                                                    </div>
                                                </td>
                                                <td class="td-nowrap">
                                                    <div class="pad-bot10">
                                                        <span class="medium">AANTAL PRODUCTEN BINNEN:</span>
                                                        <div class="nr-products-inside"><span class="qty_available">{{ $item->available  }}</span>  /  {{ $item->in_batch }}</div>
                                                    </div>
                                                </td>
                                                @if(false)<td class="td-align-bottom th-flex" style="display: none;"><a href="#" class="btn btn-success">PRODUCEREN</a></td>@endif
                                                <td class="th-flex cell-more-btns" style="width:20%">
                                                    <a href="{!! URL::route('producten.show',[$item->product_id]) !!}" class="btn btnorangeTransp btn-block">Product Bekijken</a>
                                                    <a href="{!! URL::route('batches.pdf',[$batch->batch_id, $item->product_id]) !!}" class="btn btnorangeTransp btn-block @if ($item->batchProducts->qty_available >= $item->in_batch) disabled @endif">Bestelbon Genereren</a>

                                                    {{--91 is a print product this int come from magento site--}}

                                                    @if($item->product->product_type == "91")
                                                        <a href="#" class="btn btnorangeTransp btn-block max_qty {{ $item->batchProducts->qty_available == $item->in_batch ? 'disabled':'' }}" data-product-id="{{ $item->product->id  }}" data-in-batch="{{ $item->in_batch  }}" data-batch-product-id="{{ $item->batchProducts->batch_product_id  }}">Geproduceerd melden</a>
                                                        @else
                                                        <a href="#" class="btn btnorangeTransp btn-block {{ $item->batchProducts->qty_available == $item->in_batch ? 'disabled':'' }}" @if($item->batchProducts->qty_available != $item->in_batch) data-toggle="modal" data-target="#edit_qty" data-id="{{ $item->product_id }}" data-name="{{ $item->product->name }}"  data-qty="{{ $item->batchProducts->qty_available  }}" @endif>Binnen Melden</a>
                                                    @endif

                                                    {{-- send email to factory with product of category "Commerciële boeken" (category_id = 4) --}}
                                                    {{-- @if ($item->product->category_id == 4)
                                                        <a class="btn btnorangeTransp" href="{{route('batches.sendEmailToFactory',['product_id' => $item->product->id, 'batch' => $batch->batch_id ])}}">Stuur E-mail Naar Factor</a>
                                                    @endif --}}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                            </div>
                        </div><!-- .panel -->
                    </div>
                </div>
    		</div>
    	</div><!-- end .main -->
@endsection

@push('scripts')
<script>
    $('.export-print-btn').click(function(){
        $.each($('.setPrinted'), function(key, val){
            var this_el = $(this);
            this_el.prop('checked', true).trigger('refresh');
            var nr_product_inside_block = this_el.closest('tr').find('.nr-products-inside');
            nr_product_inside_block.html(this_el.data('in_batch') + ' / ' + this_el.data('in_batch'));

        });
    });

    $('.export-bestelbon-btn').click(function(){
        //$('input.setComplete[data-val="1"]:checked').attr('checked', false).trigger('refresh');
        $.each($('input.setComplete[data-val="1"]:not(:checked)'), function(key, val){
            var this_el = $(this);
            this_el.prop('checked', true).trigger('refresh');
            var nr_product_inside_block = this_el.closest('tr').find('.nr-products-inside');
            nr_product_inside_block.html('0 / ' + this_el.data('in_batch'));

        });
        $('input.setComplete').trigger('refresh');

    });
</script>
@endpush