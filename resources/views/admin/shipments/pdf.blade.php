<!DOCTYPE html>
<html>
<head>
    <style>
        body{
            font-size:12px;
            font-family: arial, sans-serif;
        }
        h3.title{
            background-color: #009FE3;
            margin: 0;
            padding: 0 10px 0 10px;
        }

        .items th,
        .items td{
            padding: 3px 0 3px 0;
        }
        .items th:nth-child(1),
        .items td:nth-child(1){
            padding-left: 10px;
        }
        .items th{
            background-color: #009FE2;
            font-weight: 300;
        }

        .block{
            border: 1px solid #009FE3;
            margin-bottom: 15px;
        }
        .body{
            padding: 10px;
        }
        .td-title{
            background-color: #009FE3;
            font-weight: bold;
        }
    </style>
</head>

<body>

    <table width="100%">
        {{-- <tr>
            <td style="text-align: left"><img src="{{public_path()}}/assets/img/tot_logo.png" alt=""></td>
            <td style="text-align: right"><img src="{{public_path()}}/assets/img/logo.png" alt=""></td>
        </tr> --}}
        <tr>
            <td><img src="https://i.ibb.co/0n01M4X/email-header.png" alt=""></td>
        </tr>
    </table>


    <p style="font-size: 18px;">Bestelling nr {{ $order->increment_id }}</p>

    <div class="block">
        <h3 class="title">Bestelinformatie</h3>
        <div class="body">
            {{ $order->student->username }}<br>
            {{ $order->firstname}} {{ $order->middlename }} {{ $order->lastname }}<br>
            {{ $order->city }} {{ $order->street }}, {{ $order->postcode }}<br>
            {{ $order->telephone }}<br>
            {{ $order->email }}
        </div>

    </div>


    <div class="block">
        <table class="items" width="100%" cellpadding="5" cellspacing="0">
            @if ( isset($order->items['lesbundels']) && count( array_intersect($order->items['lesbundels']->pluck('item_id')->toArray(), $send_items) ) )
                <tr>
                    <td colspan="3" class="td-title">Lesbundels</td>
                </tr>
                <tr>
                    <th align="left">Titel</th>
                    <th align="left">Modulecode</th>
                    <th align="left">Druk</th>
                </tr>
                @foreach($order->items['lesbundels'] as $item)
                    @if(in_array($item->item_id, $send_items)))
                    <tr>
                        <td>{{ $item->product->name }}</td>
                        <td>{{ $item->product->modulecode }}</td>
                        <td>{{ $item->product->druk }}</td>
                    </tr>
                    @endif
                @endforeach
            @endif

            @if (isset($order->items['commerciele-boeken']) && count( array_intersect($order->items['commerciele-boeken']->pluck('item_id')->toArray(), $send_items) ))
                <tr>
                    <td colspan="3" class="td-title">Commerciële boeken</td>
                </tr>
                <tr>
                    <th align="left">Titel</th>
                    <th align="left">ISBN</th>
                    <th align="left">Druk</th>
                </tr>
                @foreach($order->items['commerciele-boeken'] as $item)
                    @if(in_array($item->item_id, $send_items))
                        <tr>
                            <td>{{ $item->product->name }}</td>
                            <td>{{ $item->product->isbn }}</td>
                            <td>{{ $item->product->druk }}</td>
                        </tr>
                    @endif
                @endforeach
            @endif

            @if (isset($order->items['keuzepakketten']) && count( array_intersect($order->items['keuzepakketten']->pluck('item_id')->toArray(), $send_items) ))
                <tr>
                    <td colspan="3" class="td-title">Keuzepakketten</td>
                </tr>
                <tr>
                    <th align="left">Titel</th>
                    <th align="left">ISBN</th>
                    <th align="left">Druk</th>
                </tr>
                @foreach($order->items['keuzepakketten'] as $item)
                    @if(in_array($item->item_id, $send_items))
                        <tr>
                            <td>{{ $item->product->name }}</td>
                            <td>{{ $item->product->isbn }}</td>
                            <td>{{ $item->product->druk }}</td>
                        </tr>
                    @endif
                @endforeach
            @endif

            @if (isset($order->items['back2school']) && count( array_intersect($order->items['back2school']->pluck('item_id')->toArray(), $send_items) ))
                <tr>
                    <td colspan="3" class="td-title">Back2School</td>
                </tr>
                <tr>
                    <th align="left">Titel</th>
                    <th align="left">ISBN</th>
                    <th align="left">Druk</th>
                </tr>
                @foreach($order->items['back2school'] as $item)
                    @if(in_array($item->item_id, $send_items))
                        <tr>
                            <td>{{ $item->product->name }}</td>
                            <td>{{ $item->product->isbn }}</td>
                            <td>{{ $item->product->druk }}</td>
                        </tr>
                    @endif
                @endforeach
            @endif
        </table>
    </div>

</body>
</html>
