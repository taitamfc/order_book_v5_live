@extends('admin.layouts.app')

@section('content')    	
        <div class="main" id="main">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel panel-custom">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="title">
                                            <h3>Verzendingen</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        @if(Auth::guard('admin')->user()->name != 'Groenhorst')
                                        <a href="{{URL::route('shipments.index', array_merge(Request::all(), ['export_postnl' => 1] ) )}}" class="btn btn-info btn-xxs-block">Expost PostNL</a>
                                            <a href="{{URL::route('shipments.index', array_merge(Request::all(), ['export' => 1] ) )}}" class="btn btn-info btn-xxs-block pull-right">RAAPLIJST</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <form action="{!! URL::route('shipments.index') !!}" method="get">
                                    <button type="submit" value="sdsd" style="position: absolute;left: -9999px;"></button>
                                    <div class="row row10">
                                        <div class="col-lg-1 col-md-4 col-xs-6 col-xxs-12 form-group">
                                            <input type="text" class="form-control" placeholder="ID .." name="id">
                                        </div>
                                        <div class="col-lg-1 col-md-4 col-xs-6 col-xxs-12 form-group">
                                            <input type="text" class="form-control" placeholder="ID .." name="batch_id">
                                        </div>
                                        <div class="col-lg-2 col-md-4 col-xs-6 col-xxs-12 form-group">
                                            <input type="text" class="form-control barcode-input" placeholder="Studentcode .."  name="username" class="username-input" autofocus>
                                        </div>
                                        <div class="col-lg-3 col-md-4 col-xs-6 col-xxs-12 form-group">
                                            <input type="text" class="form-control" placeholder="Naam .." name="name">
                                        </div>
                                        <div class="col-lg-2 col-md-4 col-xs-6 col-xxs-12 form-group">
                                            {!! Form::select('is_complete',['0' => 'Compleet?','1' => 'Ja', 2 => 'Nee'], Request::get('is_complete', 0), ['class' => 'form-control','onchange' => 'this.form.submit()']) !!}
                                        </div>
                                        <div class="col-lg-2 col-md-4 col-xs-6 col-xxs-12 form-group">
                                            {!! Form::select('shipped_all',['' => 'Verzonden?','1' => 'Nee', 2 => 'Ja'], Request::get('shipped_all'), ['class' => 'form-control','onchange' => 'this.form.submit()']) !!}
                                        </div>
                                        <div class="col-md-3 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                            {!! Form::select('product',$products->pluck('name','product_id'), Request::get('product'),['class' => 'form-control jquerySelect','placeholder' => 'Product ..','onchange' => 'this.form.submit()']) !!}
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-6 col-xxs-12 form-group">
                                            {!! Form::select('group',$groups->pluck('customer_group_code', 'customer_group_id'), Request::get('group'),['class' => 'form-control jquerySelect','placeholder' => 'Groep (Klas) ..','onchange' => 'this.form.submit()']) !!}
                                        </div>
                                        <div class="col-xs-1 col-xxs-12 form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <a href="{{ route('shipments.index') }}"><span class="glyphicon glyphicon-refresh"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-custom small-font">
                                            <tr>
                                                <th>ID</th>
                                                <th class="td-nowrap">BATCH ID</th>
                                                <th>Studentcode</th>
                                                <th>Naam</th>
                                                <th>Groep</th>
                                                <th class="td-nowrap">Alle producten aanwezig?</th>
                                                <th>Verzonden</th>
                                                <th>Verzonden?</th>
                                                <th class="th-flex">Actie</th>
                                            </tr>

                                            @foreach($orders as $order)
                                                <?php $qty_shipped =  ($order->items_shipped_count ) ? $order->items_shipped_count->count : 0; $qty = ($order->items_count) ? $order->items_count->count : 0; ?>
                                                <tr class="orderRow{{ $order->order_id }}">

                                                    <td class="td-nowrap">{{ $order->increment_id}}</td>
                                                    <td >{{ $order->batch_id}}</td>
                                                    <td>{{ $order->student ? $order->student->username : '' }}</td>
                                                    <td class="td-nowrap">{{ $order->firstname }} {{ $order->lastname }}</td>
                                                    <td>{{ $order->student->group ? $order->student->group->customer_group_code : ''  }}</td>
                                                    <td class="complete">@if ($order->batchProducts->whereIn('product_id', $order->items->pluck('product_id')->toArray())->where('is_complete', 0)->count() >= $order->batchProducts->whereIn('product_id', $order->items->pluck('product_id')->toArray())->count() ) Ja @else Nee @endif </td>
                                                    <td class="td-nowrap"> <span class="qty_shipped">{{ $qty_shipped}}</span> / {{ $qty}} </td>
                                                    <td class="text-center">
                                                        <label class="checkbox-label empty">
                                                            <input type="checkbox" class="styled checkbox-custom" disabled @if ($qty_shipped >= $qty) checked @endif>
                                                        </label>
                                                    </td>
                                                    <td class="td-nowrap">
                                                        <a href="{!! route('shipments.show',[$order->order_id]) !!}" class="txt-orange">Bekijken</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                    {{ $orders->appends(Request::except('page'))->links() }}
                                </form>
                            </div>
                        </div><!-- .panel -->
                    </div>
                </div>
            </div>
        </div><!-- end .main -->
@endsection