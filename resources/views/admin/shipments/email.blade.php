<style type="text/css">
    td {
        padding:5px;
    }
</style>
Hallo {{ $order->firstname }} {{ $order->middlename }} {{ $order->lastname }}, <br />
<br />
De volgende producten zijn naar je verzonden: <br />
<br />
@if ( isset($order->items['Lesbundels']) )

        <table >
            <tr>
                <th align="left" style="padding:5px;">Modulecode</th>
                <th align="left" style="padding:5px;">Druk</th>
                <th align="left" style="padding:5px;">Titel</th>
                <th align="left" style="padding:5px;">Prijs</th>
            </tr>
            @foreach($order->items['Lesbundels'] as $item)
                <tr>
                    <td style="padding:5px;">{{ $item->product->modulecode }}</td>
                    <td style="padding:5px;">{{ $item->product->druk }}</td>
                    <td style="padding:5px;">{{ $item->product->name }}</td>
                    <td style="padding:5px;">€ {{ $item->price }}</td>
                </tr>
            @endforeach
        </table>
    <br><br>    
@endif



@if (isset($order->items['Commerciële boeken']))

        <table>
            <tr>
                <th align="left" style="padding:5px;" >ISBN</th>
                <th align="left" style="padding:5px;">Druk</th>
                <th align="left" style="padding:5px;">Titel</th>
                <th align="left" style="padding:5px;">Auteur</th>
                <th align="left" style="padding:5px;">Prijs</th>

            </tr>
            @foreach($order->items['Commerciële boeken'] as $item)
                <tr>
                    <td>
                    <td style="padding:5px;">{{ $item->product->isbn }}</td>
                    <td style="padding:5px;">{{ $item->product->druk }}</td>
                    <td style="padding:5px;">{{ $item->product->name }}</td>
                    <td style="padding:5px;">{{ $item->product->provider }}</td>
                    <td style="padding:5px;">€ {{ $item->price }}</td>

                </tr>
            @endforeach
        </table>
    <br><br>
@endif



@if (isset($order->items['Keuzepakketten']))

        <table>
            <tr>

                <th align="left" style="padding:5px;" >ISBN</th>
                <th align="left" style="padding:5px;" >Druk</th>
                <th align="left" style="padding:5px;">Titel</th>
                <th align="left" style="padding:5px;">Auteur</th>
                <th align="left" style="padding:5px;">Prijs</th>

            </tr>
            @foreach($order->items['Keuzepakketten'] as $item)
                <tr>
                    <td style="padding:5px;">{{ $item->product->isbn }}</td>
                    <td style="padding:5px;">{{ $item->product->druk }}</td>
                    <td style="padding:5px;">{{ $item->product->name }}</td>
                    <td style="padding:5px;">{{ $item->product->provider }}</td>
                    <td style="padding:5px;">€ {{ $item->price }}</td>

                </tr>
            @endforeach
        </table>

@else
   
@endif


@if (isset($order->items['Pretpakketten']))

        <table>
            <tr>

                <th align="left" style="padding:5px;" >ISBN</th>
                <th align="left" style="padding:5px;" >Druk</th>
                <th align="left" style="padding:5px;">Titel</th>
                <th align="left" style="padding:5px;">Auteur</th>
                <th align="left" style="padding:5px;">Prijs</th>

            </tr>
            @foreach($order->items['Pretpakketten'] as $item)
                <tr>
                    <td style="padding:5px;">{{ $item->product->isbn }}</td>
                    <td style="padding:5px;">{{ $item->product->druk }}</td>
                    <td style="padding:5px;">{{ $item->product->name }}</td>
                    <td style="padding:5px;">{{ $item->product->provider }}</td>
                    <td style="padding:5px;">€ {{ $item->price }}</td>

                </tr>
            @endforeach
        </table>

@else
   
@endif

Met vriendelijke groeten, <br />
<br />
Groenhorst