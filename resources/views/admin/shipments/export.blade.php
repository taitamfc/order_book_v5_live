<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <style>
        body{
            font-size: 12px;
        }
        .products {
            border-collapse: collapse;
        }
        .products th,
        .products td{
            border: 1px solid #000;
        }
        .products th{
            background-color: #bcbcbc;
        }
    </style>
</head>

<body>
ID: {{$order->increment_id}}<br>
Batch ID: {{$order->batch_id}}<br>
Naam: {{ $order->firstname }} {{ $order->lastname }}<br>
@if($order->student)
<img src="data:image/png;base64,{!! DNS1D::getBarcodePNG($order->student->username, "C128") !!}" alt="barcode" width="250px" />
@endif
<table class="products" width="100%" cellspacing="0" cellpadding="5">
    <tr>
        <th>ISBN</th>
        <th>Druk</th>
        <th>Titel</th>
        <th>Auteur</th>
        <th>Prijs</th>
    </tr>
    @foreach($items as $item)
        @if($item->qty_shipped <= 0)
            <tr>
                <td>{{$item->product->isbn}}</td>
                <td>{{$item->product->druk}}</td>
                <td>{{$item->product->name}}</td>
                <td>{{ $item->product->provider }}</td>
                <td style="text-align: right;">€{{ \App\Helpers\Custom::money_format($item->price) }}</td>
            </tr>
        @endif
    @endforeach
</table>
</body>

</html>