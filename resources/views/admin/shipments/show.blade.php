@extends('admin.layouts.app')

@section('content') 
    	<div class="main" id="main">
    		<div class="container">
    			<div class="row">
    				<div class="col-xs-12">
                        <div class="alert alert-success barcode-alert hidden" role="alert"></div>
                        {{--<div class="block-title">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-sm-6">--}}
                                    {{--<h2>Bestelling</h2>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-6 text-sm-right">--}}
                                    {{--<a href="{!! URL::route('shipments.send.email',[$order->order_id]) !!}" class="btn btnorangeTransp">VERZENDEN</a>--}}
                                    {{--<a href="{!! URL::route('shipments.index') !!}" class="btn btnorangeTransp">TERUG</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="panel panel-custom">
                                    <div class="panel-heading">
                                        <div class="row">
                                            @if(Auth::guard('admin')->user()->isAdmin())
                                            <a href="{{URL::route('shipments.reset', ['id' => $order->order_id])}}" class="btn btn-info btn-xxs-block pull-left" onclick="return confirm('Weet je het zeker?')">Reset verzending</a>
                                            {!! Form::open(['route' => ['shipments.pdf', $order->order_id],'method' => 'POST', 'class' => 'export-form']) !!}
                                            <button class="btn btn-info btn-xxs-block pull-right" type="submit" disabled>Pakbon</button>
                                            {!! Form::close() !!}
                                            @else
                                                &nbsp;
                                            @endif
                                        </div>
                                        {{-- <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel1" aria-expanded="true" aria-controls="collapsePanel1"></span> --}}
                                    </div>
                                    <div class="collapse in" id="collapsePanel1">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <input type="text" id="barcode-input" class="form-control" autofocus>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- .panel -->
                                {!! Form::open(['route' => ['orders.update', $order->order_id],'method' => 'POST']) !!}
                                <input type="hidden" name="id" value="{{$order->order_id}}">
                                <div class="panel panel-custom">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-8 col-xxs-12 text-center-xxs">
                                                <div class="title">
                                                    <h3>Status</h3>
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-xxs-12 text-xs-right">
                                                <button class="btn btn-info btn-xxs-block" type="submit">OPSLAAN</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="collapse in" id="collapsePanel1">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <select name="status" id="status" class="form-control">
                                                    @if ($order->status == "pending")
                                                        <option value="pending" selected>In Behandeling</option>
                                                        <option value="complete">Afgerond</option>
                                                    @else
                                                        <option value="pending">In Behandeling</option>
                                                        <option value="complete" selected>Afgerond</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="panel panel-custom">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-8 col-xxs-12 text-center-xxs">
                                                    <div class="title">
                                                        <h3>Persoonlijke informatie</h3>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-xxs-12 text-xs-right">
                                                    <button class="btn btn-info btn-xxs-block" type="submit">OPSLAAN</button>
                                                </div>
                                            </div>
                                            {{-- <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel1" aria-expanded="true" aria-controls="collapsePanel1"></span> --}}
                                        </div>
                                        <div class="collapse in" id="collapsePanel1">
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="username" class="control-label">Besteldatum:</label>
                                                    <input type="text" disabled class="form-control" value="{{ date('d/m/Y - H:i',strtotime($order->created_at)) }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username" class="control-label">Studentnummer:</label>
                                                    <input type="text" disabled name="username" id="username" class="form-control" value="{{ $order->student->username }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="firstname" class="control-label">Voornaam:</label>
                                                    <input type="text" name="firstname" id="firstname" class="form-control" value="{{ $order->firstname }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="middlename" class="control-label">Tussenvoegsel:</label>
                                                    <input type="text" name="middlename" id="middlename" class="form-control" value="{{ $order->middlename }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="lastname" class="control-label">Achternaam:</label>
                                                    <input type="text" name="lastname" id="lastname" class="form-control" value="{{ $order->lastname }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="lastname" class="control-label">Groep (Klas):</label>
                                                    <input type="text" class="form-control" value="{{ $order->student->group->customer_group_code }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="email" class="control-label">E-mailadres:</label>
                                                    <input type="email" name="email" id="email" class="form-control" value="{{ $order->email }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="telephone" class="control-label">Telefoonnummer:</label>
                                                    <input type="tel" name="telephone" id="telephone" class="form-control" value="{{ $order->telephone }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="street" class="control-label">Straat:</label>
                                                    <input type="text" name="street" id="street" class="form-control" value="{{ $order->street }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="postcode" class="control-label">Postcode:</label>
                                                    <input type="text" name="postcode" id="postcode" class="form-control" value="{{ $order->postcode }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="city" class="control-label">Plaats:</label>
                                                    <input type="text" name="city" id="city" class="form-control" value="{{ $order->city }}">
                                                </div>

                                            </div>
                                        </div>
                                    </div><!-- .panel -->
                                {!! Form::close() !!}
                                {{--<div class="panel panel-custom">--}}
                                    {{--<div class="panel-heading">--}}
                                        {{--<div class="title">--}}
                                            {{--<h3>PERSOONLIJKE INFORMATIE</h3>--}}
                                        {{--</div>--}}
                                        {{--<span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel2" aria-expanded="true" aria-controls="collapsePanel2"></span>--}}
                                    {{--</div>--}}
                                    {{--<div class="collapse in" id="collapsePanel2">--}}
                                        {{--<div class="panel-body">--}}
                                            {{--<form action="#" method="post">--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<label for="label_name" class="control-label">E-mailadres:</label>--}}
                                                    {{--<input type="email" class="form-control" value="{{ $order->email }}">--}}
                                                {{--</div>--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<label for="label_name" class="control-label">Telefoonnummer:</label>--}}
                                                    {{--<input type="tel" class="form-control" value="{{ $order->telephone }}">--}}
                                                {{--</div>--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<label for="label_name" class="control-label">Bankrekeningnummer:</label>--}}
                                                    {{--<input type="text" class="form-control" value="{{ $order->bank_account }}">--}}
                                                {{--</div>--}}
                                            {{--</form>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div><!-- .panel -->--}}
                                {{--<div class="panel panel-custom">--}}
                                    {{--<div class="panel-heading">--}}
                                        {{--<div class="title">--}}
                                            {{--<h3>ADRES</h3>--}}
                                        {{--</div>--}}
                                        {{--<span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel3" aria-expanded="true" aria-controls="collapsePanel3"></span>--}}
                                    {{--</div>--}}
                                    {{--<div class="collapse in" id="collapsePanel3">--}}
                                        {{--<div class="panel-body">--}}
                                            {{--<form action="#" method="post">--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<label for="label_name" class="control-label">Straat:</label>--}}
                                                    {{--<input type="text" class="form-control" value="{{ $order->street }}">--}}
                                                {{--</div>--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<label for="label_name" class="control-label">Postcode:</label>--}}
                                                    {{--<input type="text" class="form-control" value="{{ $order->postcode }}">--}}
                                                {{--</div>--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<label for="label_name" class="control-label">Plaats:</label>--}}
                                                    {{--<input type="text" class="form-control" value="{{ $order->city }}">--}}
                                                {{--</div>--}}
                                            {{--</form>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div><!-- .panel -->--}}
                            </div>
                            <div class="col-md-8">
                                <div class="panel panel-custom">
                                    <div class="panel-heading">
                                        <div class="title">
                                            <h3>Lesbundels</h3>
                                        </div>
                                        <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel4" aria-expanded="true" aria-controls="collapsePanel4"></span>
                                    </div>
                                    <div class="collapse in" id="collapsePanel4">
                                        <div class="panel-body">
                                            @if ( isset($order->items['lesbundels']) )
                                                <div class="table-responsive">
                                                    <table class="table table-custom">
                                                        <tr>
                                                            <th>Verzenden</th>
                                                            <th>Verzonden</th>
                                                            <th class="th-two">Modulecode</th>
                                                            <th class="th-two">Druk</th>
                                                            <th>Titel</th>
                                                        </tr>
                                                        @foreach($order->items['lesbundels'] as $item)
                                                            <tr>
                                                                <td class="text-center">
                                                                    <label class="checkbox-label empty">
                                                                        <input type="checkbox" class="styled checkbox-custom setShipped lesbundels" data-item_id="{{ $item->item_id }}" data-isbn="{{$item->product->isbn}}" data-product="{{$item->product->name}}" @if ($item->qty_shipped > 0) checked disabled @endif @if(!Auth::guard('admin')->user()->isAdmin()) disabled @endif>
                                                                    </label>
                                                                </td>
                                                                <td class="text-center">
                                                                    <label class="checkbox-label empty">
                                                                        <input type="checkbox" class="styled checkbox-custom shipped-items" data-item_id="{{ $item->item_id }}" data-isbn="{{$item->product->isbn}}" @if ($item->qty_shipped > 0) checked @endif disabled>
                                                                    </label>
                                                                </td>
                                                                @if(in_array($item->product_id, $send_items))
                                                                    <td><strong>{{ $item->product->modulecode }}</strong></td>
                                                                    <td><strong>{{ $item->product->druk }}</strong></td>
                                                                    <td><strong>{{ $item->product->name }}</strong></td>
                                                                @else
                                                                    <td>{{ $item->product->modulecode }}</td>
                                                                    <td>{{ $item->product->druk }}</td>
                                                                    <td>{{ $item->product->name }}</td>
                                                                @endif
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
                                            @else 
                                                <p><em>Er zijn geen lesbundels besteld door de student.</em></p>   
                                            @endif
                                        </div>
                                    </div>
                                </div><!-- .panel -->
                                <div class="panel panel-custom">
                                    <div class="panel-heading">
                                        <div class="title">
                                            <h3>Commerciële boeken</h3>
                                        </div>
                                        <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel5" aria-expanded="true" aria-controls="collapsePanel5"></span>
                                    </div>
                                    <div class="collapse in" id="collapsePanel5">
                                        <div class="panel-body">
                                            @if (isset($order->items['commerciele-boeken']))
                                                <div class="table-responsive">
                                                    <table class="table table-custom">
                                                        <tr>
                                                            <th>Verzenden</th>
                                                            <th>Verzonden</th>
                                                            <th class="th-two">ISBN</th>
                                                            <th class="th-two">Druk</th>
                                                            <th>Titel</th>
                                                            <th>Auteur</th>
                                                        </tr>
                                                        @foreach($order->items['commerciele-boeken'] as $item)
                                                            <tr>
                                                                <td class="text-center">
                                                                    <label class="checkbox-label empty">
                                                                        <input type="checkbox" class="styled checkbox-custom setShipped" data-item_id="{{ $item->item_id }}" data-isbn="{{$item->product->isbn}}" data-product="{{$item->product->name}}" @if ($item->qty_shipped > 0) checked disabled @endif @if(!Auth::guard('admin')->user()->isAdmin()) disabled @endif>
                                                                    </label>
                                                                </td>
                                                                <td class="text-center">
                                                                    <label class="checkbox-label empty">
                                                                        <input type="checkbox" class="styled checkbox-custom shipped-items" data-item_id="{{ $item->item_id }}" data-isbn="{{$item->product->isbn}}" @if ($item->qty_shipped > 0) checked @endif disabled>
                                                                    </label>
                                                                </td>
                                                                @if(in_array($item->product_id, $send_items))
                                                                    <td><strong>{{ $item->product->isbn }}</strong></td>
                                                                    <td><strong>{{ $item->product->druk }}</strong></td>
                                                                    <td><strong>{{ $item->product->name }}</strong></td>
                                                                    <td><strong>{{ $item->product->provider }}</strong></td>
                                                                @else
                                                                    <td>{{ $item->product->isbn }}</td>
                                                                    <td>{{ $item->product->druk }}</td>
                                                                    <td>{{ $item->product->name }}</td>
                                                                    <td>{{ $item->product->provider }}</td>
                                                                @endif
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
                                            @else 
                                                <p><em>Er zijn geen commerciële boeken besteld door de student.</em></p>       
                                            @endif
                                        </div>
                                    </div>
                                </div><!-- .panel -->
                                <div class="panel panel-custom">
                                    <div class="panel-heading">
                                        <div class="title">
                                            <h3>Keuzepakketten</h3>
                                        </div>
                                        <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel6" aria-expanded="true" aria-controls="collapsePanel6"></span>
                                    </div>
                                    <div class="collapse in" id="collapsePanel6">
                                        <div class="panel-body panel-content">

                                            @if (isset($order->items['keuzepakketten']))
                                                <div class="table-responsive">
                                                    <table class="table table-custom">
                                                        <tr>
                                                            <th>Verzenden</th>
                                                            <th>Verzonden</th>
                                                            <th class="th-two">ISBN</th>
                                                            <th class="th-two">Druk</th>
                                                            <th>Titel</th>
                                                            <th>Auteur</th>
                                                        </tr>
                                                        @foreach($order->items['keuzepakketten'] as $item)
                                                            <tr>
                                                                <td class="text-center">
                                                                    <label class="checkbox-label empty">
                                                                        <input type="checkbox" class="styled checkbox-custom setShipped" data-item_id="{{ $item->item_id }}" data-isbn="{{$item->product->isbn}}" data-product="{{$item->product->name}}" @if ($item->qty_shipped > 0) checked disabled @endif @if(!Auth::guard('admin')->user()->isAdmin()) disabled @endif>
                                                                    </label>
                                                                </td>
                                                                <td class="text-center">
                                                                    <label class="checkbox-label empty">
                                                                        <input type="checkbox" class="styled checkbox-custom shipped-items" data-item_id="{{ $item->item_id }}" data-isbn="{{$item->product->isbn}}" @if ($item->qty_shipped > 0) checked @endif disabled>
                                                                    </label>
                                                                </td>
                                                                @if(in_array($item->product_id, $send_items))
                                                                    <td><strong>{{ $item->product->isbn }}</strong></td>
                                                                    <td><strong>{{ $item->product->druk }}</strong></td>
                                                                    <td><strong>{{ $item->product->name }}</strong></td>
                                                                    <td><strong>{{ $item->product->provider }}</strong></td>
                                                                @else
                                                                    <td>{{ $item->product->isbn }}</td>
                                                                    <td>{{ $item->product->druk }}</td>
                                                                    <td>{{ $item->product->name }}</td>
                                                                    <td>{{ $item->product->provider }}</td>
                                                                @endif
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
                                            @else
                                                <p><em>Er zijn geen keuzepakketten besteld door de student.</em></p>
                                            @endif

                                        </div>
                                    </div>
                                </div><!-- .panel -->
                                <div class="panel panel-custom">
                                    <div class="panel-heading">
                                        <div class="title">
                                            <h3>Back2School</h3>
                                        </div>
                                        <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel6" aria-expanded="true" aria-controls="collapsePanel6"></span>
                                    </div>
                                    <div class="collapse in" id="collapsePanel6">
                                        <div class="panel-body panel-content">

                                            @if (isset($order->items['back2school']))
                                                <div class="table-responsive">
                                                    <table class="table table-custom">
                                                        <tr>
                                                            <th>Verzenden</th>
                                                            <th>Verzonden</th>
                                                            <th class="th-two">ISBN</th>
                                                            <th class="th-two">Druk</th>
                                                            <th>Titel</th>
                                                            <th>Auteur</th>
                                                        </tr>
                                                        @foreach($order->items['back2school'] as $item)
                                                            <tr>
                                                                <td class="text-center">
                                                                    <label class="checkbox-label empty">
                                                                        <input type="checkbox" class="styled checkbox-custom setShipped" data-item_id="{{ $item->item_id }}" data-isbn="{{$item->product->isbn}}" data-product="{{$item->product->name}}" @if ($item->qty_shipped > 0) checked disabled @endif @if(!Auth::guard('admin')->user()->isAdmin()) disabled @endif>
                                                                    </label>
                                                                </td>
                                                                <td class="text-center">
                                                                    <label class="checkbox-label empty">
                                                                        <input type="checkbox" class="styled checkbox-custom shipped-items" data-item_id="{{ $item->item_id }}" data-isbn="{{$item->product->isbn}}" @if ($item->qty_shipped > 0) checked @endif disabled>
                                                                    </label>
                                                                </td>
                                                                @if(in_array($item->product_id, $send_items))
                                                                    <td><strong>{{ $item->product->isbn }}</strong></td>
                                                                    <td><strong>{{ $item->product->druk }}</strong></td>
                                                                    <td><strong>{{ $item->product->name }}</strong></td>
                                                                    <td><strong>{{ $item->product->provider }}</strong></td>
                                                                @else
                                                                    <td>{{ $item->product->isbn }}</td>
                                                                    <td>{{ $item->product->druk }}</td>
                                                                    <td>{{ $item->product->name }}</td>
                                                                    <td>{{ $item->product->provider }}</td>
                                                                @endif
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
                                            @else
                                                <p><em>Er zijn geen pretpakketten besteld door de student.</em></p>
                                            @endif

                                        </div>
                                    </div>
                                </div><!-- .panel -->                                
                                <div class="panel panel-custom">
                                    <div class="panel-heading">
                                        <div class="title">
                                            <h3>TRACK &amp; TRACE</h3>
                                        </div>
                                        <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel7" aria-expanded="true" aria-controls="collapsePanel7"></span>
                                    </div>
                                    <div class="collapse in" id="collapsePanel7">
                                        <div class="panel-body">
                                            {!! Form::open(['route' => ['shipments.update',$order->order_id], 'method' => 'PUT']) !!}
                                                <input type="submit" style="display:none">
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Track &amp; trace code eerste verzending:</label>
                                                    <input type="text" class="form-control" name="track_number" value="{{ $order->track_number }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Track &amp; trace code tweede verzending:</label>
                                                    <input type="text" class="form-control" name="track_number2" value="{{ $order->track_number2 }}">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div><!-- .panel --> 
                            </div>
                        </div>
                    </div>
                </div>
    		</div>
    	</div><!-- end .main -->
@endsection
@push('scripts')
<script>
    $('#barcode-input').keydown(function(event){
        if( event.keyCode == 13){

            var res = 0;

            var isbn = $(this).val();
            var product = null;
            var username = $('#username').val();
            $.each($('input.setShipped'), function(k, v){
               if($(v).data('isbn') ==  isbn && $(v).is(':checked') == false){
                   $(v).attr('checked','checked').trigger('refresh').trigger('change');
                   product = $(v).data('product');
                   res = 1;
               }

               if(isbn == username && $(v).is(':checked') == false && $(v).hasClass('lesbundels')){
                   $(v).attr('checked','checked').trigger('refresh').trigger('change');
                   res = 2;
               }
            });

            if($('input.setShipped').length == $('input.setShipped:checked').length){
                res = 3;
            }

            if(res == 1){
                $('.barcode-alert').text('Product ' + product + ' - ' + isbn + ' is succesvol aangevinkt.').removeClass('alert-danger').removeClass('hidden').addClass('alert-success');
            }
            else if(res == 2){
                $('.barcode-alert').text('De lesbundels zijn succesvol aangevinkt.').removeClass('alert-danger').removeClass('hidden').addClass('alert-success');
            }
            else if(res == 3){
                $('.barcode-alert').text('De order is volledig gereed.').removeClass('alert-danger').removeClass('hidden').addClass('alert-success');
            }
            else{
                $('.barcode-alert').text('Product ' + isbn + ' kon helaas niet gevonden worden.').removeClass('alert-success').removeClass('hidden').addClass('alert-danger');
            }

            $(this).val('');
        }
    });

    $('.export-form').submit(function(e){

        //remove old checked inputs
        $.each($('.shipped-items :checked'), function(k, v){
            $('.export-form').find('input[name="send-items[]"][value="'+$(v).data('item_id')+'"]').remove();
        });

        //checked and disable items
        $.each($('.setShipped :checked'), function(k, v){
            $(v).attr('checked','checked').prop('disabled', true).trigger('refresh');
            $(v).closest('tr').find('.shipped-items').attr('checked','checked').trigger('refresh');
        });

        return true;

    });

    $('input.setShipped').change(function(){
        if($('input.setShipped').length == $('input.setShipped:checked').length){
            $('.barcode-alert').text('De order is volledig gereed.').removeClass('alert-danger').removeClass('hidden').addClass('alert-success');
        }
    });
</script>
@endpush