@extends('admin.layouts.app')

@section('content')

    <div class="main" id="main">
        <div class="container">
            @include('flash::message')
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-custom">

                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-5 col-xxs-12 text-center-xxs">
                                        <div class="title">
                                            <h3>Profiel bewerken</h3>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="panel-body">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            {!! Form::open(['route' => 'profileUpdate','method' => 'POST']) !!}
                                <div class="form-group">
                                    <label for="label_name" class="control-label">Naam:</label>
                                    <input type="text" class="form-control" value="{!! Auth::guard('admin')->user()->name !!}" name="name">
                                </div>

                                <div class="form-group">
                                    <label for="label_name" class="control-label">E-mail:</label>
                                    <input type="email" class="form-control" value="{!! Auth::guard('admin')->user()->email !!}" name="email">
                                </div>

                                <div class="form-group">
                                    <label for="label_name" class="control-label">Wachtwoord:</label>
                                    <input type="password" class="form-control"  name="new_password">
                                </div>

                                <div class="form-group">
                                    <label for="label_name" class="control-label">Herhaal wachtwoord:</label>
                                    <input type="password" class="form-control"  name="new_password_repeat">
                                </div>

                                <input type="submit" class="btn btnorangeTransp btn-sm" value="Opslaan">

                             {!! Form::close() !!}

                            </div>
                        </form>
                    </div><!-- .panel -->
                </div>
            </div>
        </div>
    </div><!-- end .main -->
@endsection