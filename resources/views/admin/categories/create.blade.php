@extends('admin.layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link href="https://nightly.datatables.net/select/css/select.dataTables.css?_=766c9ac11eda67c01f759bab53b4774d.css" rel="stylesheet" type="text/css" />
    <style>
        .form-control{
            color: #434343;
        }
    </style>
    <div class="main" id="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="block-title">
                        <div class="row">
                            <div class="col-sm-6">

                            </div>
                            <div class="col-sm-6 text-sm-right">
                                {{--<a href="#" class="btn btn-success">TERUG</a>--}}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                @if ($errors->any() || Session::has('error'))
                                    <div class="alert alert-danger">
                                        <ul>
                                            @if(Session::has('error'))
                                                <li>{{Session::get('error')}}</li>
                                            @endif
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                
                                <div class="panel panel-custom">
                                    <div class="panel-heading">
                                        <div class="title">
                                            <h3>Maak een nieuwe categorie</h3>
                                        </div>
                                        <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel1" aria-expanded="true" aria-controls="collapsePanel1"></span>
                                    </div>
                                    <div class="collapse in" id="collapsePanel1">
                                        <div class="panel-body">
                                            <form action="{{route('categories.store')}}" method="post">
                                                @csrf()
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Toestand:</label>
                                                    <select name="is_active" id="is_active" class="form-control">
                                                        <option value="1" selected>Actief</option>
                                                        <option value="0">Inactief</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Categorie naam: <span style="color:red">*</span></label>
                                                    <input type="text" name="name" class="form-control" required value="{!! old('name') !!}">
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-sm btn-primary">Opslaan</button>
                                                    {{-- <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modalAssignProducts">Product koppelen aan categorie</button> --}}
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div><!-- .panel -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- end .main -->
<script>
    (function($){
        $(document).ready(function() {
            
        });
    })(jQuery);
</script>
@endsection

