@extends('admin.layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link href="https://nightly.datatables.net/select/css/select.dataTables.css?_=766c9ac11eda67c01f759bab53b4774d.css" rel="stylesheet" type="text/css" />
    <style>
        .form-control{
            color: #434343;
        }
    </style>
    <div class="main" id="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="block-title">
                        <div class="row">
                            <div class="col-sm-6">

                            </div>
                            <div class="col-sm-6 text-sm-right">
                                {{--<a href="#" class="btn btn-success">TERUG</a>--}}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                @if ($errors->any() || Session::has('error'))
                                    <div class="alert alert-danger">
                                        <ul>
                                            @if(Session::has('error'))
                                                <li>{{Session::get('error')}}</li>
                                            @endif
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                
                                <div class="panel panel-custom">
                                    <div class="panel-heading">
                                        <div class="title">
                                            <h3>{!! $category->name !!}</h3>
                                        </div>
                                        <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel1" aria-expanded="true" aria-controls="collapsePanel1"></span>
                                    </div>
                                    <div class="collapse in" id="collapsePanel1">
                                        <div class="panel-body">
                                            <form action="{{route('categories.update')}}" method="post">
                                                @csrf()
                                                <input type="hidden" name="category_id" value="{{$category->category_id}}">
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Toestand:</label>
                                                    <select name="is_active" id="is_active" class="form-control">
                                                        @if ($category->is_active)
                                                            <option value="1" selected>Actief</option>
                                                            <option value="0">Inactief</option>
                                                        @else
                                                            <option value="1">Actief</option>
                                                            <option value="0" selected>Inactief</option>
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Categorie naam: <span style="color:red">*</span></label>
                                                    <input type="text" name="name" class="form-control" required value="{!! $category->name !!}">
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Lijst Boeken:</label>
                                                    <br>
                                                    <div class="">
                                                        <table class="list_products">
                                                            <thead>
                                                                <tr>
                                                                    <th>ID</th>
                                                                    <th>Product</th>
                                                                    <th>ISBN</th>
                                                                    <th>STOCK</th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($list_products as $index => $product)
                                                                <tr>
                                                                    <td>{!! $product->id !!}</td>
                                                                    <td class="">{!! $product->name !!}</td>
                                                                    <td>
                                                                        {!! $product->isbn !!}
                                                                    </td>
                                                                    <td>
                                                                        {!! $product->reserve_from_stock !!}
                                                                    </td>
                                                                    <td>
                                                                        <a href="{!! URL::route('producten.show',$product->id) !!}" class="btn txt-orange btn-sm">Bekijken</a>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-sm btn-primary">Opslaan</button>
                                                    <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modalAssignProducts">Product koppelen aan categorie</button>
                                                </div>
                                            </form>
                                            <form action="{{route('categories.delete')}}" method="post">
                                                @csrf()
                                                <input type="hidden" name="category_id" value="{{$category->category_id}}">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-danger">Verwijderen</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div><!-- .panel -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- end .main -->


    <!-- Modal assign products -->
<div class="modal fade" id="modalAssignProducts" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-xdev modal-dialog-centered" role="document">
        <div class="modal-content xdev-table">
            <form id="formAssign" action="{{ route('categories.assignproducts') }}" method="post">
                @csrf
                <input type="hidden" id="category_id" name="category_id" value="{{$category->category_id}}">
                <input type="hidden" id="product_id" name="product_id" value="">
                <div class="modal-header">
                    <h5 class="modal-title text-uppercase" id="exampleModalLongTitle">Koppel producten aan deze categorie</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table id="data-table-products" class="table table-sc-ex">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Product</th>
                                <th>ISBN</th>
                                <th>CATEGORIE</th>
                                <th>STOCK</th>
                                <th>
                                    <label class="checkbox-inline">
                                        <input class="check-all" type="checkbox" value="">
                                    </label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- @foreach ($all_products as $index => $product)
                            @php
                                $checked = 0;
                                $selected = "";
                            @endphp
                            @foreach ($list_products as $index => $pro)
                                @php
                                    if($pro->id == $product->id){
                                        $selected = "selected";
                                        $checked = 1;
                                    }
                                @endphp
                            @endforeach
                            <tr class="{{$selected}}">
                                <td>{!! $product->id !!}</td>
                                <td class="">{!! $product->name !!}</td>
                                <td>
                                    {!! $product->isbn !!}
                                </td>
                                <td>
                                    {!! $product->category_name !!}
                                </td>
                                <td>
                                    {!! $product->reserve_from_stock !!}
                                </td>
                                <td class="check"><span class="hidden">{{$checked}}</span></td>
                            </tr>
                            @endforeach --}}
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuleren</button>
                    <button type="submit" value="1" name="assign_product" class="btn btn-primary">Opslaan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://nightly.datatables.net/select/js/dataTables.select.js?_=766c9ac11eda67c01f759bab53b4774d"></script>
<script>
    (function($){
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name="csrf-token"]').val()
                }
            });

            $(".list_products").DataTable({
                "language": {
                    "decimal":        "",
                    "emptyTable":     "Geen data beschikbaar in de tabel",
                    "info":           "Toon _START_ tot _END_ van _TOTAL_ items",
                    "infoEmpty":      "Toon 0 van 0 van 0 items",
                    "infoFiltered":   "(gefilterd uit _MAX_ totaal inzendingen)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Toon _MENU_ items",
                    "loadingRecords": "Bezig met laden...",
                    "processing":     "Verwerken...",
                    "search":         "Zoeken:",
                    "zeroRecords":    "Geen overeenkomende records gevonden",
                    "paginate": {
                        "first":      "Eerste",
                        "last":       "Laatste",
                        "next":       "Volgende",
                        "previous":   "Vorige"
                    },
                    "aria": {
                        "sortAscending":  ": activeren om kolom oplopend te sorteren",
                        "sortDescending": ": activeren om kolom aflopend te sorteren"
                    },
                    buttons: {
                        colvis: 'Kolomzichtbaarheid'
                    }
                },
            });

            var dataTableProducts = $("#data-table-products").DataTable({
                'select':{
                    style:    'multi',
                    selector: 'td:last-child'
                },
                "language": {
                    "decimal":        "",
                    "emptyTable":     "Geen data beschikbaar in de tabel",
                    "info":           "Toon _START_ tot _END_ van _TOTAL_ items",
                    "infoEmpty":      "Toon 0 van 0 van 0 items",
                    "infoFiltered":   "(gefilterd uit _MAX_ totaal inzendingen)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Toon _MENU_ items",
                    "loadingRecords": "Bezig met laden...",
                    "processing":     "Verwerken...",
                    "search":         "Zoeken:",
                    "zeroRecords":    "Geen overeenkomende records gevonden",
                    "paginate": {
                        "first":      "Eerste",
                        "last":       "Laatste",
                        "next":       "Volgende",
                        "previous":   "Vorige"
                    },
                    "aria": {
                        "sortAscending":  ": activeren om kolom oplopend te sorteren",
                        "sortDescending": ": activeren om kolom aflopend te sorteren"
                    },
                    buttons: {
                        colvis: 'Kolomzichtbaarheid'
                    }
                },
                "ajax": {
                    "url": "/admin/getProductsHasChecked/<?php echo $category->category_id?>",
                    "dataSrc": function ( json ) {
                        return json;
                    },
                },
                "createdRow": function(row,data,dataIndex) {
                    if(data.selected == "selected"){
                        $(row).addClass('selected');
                    }
                },
                "initComplete":function( settings, json){
                    dataTableProducts.rows('.selected').select();

                    var getDataRows = function(){
                        var data = dataTableProducts.rows({selected:  true}).data();
                        /* console.log(data); */
                        if(data.length == 0){
                            modalAssignProducts.find("#product_id").val('');
                        }
                        var productIdArray=[];      
                        for (var i=0; i < data.length ;i++){
                            /* console.log(data[i].id); */
                            productIdArray.push(data[i].id);
                        }
                        var sProductIdData = productIdArray.join();
                        modalAssignProducts.find("#product_id").val(sProductIdData);
                    };
                    dataTableProducts.on( 'select', function ( e, dt, type, indexes ) {
                        if (type === 'row'){
                            getDataRows();
                        }
                    });
                    dataTableProducts.on( 'deselect', function ( e, dt, type, indexes ) {
                        if (type === 'row'){
                            getDataRows();
                        }
                    });

                    $(".check-all").on( "click", function(e) {
                    if ($(this).is( ":checked" )) {
                        dataTableProducts.rows().select();        
                    } else {
                        dataTableProducts.rows().deselect(); 
                    }
                        getDataRows();
                    });

                    var modalAssignProducts = $("#modalAssignProducts");
                    
                    getDataRows();

                },
                "columnDefs": [
                    {
                        orderable: true,
                        className: 'select-checkbox',
                        targets:   5
                    },
                    {
                        "targets": 0,
                        "data": "id",
                        "name": "id"
                    },
                    {
                        "targets": 1,
                        "data": "name",
                        "name": "name"
                    },
                    {
                        "targets": 2,
                        "data": "isbn",
                        "name": "isbn"
                    },
                    {
                        "targets": 3,
                        "data": "category_name",
                        "name": "category_name"
                    },
                    {
                        "targets": 4,
                        "data": "reserve_from_stock",
                        "name": "reserve_from_stock"
                    },
                    {
                        "targets": 5,
                        'render': function (data, type, full, meta) {
                            return '<span class="hidden">'+ full.checked +'</span>';
                        }
                    }
                ],
            });

            
            

        });
    })(jQuery);
</script>
@endsection

