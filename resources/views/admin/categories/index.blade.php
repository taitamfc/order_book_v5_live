@extends('admin.layouts.app')

@section('content')
<div class="main" id="main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="title">
                            <h3>Categorieën
                                <a class="btn btn-sm btn-default pull-right" href="{{route('categories.create')}}">Creëren</a>
                            </h3>
                        </div>
                    </div>
                    
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form action="{!! URL::route('categories.index') !!}" method="GET" id="searchFormStudents">
                            <div class="row row10">
                                <div class="col-lg-1 col-sm-4 col-xs-2 col-xxs-12 form-group">
                                    {!! Form::text('category_id',isset($_GET['category_id']) ? $_GET['category_id'] : null,['class' => 'form-control brdRadius','placeholder' => 'ID ..']) !!}
                                </div>
                                <div class="col-lg-2 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                    {!! Form::text('name',isset($_GET['name']) ? $_GET['name'] : null,['class' => 'form-control brdRadius','placeholder' => 'Naam ..']) !!}
                                </div>
                                <div class="col-xs-1 col-xxs-12 form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <a href="{{ route('categories.index') }}"><span class="glyphicon glyphicon-refresh"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" style="position: absolute;left: -9999px;"></button>
                            </div>
                        </form>

                        <div class="table-responsive">
                            <table class="table table-custom small-font">
                                <tr>
                                    <th>ID</th>
                                    <th>Categorie</th>
                                    <th>Bovenliggende Categorie ID</th>
                                    <th>Toestand</th>
                                    <th class="th-flex">Actie</th>
                                </tr>
                                @forelse($categories as $category)
                                    @php
                                        $is_active = "Actief";
                                        if(!$category->is_active){
                                            $is_active = "Inactief";
                                        }
                                    @endphp
                                    <tr>
                                        <td>{!! $category->category_id !!}</td>
                                        <td class="">{!! $category->name !!}</td>
                                        <td class="">{!! $category->parent_id !!}</td>
                                        <td class="">{!! $is_active !!}</td>
                                        <td>
                                            <a href="{!! URL::route('categories.show',$category->category_id) !!}" class="btn txt-orange btn-sm">Bekijken</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <h3 style="text-align: center">Er zijn geen resultaten gevonden om te tonen.</h3>
                                    </tr>
                                @endforelse
                            </table>
                            <div class="pagination">
                                {!! $categories->appends($filter)->render() !!}
                            </div>
                        </div>

                    </div>
                </div><!-- .panel -->
            </div>
        </div>
    </div>
</div><!-- end .main -->

@endsection