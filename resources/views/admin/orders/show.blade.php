@extends('admin.layouts.app')

@section('content') 
    	<div class="main" id="main">
    		<div class="container">
    			<div class="row">
    				<div class="col-xs-12">
                        @include('flash::message')
                        {{--<div class="block-title">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-sm-6">--}}
                                    {{--<h2>Bestelling</h2>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-6 text-sm-right">--}}
                                    {{--<a href="{!! URL::route('orders.index') !!}" class="btn btn-success">TERUG</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::open(['route' => ['orders.update', $order->order_id],'method' => 'POST']) !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-custom">
                                            <div class="panel-heading">
                                                <div class="row">
                                                    <div class="col-xs-8 col-xxs-12 text-center-xxs">
                                                        <div class="title">
                                                            <h3>Status</h3>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4 col-xxs-12 text-xs-right">
                                                        <button class="btn btn-info btn-xxs-block" type="submit">OPSLAAN</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="collapse in" id="collapsePanel1">
                                                <div class="panel-body">
                                                    <div class="form-group">
                                                        <select name="status" id="status" class="form-control">
                                                            @if ($order->status == "pending")
                                                                <option value="pending" selected>In Behandeling</option>
                                                                <option value="complete">Afgerond</option>
                                                            @else
                                                                <option value="pending">In Behandeling</option>
                                                                <option value="complete" selected>Afgerond</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        
                                        <input type="hidden" name="id" value="{{$order->order_id}}">
                                        <div class="panel panel-custom">
                                            <div class="panel-heading">
                                                <div class="row">
                                                    <div class="col-xs-8 col-xxs-12 text-center-xxs">
                                                        <div class="title">
                                                            <h3>Persoonlijke informatie</h3>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4 col-xxs-12 text-xs-right">
                                                        <button class="btn btn-info btn-xxs-block" type="submit">OPSLAAN</button>
                                                    </div>
                                                </div>
                                                {{--<span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel1" aria-expanded="true" aria-controls="collapsePanel1"></span>--}}
                                            </div>
                                            <div class="collapse in" id="collapsePanel1">
                                                <div class="panel-body">
                                                    <div class="form-group">
                                                        <label for="username" class="control-label">Studentnummer:</label>
                                                        <input type="text" disabled name="username" id="username" class="form-control" value="{{ $order->student->username }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="firstname" class="control-label">Voornaam:</label>
                                                        <input type="text" name="firstname" id="firstname" class="form-control" value="{{ $order->firstname }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="middlename" class="control-label">Tussenvoegsel:</label>
                                                        <input type="text" name="middlename" id="middlename" class="form-control" value="{{ $order->middlename }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="lastname" class="control-label">Achternaam:</label>
                                                        <input type="text" name="lastname" id="lastname" class="form-control" value="{{ $order->lastname }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email" class="control-label">E-mailadres:</label>
                                                        <input type="email" name="email" id="email" class="form-control" value="{{ $order->email }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="telephone" class="control-label">Telefoonnummer:</label>
                                                        <input type="tel" name="telephone" id="telephone" class="form-control" value="{{ $order->telephone }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="street" class="control-label">Straat:</label>
                                                        <input type="text" name="street" id="street" class="form-control" value="{{ $order->street }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="postcode" class="control-label">Postcode:</label>
                                                        <input type="text" name="postcode" id="postcode" class="form-control" value="{{ $order->postcode }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="city" class="control-label">Plaats:</label>
                                                        <input type="text" name="city" id="city" class="form-control" value="{{ $order->city }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- .panel -->
                                    
                                    </div>
                                </div>
                                {!! Form::close() !!}

                                {{--<div class="panel panel-custom">--}}
                                    {{--<div class="panel-heading">--}}
                                        {{--<div class="title">--}}
                                            {{--<h3>PERSOONLIJKE INFORMATIE</h3>--}}
                                        {{--</div>--}}
                                        {{--<span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel2" aria-expanded="true" aria-controls="collapsePanel2"></span>--}}
                                    {{--</div>--}}
                                    {{--<div class="collapse in" id="collapsePanel2">--}}
                                        {{--<div class="panel-body">--}}
                                            {{--<form action="#" method="post">--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<label for="label_name" class="control-label">E-mailadres:</label>--}}
                                                    {{--<input type="email" class="form-control" value="{{ $order->email }}">--}}
                                                {{--</div>--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<label for="label_name" class="control-label">Telefoonnummer:</label>--}}
                                                    {{--<input type="tel" class="form-control" value="{{ $order->telephone }}">--}}
                                                {{--</div>--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<label for="label_name" class="control-label">Bankrekeningnummer:</label>--}}
                                                    {{--<input type="text" class="form-control" value="{{ $order->bank_account }}">--}}
                                                {{--</div>--}}
                                            {{--</form>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div><!-- .panel -->--}}
                                {{--<div class="panel panel-custom">--}}
                                    {{--<div class="panel-heading">--}}
                                        {{--<div class="title">--}}
                                            {{--<h3>ADRES</h3>--}}
                                        {{--</div>--}}
                                        {{--<span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel3" aria-expanded="true" aria-controls="collapsePanel3"></span>--}}
                                    {{--</div>--}}
                                    {{--<div class="collapse in" id="collapsePanel3">--}}
                                        {{--<div class="panel-body">--}}
                                            {{--<form action="#" method="post">--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<label for="label_name" class="control-label">Straat:</label>--}}
                                                    {{--<input type="text" class="form-control" value="{{ $order->street }}">--}}
                                                {{--</div>--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<label for="label_name" class="control-label">Postcode:</label>--}}
                                                    {{--<input type="text" class="form-control" value="{{ $order->postcode }}">--}}
                                                {{--</div>--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<label for="label_name" class="control-label">Plaats:</label>--}}
                                                    {{--<input type="text" class="form-control" value="{{ $order->city }}">--}}
                                                {{--</div>--}}
                                            {{--</form>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div><!-- .panel -->--}}
                            </div>
                            <div class="col-md-8">
                                <div class="panel panel-custom">
                                    <div class="panel-heading">
                                        <div class="title">
                                            <h3>Lesbundels</h3>
                                        </div>
                                        <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel4" aria-expanded="true" aria-controls="collapsePanel4"></span>
                                    </div>
                                    <div class="collapse in" id="collapsePanel4">
                                        <div class="panel-body">
                                            @if ( isset($order->items['lesbundels']) )
                                                <div class="table-responsive">
                                                    <table class="table table-custom">
                                                        <tr>
                                                            <th class="th-two">Modulecode</th>
                                                            <th class="th-two">Druk</th>
                                                            <th>Titel</th>
                                                            <th class="th-price align-r">Prijs</th>
                                                        </tr>
                                                        @foreach($order->items['lesbundels'] as $item)
                                                            <tr>
                                                                <td>{{ $item->product->modulecode }}</td>
                                                                <td>{{ $item->product->druk }}</td>
                                                                <td>{{ $item->product->name }}</td>
                                                                <td class="align-r">€ {{ \App\Helpers\Custom::money_format($item->price) }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
                                            @else 
                                                <p><em>Er zijn geen lesbundels besteld door de student.</em></p>
                                            @endif
                                        </div>
                                    </div>
                                </div><!-- .panel -->
                                <div class="panel panel-custom">
                                    <div class="panel-heading">
                                        <div class="title">
                                            <h3>Commerciële boeken</h3>
                                        </div>
                                        <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel5" aria-expanded="true" aria-controls="collapsePanel5"></span>
                                    </div>
                                    <div class="collapse in" id="collapsePanel5">
                                        <div class="panel-body">
                                            @if (isset($order->items['commerciele-boeken']))
                                                <div class="table-responsive">
                                                    <table class="table table-custom">
                                                        <tr>
                                                            <th class="th-two">ISBN</th>
                                                            <th class="th-two">Druk</th>
                                                            <th>Titel</th>
                                                            <th>Auteur</th>
                                                            <th class="th-price align-r">Prijs</th>

                                                        </tr>
                                                        @foreach($order->items['commerciele-boeken'] as $item)
                                                            <tr>
                                                                <td>{{ $item->product->isbn }}</td>
                                                                <td>{{ $item->product->druk }}</td>
                                                                <td>{{ $item->product->name }}</td>
                                                                <td>{{ $item->product->provider }}</td>
                                                                <td class="align-r">€ {{ \App\Helpers\Custom::money_format($item->price) }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
                                            @else 
                                                <p><em>Er zijn geen commerciële boeken besteld door de student.</em></p>
                                            @endif
                                        </div>
                                    </div>
                                </div><!-- .panel -->
                                <div class="panel panel-custom">
                                    <div class="panel-heading">
                                        <div class="title">
                                            <h3>Keuzepakketten</h3>
                                        </div>
                                        <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel6" aria-expanded="true" aria-controls="collapsePanel6"></span>
                                    </div>
                                    <div class="collapse in" id="collapsePanel6">
                                        <div class="panel-body panel-content">

                                            @if (isset($order->items['keuzepakketten']))
                                                <div class="table-responsive">
                                                    <table class="table table-custom">
                                                        <tr>
                                                            <th class="th-two">ISBN</th>
                                                            <th class="th-two">Druk</th>
                                                            <th>Titel</th>
                                                            <th>Auteur</th>
                                                            <th class="th-price align-r">Prijs</th>
                                                        </tr>
                                                        @foreach($order->items['keuzepakketten'] as $item)
                                                            <tr>
                                                                <td>{{ $item->product->isbn }}</td>
                                                                <td>{{ $item->product->druk }}</td>
                                                                <td>{{ $item->product->name }}</td>
                                                                <td>{{ $item->product->provider }}</td>
                                                                <td class="align-r">€ {{ \App\Helpers\Custom::money_format($item->price) }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
                                            @else
                                                <p><em>Er zijn geen keuzepakketten besteld door de student.</em></p>
                                            @endif

                                        </div>
                                    </div>
                                </div><!-- .panel -->
                                <div class="panel panel-custom">
                                    <div class="panel-heading">
                                        <div class="title">
                                            <h3>Back2School</h3>
                                        </div>
                                        <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel6" aria-expanded="true" aria-controls="collapsePanel6"></span>
                                    </div>
                                    <div class="collapse in" id="collapsePanel6">
                                        <div class="panel-body panel-content">

                                            @if (isset($order->items['back2school']))
                                                <div class="table-responsive">
                                                    <table class="table table-custom">
                                                        <tr>
                                                            <th class="th-two">ISBN</th>
                                                            <th class="th-two">Druk</th>
                                                            <th>Titel</th>
                                                            <th>Auteur</th>
                                                            <th class="th-price align-r">Prijs</th>
                                                        </tr>
                                                        @foreach($order->items['back2school'] as $item)
                                                            <tr>
                                                                <td>{{ $item->product->isbn }}</td>
                                                                <td>{{ $item->product->druk }}</td>
                                                                <td>{{ $item->product->name }}</td>
                                                                <td>{{ $item->product->provider }}</td>
                                                                <td class="align-r">€ {{ \App\Helpers\Custom::money_format($item->price) }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
                                            @else
                                                <p><em>Er zijn geen pretpakketten besteld door de student.</em></p>
                                            @endif

                                        </div>
                                    </div>
                                </div><!-- .panel -->                                
                            </div>
                        </div>
                    </div>
                </div>
    		</div>
    	</div><!-- end .main -->
@endsection    	