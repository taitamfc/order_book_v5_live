@extends('admin.layouts.app')

@section('content')    	

        <div class="main" id="main">
    		<div class="container">
                @include('flash::message')
    			<div class="row">
                    <form action="{!! URL::route('orders.index') !!}" method="GET" id="ordersForm">
                    <div class="col-md-2" style="float: left; margin-bottom: 25px;">
                        {!! Form::select('per_page',['20' => '20','50' => '50','100' => '100' ,'200' => '200','400' => '400'],isset($_GET['per_page']) ? $_GET['per_page'] : '100',['class' => 'form-control filterSelect']) !!}
                    </div>
    				<div class="col-xs-12">
                        <div class="panel panel-custom">
                                <input type="hidden" name="batch" value="0">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-5 col-xxs-12 text-center-xxs">
                                            <div class="title">
                                                <h3>Bestellingen</h3>
                                            </div>
                                        </div>
                                        <div class="col-xs-7 col-xxs-12 text-xs-right">
                                            <button type="button" class="btn btn-info btn-xxs-block create-batch">BATCH MAKEN VAN SELECTIE</button>
                                            <button type="submit" style="position: absolute;left: -9999px;"></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">

                                    <div class="row row10">
                                        <div class="col-lg-2 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon check">
                                                    <label class="checkbox-label empty">
                                                        <input type="checkbox" class="styled checkbox-custom checkAll" >
                                                    </label>
                                                </div>
                                                <input type="text" class="form-control" placeholder="ID .." name="id" @if(isset($_GET['id'])) value="{!! $_GET['id'] !!}" @endif>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                            <input type="text" class="form-control brdRadius" placeholder="Studentcode .." name="username" @if(isset($_GET['username'])) value="{!! $_GET['username'] !!}" @endif>
                                        </div>
                                        <div class="col-lg-2 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                            <input type="text" class="form-control brdRadius" placeholder="Naam .." name="name" @if(isset($_GET['name'])) value="{!! $_GET['name'] !!}" @endif>
                                        </div>
                                        <div class="col-lg-1 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                            {!! Form::select('group',$groups->pluck('customer_group_code','customer_group_id'),isset($_GET['group']) ? $_GET['group'] : null,['class' => 'form-control filterSelect jquerySelect','placeholder' => '....']) !!}
                                        </div>
                                        <div class="col-lg-6 col-sm-8 col-xs-12">
                                            <div class="row row10">
                                                <div class="col-xs-4 col-xxs-12 form-group">
                                                    <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                                        <input type="text" name="from" class="form-control DateFromOrder" placeholder="Van .." name="name" @if(isset($_GET['from'])) value="{!! $_GET['from'] !!}" @endif>
                                                        <div class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-xxs-12 form-group">
                                                    <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                                        <input type="text" name="until" class="form-control DateUntilOrder" placeholder="Tot .." @if(isset($_GET['until'])) value="{!! $_GET['until'] !!}" @endif>
                                                        <div class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                                    <select name="status" id="status" class="form-control">
                                                        @if (isset($_GET['status']))
                                                            @if ($_GET['status'] == "pending")
                                                                <option value="pending" selected>In Behandeling</option>
                                                                <option value="complete">Afgerond</option>
                                                            @else
                                                                <option value="pending">In Behandeling</option>
                                                                <option value="complete" selected>Afgerond</option>
                                                            @endif
                                                        @else
                                                            <option value="pending">In Behandeling</option>
                                                            <option value="complete" selected>Afgerond</option>
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="col-xs-1 col-xxs-12 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <a href="{{ route('orders.index') }}" class="refresh"><span class="glyphicon glyphicon-refresh"></span></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-custom">
                                            <tr>
                                                <th class="th-flex">&nbsp;</th>
                                                <th><a href="{{ route('orders.index') . Request::has('order_id') && trim(Request::get('order_id')) == 'asc' ? '?order_id=desc' : '?order_id=asc' }}">ID</a></th>
                                                <th><a href="{{ route('orders.index') . Request::has('studentcode') && trim(Request::get('studentcode')) == 'asc' ? '?studentcode=desc' : '?studentcode=asc' }}">Studentcode</a></th>
                                                <th><a href="{{ route('orders.index') . Request::has('naam') && trim(Request::get('naam')) == 'asc' ? '?naam=desc' : '?naam=asc' }}">Naam</a></th>
                                                <th><a href="{{ route('orders.index') . Request::has('customer_group_code') && trim(Request::get('customer_group_code')) == 'asc' ? '?customer_group_code=desc' : '?customer_group_code=asc' }}">Groep</a></th>
                                               
                                                <th><a href="{{ route('orders.index') . Request::has('ordersupdate') && trim(Request::get('ordersupdate')) == 'asc' ? '?ordersupdate=desc' : '?ordersupdate=asc' }}">Besteldatum</a></th>
                                                 <th><a href="{{ route('orders.index') . Request::has('orderStatus') && trim(Request::get('orderStatus')) == 'asc' ? '?orderStatus=desc' : '?orderStatus=asc' }}">Status</a></th>
                                                <th class="th-flex">Actie</th>
                                            </tr>

                                            @foreach($orders as $order)
                                                @php
                                                    if($order->status == "pending"){
                                                        $order->status = "In Behandeling";
                                                    }
                                                    if($order->status == "complete"){
                                                        $order->status = "Afgerond";
                                                    }
                                                @endphp
                                                <tr>
                                                    <td>
                                                        <label class="checkbox-label empty">
                                                            <input type="checkbox" class="styled checkbox-custom orderId" name="order_ids[]" value="{{ $order->order_id }}">
                                                        </label>
                                                    </td>
                                                    <td class="td-nowrap">{{ $order->order_id}}</td>
                                                    <td>{{ $order->student ? $order->student->username : '' }}</td>
                                                    <td class="td-nowrap">{{ $order->firstname }} {{ $order->lastname }}</td>
                                                    <td>{{ $order->customer_group_code }}</td>
                                                    <td class="td-nowrap">{{ date('d/m/Y - H:i',strtotime($order->created_at)) }}</td>
                                                    <td>{{ $order->status }}</td>
                                                    <td>
                                                        <a href="{!! route('orders.show',[$order->order_id]) !!}" class="txt-orange">Bekijken</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>

                                    </div>

                                    {{ $orders->appends($filter)->links() }}

                                    {{--<div class="col-md-2" style="margin-top: 25px;float: right;">--}}
                                    {{--{!! Form::select('per_page',['20' => '20','50' => '50','100' => '100' ,'200' => '200','400' => '400'],isset($_GET['per_page']) ? $_GET['per_page'] : '100',['class' => 'form-control filterSelect']) !!}--}}
                                    {{--</div>--}}

                                </div>
                        </div><!-- .panel -->
                    </div>
                    </form>
                </div>
    		</div>
    	</div><!-- end .main -->
@endsection

@push('scripts')
<script>
    $('.create-batch').click(function(){
        $('input[name="batch"]').val(1);
        $('#ordersForm').submit();
    });

    $("#status").change(function(){
        $('#ordersForm').submit();
    });
</script>
@endpush