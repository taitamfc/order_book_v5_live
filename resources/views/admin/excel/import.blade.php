@extends('admin.layouts.app')

@section('content')


    <div class="main" id="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-custom">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="title">
                                        <h3>Importeren</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                @if ($errors->any() || Session::has('error'))
                                    <div class="alert alert-danger">
                                        <ul>
                                            @if(Session::has('error'))
                                                <li>{{Session::get('error')}}</li>
                                            @endif
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @if (session()->has('result'))
                                    <div class="alert alert-info" style="width:100%;text-align: left">
                                        <ul>
                                            @php
                                                $result = \Session::get('result');
                                            @endphp
                                            <li>Totaal rijen: {{$result['total_rows']}}</li>
                                            <li class="text-success">Succes: {{$result['success']}}</li>
                                            <li class="text-warning">Duplicaat: {{$result['duplicate']}}</li>
                                            <li class="text-warning" style="word-break: break-word;">Dubbele rijen: {{json_encode($result['rows_duplicate'])}}</li>
                                            <li class="text-danger">Fout: {{$result['error']}}</li>
                                            <li class="text-danger" style="word-break: break-word;">Kolom: {{json_encode($result['rows_error'])}}</li>
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="panel-body">
                            <form action="{!! URL::route('excel.import.submit') !!}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row row10">
                                    <div class="col-sm-2 form-group">
                                        <select name="import-type" id="import-type" class="form-control">
                                            <option value="1" link-template="/import/templates/import_books_template.xlsx">Producten</option>
                                            <option value="2" link-template="/import/templates/import_customers_template.xlsx">Studenten</option>
                                            <option value="3" link-template="/import/templates/import_commerce_product_template.xlsx">Commerciële Boeken</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2 form-group">
                                        <button type="button" class="bt-download-template btn btn-sm btn-default">Sjabloon Downloaden</button>
                                    </div>
                                </div>
                                <div class="row row10">
                                    <div class="col-sm-4 form-group">
                                        <input type="file" required name="import" id="import" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" class="form-control" value="" placeholder="klik om het bestand te selecteren...">
                                    </div>
                                    <div class="col-sm-2 form-group">
                                        <select name="group" id="group" class="form-control">
                                            <option value="0" selected>Alle groepen</option>
                                            @foreach ($groups as $group)
                                                <option value="{{$group->customer_group_code}}">{{$group->customer_group_code}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-2 form-group">
                                        <select name="category" id="category" class="form-control">
                                            <option value="0" selected>Alle categorieën</option>
                                            @foreach ($categories as $ctg)
                                                <option value="{{$ctg->category_id}}">{{$ctg->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-sm btn-primary">Importeren</button>
                            </form>
                        </div>
                    </div><!-- .panel -->
                </div>
            </div>
        </div>
    </div><!-- end .main -->
    
@endsection
@push('scripts')
    <script>
        (function($){
            $(document).ready(function() {
                $("button[type='submit']").click(function(){
                var $fileUpload = $("input[type='file']");
                    if (parseInt($fileUpload.get(0).files.length)>1){
                        // handle here
                        alert("Kies slechts één bestand!");
                    return false;
                    }
                });

                $(".bt-download-template").on("click",function(){
                    var linkDownload = $("#import-type option:selected").attr('link-template');
                    window.location.href = linkDownload;
                });

                $("#import-type").on("change",function(){
                    if($(this).val() == 2){
                        $("#category").attr("disabled","disabled");
                        $("#group").removeAttr("disabled");
                    }else{
                        $("#category").removeAttr("disabled");
                        $("#group").attr("disabled","disabled");
                    }
                });

                $("#category").on("change",function(){
                    if($(this).val() == '<?php echo env("BACK_2_SCHOOL_ID")?>'){
                        $("#group").attr("disabled","disabled");
                    }else{
                        $("#group").removeAttr("disabled");
                    }
                });
            });
        })(jQuery);
    </script>
@endpush