@extends('admin.layouts.app')

@section('content')

    <div class="main" id="main">
        <div class="container">
            @include('flash::message')
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-custom">

                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-5 col-xxs-12 text-center-xxs">
                                        <div class="title">
                                            <h3>CMS Pagina</h3>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="panel-body">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            {!! Form::open(['route' => 'cms.update','method' => 'POST']) !!}
                                <input type="hidden" name="id" value="{{$cms->id}}">
                                <div class="form-group">
                                    <label for="label_name" class="control-label">Ingeschakeld: <span style="color:red">*</span></label>
                                    <select name="is_enabled" id="is_enabled" class="form-control">
                                        @if ($cms->is_enabled)
                                            <option value="1" selected>Actief</option>
                                            <option value="0">Inactief</option>
                                        @else
                                            <option value="1">Actief</option>
                                            <option value="0" selected>Inactief</option>
                                        @endif
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="label_name" class="control-label">Titelpagina: <span style="color:red">*</span></label>
                                    <input type="text" name="title" class="form-control" required value="{!! $cms->title !!}">
                                </div>

                                <div class="form-group">
                                    <label for="label_name" class="control-label">Slug: <span style="color:red">*</span></label>
                                    <input type="text" name="slug" class="form-control" required value="{!! $cms->slug !!}">
                                </div>

                                <div class="form-group">
                                    <label for="label_name" class="control-label">Inhoud:</label>
                                    <textarea name="content" id="content" class="form-control" cols="30" rows="10">{!! $cms->content !!}</textarea>
                                </div>
 
                                <input type="submit" class="btn btn-sm btn-primary" value="Opslaan">

                             {!! Form::close() !!}

                            </div>
                        </form>
                    </div><!-- .panel -->
                </div>
            </div>
        </div>
    </div><!-- end .main -->
    <script src="/assets/plugins/ckeditor/ckeditor.js"></script>
	{{-- <script src="/js/ckfinder/ckfinder.js"></script> --}}
    <script>
        $(document).ready(function(){
            var editor = CKEDITOR.instances['content'];
        if (editor) { editor.destroy(true); }
            CKEDITOR.replace('content', {
                /* filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',
                filebrowserImageUploadUrl: "{{ asset('ckfinder/core/connctor/php/connector.php?command=QuickUpload&type=Images') }}", */
                enterMode: CKEDITOR.ENTER_BR,
                htmlEncodeOutput: true
            });
        });
    </script>
    {{-- @include('ckfinder::setup') --}}
@endsection
