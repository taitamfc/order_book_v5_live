@extends('admin.layouts.app')

@section('content')
<div class="main" id="main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="title">
                            <h3>CMS pagina
                                <a class="btn btn-sm btn-default pull-right" href="{{route('cms.create')}}">Creëren</a>
                            </h3>
                        </div>
                    </div>
                    
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-custom small-font">
                                <tr>
                                    <th>ID</th>
                                    <th>Titel</th>
                                    <th>Slug</th>
                                    <th>Toestand</th>
                                    <th class="th-flex">Actie</th>
                                </tr>
                                <tr>
                                    <td>0</td>
                                    <td class="">FAQ'S</td>
                                    <td class=""><a href="/faq">faq</a></td>
                                    <td class="">Actie</td>
                                    <td>
                                        <a href="{!! URL::route('faq') !!}" class="btn txt-orange btn-sm">Bekijken</a>
                                    </td>
                                </tr>
                                @foreach($cmsList as $cms)
                                    @php
                                        $is_enabled = "Actief";
                                        if(!$cms->is_enabled){
                                            $is_enabled = "Inactief";
                                        }
                                    @endphp
                                    <tr>
                                        <td>{!! $cms->id !!}</td>
                                        <td class="">{!! $cms->title !!}</td>
                                        <td class=""><a href="{!! route('cms.view',['slug' => $cms->slug]) !!}">{!!$cms->slug!!}</a></td>
                                        <td class="">{!! $is_enabled !!}</td>
                                        <td>
                                            <a href="{!! URL::route('cms.show',$cms->id) !!}" class="btn txt-orange btn-sm">Bekijken</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>

                    </div>
                </div><!-- .panel -->
            </div>
        </div>
    </div>
</div><!-- end .main -->

@endsection