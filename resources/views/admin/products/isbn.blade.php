@extends('admin.layouts.app')

@section('content')
<div class="main" id="main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="title">
                            <h3>Producten</h3>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form action="{!! URL::route('producten.isbn') !!}" method="GET" id="searchFormStudents">
                            <div class="row row10">
                                <div class="col-lg-2 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                    {!! Form::text('isbn',isset($_GET['isbn']) ? $_GET['isbn'] : null,['class' => 'form-control brdRadius','placeholder' => 'ISBN ..']) !!}
                                </div>
                                <div class="col-lg-2 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                    {!! Form::text('name',isset($_GET['name']) ? $_GET['name'] : null,['class' => 'form-control brdRadius','placeholder' => 'Naam ..']) !!}
                                </div>
                                <div class="col-lg-2 col-sm-4 col-xs-4 col-xxs-12 form-group">
                                    {!! Form::select('category',$categories->pluck('name','category_id'),isset($_GET['category']) ? $_GET['category'] : null,['class' => 'form-control filterSelect','placeholder' => 'Categorie ..']) !!}
                                </div>
                                <div class="col-lg-2 col-sm-4 col-xs-4 col-xxs-12 form-group">
                                    {!! Form::select('batch',$batches->pluck('batch_id', 'batch_id'),isset($_GET['batch']) ? $_GET['batch'] : null,['class' => 'form-control filterSelect','placeholder' => 'Batches ..']) !!}
                                </div>
                                <div class="col-lg-5 col-sm-8 col-xs-12 form-group">
                                    {!! Form::select('below_minimum',['1' => '1','2' => '2','3' => '4','5' => '5','6' => '6','7' => '7','8' => '9','10' => '10'],isset($_GET['below_minimum']) ? $_GET['below_minimum'] : null,['class' => 'form-control filterSelect','placeholder' => 'Toon enkel producten voorraad lager dan minimaal ..']) !!}
                                </div>
                                <div class="col-xs-1 col-xxs-12 form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <a href="{{ route('producten.index') }}"><span class="glyphicon glyphicon-refresh"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" style="position: absolute;left: -9999px;"></button>
                            </div>
                        </form>

                        <div class="table-responsive">
                            <table class="table table-custom small-font">
                                <tr>
                                    <th>ID</th>
                                    <th>Product</th>
                                    <th>ISBN</th>
                                    <th>Categorie</th>
                                    <th>Min. Voorraad</th>
                                    <th>Huidige Voorraad</th>
                                    <th>In bestelling(en)</th>
                                    <th>Gereserveerd</th>
                                    <th>Leverancier</th>
                                    <th class="th-flex">Actie</th>
                                </tr>
                                @forelse($products as $product)
                                    <tr>
                                        <td>{!! $product->product_id !!}</td>
                                        <td class="td-nowrap">{!! $product->name !!}</td>
                                        <td class="td-nowrap">{!! $product->isbn !!}</td>
                                        <td class="td-nowrap">{!! $product->category ? $product->category->name : '' !!}</td>
                                        <td>{!! $product->min_qty !!}</td>
                                        <td>{!! $product->qty !!}</td>
                                        <td>
                                            @if(Request::has('batch'))
                                                {!!  \Illuminate\Support\Facades\DB::table('order_items')->whereRaw('product_id IN ('.$product->products.')')->where('batch_id', Request::get('batch'))->count() !!}
                                            @else
                                                {!!  \Illuminate\Support\Facades\DB::table('order_items')->whereRaw('product_id IN ('.$product->products.')')->count() !!}
                                            @endif
                                        </td>
                                        <td>
                                            {!! $product->reserve_from_stock !!}
                                        </td>
                                        <td><a href="#" class="link">{!! $product->provider !!}</a></td>
                                        <td>
                                            <a href="{!! URL::route('producten.show',['id' => $product->id ]) !!}" class="btn txt-orange btn-sm">Bekijken</a>
                                        </td>
                                    </tr>

                                @empty
                                    <tr>
                                        <h3 style="text-align: center">Er zijn geen resultaten gevonden om te tonen.</h3>
                                    </tr>
                                @endforelse
                            </table>
                            <div class="pagination">

                                {!! $products->appends($filter)->render() !!}
                            </div>
                        </div>

                    </div>
                </div><!-- .panel -->
            </div>
        </div>
    </div>
</div><!-- end .main -->

@endsection