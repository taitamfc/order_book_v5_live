@extends('admin.layouts.app')

@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
@endpush

@section('content')
<div class="main" id="main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="title">
                            <h3>Producten</h3>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form action="{!! URL::route('producten.index') !!}" method="GET" id="searchFormStudents">
                            <div class="row row10">
                                <div class="col-lg-1 col-sm-4 col-xs-2 col-xxs-12 form-group">
                                    {!! Form::text('id',isset($_GET['id']) ? $_GET['id'] : null,['class' => 'form-control brdRadius','placeholder' => 'ID ..']) !!}
                                </div>
                                <div class="col-lg-2 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                    {!! Form::text('name',isset($_GET['name']) ? $_GET['name'] : null,['class' => 'form-control brdRadius','placeholder' => 'Naam ..']) !!}
                                </div>
                                <div class="col-lg-2 col-sm-4 col-xs-4 col-xxs-12 form-group">
                                    {!! Form::select('category',$categories->pluck('name','category_id'),isset($_GET['category']) ? $_GET['category'] : null,['class' => 'form-control filterSelect','placeholder' => 'Categorie ..']) !!}
                                </div>
                                <div class="col-lg-5 col-sm-8 col-xs-12 form-group">
                                    {!! Form::select('below_minimum',['1' => '1','2' => '2','3' => '4','5' => '5','6' => '6','7' => '7','8' => '9','10' => '10'],isset($_GET['below_minimum']) ? $_GET['below_minimum'] : null,['class' => 'form-control filterSelect','placeholder' => 'Toon enkel producten voorraad lager dan minimaal ..']) !!}
                                </div>
                                <div class="col-xs-1 col-xxs-12 form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <a href="{{ route('producten.index') }}"><span class="glyphicon glyphicon-refresh"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" style="position: absolute;left: -9999px;"></button>
                            </div>
                        </form>

                        <div class="table-responsive">
                            <table id="data-table" class="table table-custom small-font">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Product</th>
                                        <th>Categorie</th>
                                        <th>Min. Voorraad</th>
                                        <th>Huidige Voorraad</th>
                                        <th>Besteld uitgever(en)</th>
                                        <th>Nog te leveren</th>
                                        <th>Leverancier</th>
                                        <th class="th-flex">Actie</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($products as $product)
                                        <tr>
                                            <td>{!! $product->id !!}</td>
                                            <td class="">{!! $product->name !!}</td>
                                            <td class="">{!! $product->category ? $product->category->name : '' !!}</td>
                                            <td>{!! $product->min_qty !!}</td>
                                            <td>{!! $product->qty !!}</td>
                                            <td>
                                                {!!  \Illuminate\Support\Facades\DB::table('order_items')->where('product_id',$product->id)->count() !!}
                                            </td>
                                            <td>
                                                {!! $product->reserve_from_stock !!}
                                            </td>
                                            <td><a href="#" class="link">{!! $product->provider !!}</a></td>
                                            <td>
                                                <a href="{!! URL::route('producten.show',$product->id) !!}" class="btn txt-orange btn-sm">Bekijken</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- .panel -->
            </div>
        </div>
    </div>
</div><!-- end .main -->

@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    
<script>
    $(document).ready(function(){
        $("#data-table").DataTable({
            'paging': true,
            "pagingType": "simple_numbers",
            'lengthChange': true,
            'searching': true,
            'info': true,
            'autoWidth': false,
            'stateSave': true,
            "language": {
                "decimal":        "",
                "emptyTable":     "Geen data beschikbaar in de tabel",
                "info":           "Toon _START_ tot _END_ van _TOTAL_ items",
                "infoEmpty":      "Toon 0 van 0 van 0 items",
                "infoFiltered":   "(gefilterd uit _MAX_ totaal inzendingen)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Toon _MENU_ items",
                "loadingRecords": "Bezig met laden...",
                "processing":     "Verwerken...",
                "search":         "Zoeken:",
                "zeroRecords":    "Geen overeenkomende records gevonden",
                "paginate": {
                    "first":      "Eerste",
                    "last":       "Laatste",
                    "next":       "Volgende",
                    "previous":   "Vorige"
                },
                "aria": {
                    "sortAscending":  ": activeren om kolom oplopend te sorteren",
                    "sortDescending": ": activeren om kolom aflopend te sorteren"
                }
            },
            dom: 'Bfrtip',
            buttons: [
                {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7]
                    }
                },
                {
                    extend: 'csvHtml5',
                    exportOptions: {
                        columns: [0,1,2,3,4,5,6,7]
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [0,1,2,3,4,5,6,7]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [0,1,2,3,4,5,6,7]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0,1,2,3,4,5,6,7]
                    }
                },
            ],
        });
    });
</script>
@endpush
