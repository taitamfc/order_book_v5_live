@extends('admin.layouts.app')

@section('content')
    <style>
        .form-control{
            color: #434343;
        }
        
        .add-group{
            position: relative;
            display: inline-block;
        }
        .add-group button{
            position: absolute;
            top: 37%;
            left: 37%;
        }
    </style>
    <div class="main" id="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="block-title">
                        <div class="row">
                            <div class="col-sm-6">

                            </div>
                            <div class="col-sm-6 text-sm-right">
                                {{--<a href="#" class="btn btn-success">TERUG</a>--}}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                @if ($errors->any() || Session::has('error'))
                                    <div class="alert alert-danger">
                                        <ul>
                                            @if(Session::has('error'))
                                                <li>{{Session::get('error')}}</li>
                                            @endif
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                
                                <div class="panel panel-custom">
                                    <div class="panel-heading">
                                        <div class="title">
                                            <h3>{!! $product->name !!}</h3>
                                        </div>
                                        <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel1" aria-expanded="true" aria-controls="collapsePanel1"></span>
                                    </div>
                                    <div class="collapse in" id="collapsePanel1">
                                        <div class="panel-body">
                                            <form action="{{route('producten.update')}}" method="post">
                                                @csrf()
                                                <input type="hidden" name="id" value="{{$product->id}}">
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Toestand:</label>
                                                    <select name="status" id="status" class="form-control">
                                                        @if ($product->status)
                                                            <option value="1" selected>Actief</option>
                                                            <option value="0">Inactief</option>
                                                        @else
                                                            <option value="1">Actief</option>
                                                            <option value="0" selected>Inactief</option>
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Product Titel: <span style="color:red">*</span></label>
                                                    <input type="text" name="name" class="form-control" required value="{!! $product->name !!}">
                                                </div>

                                                @if ($product->category_id == 6)
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Afbeeldingen:</label>
                                                    <div class=""> 
                                                        <input type="hidden" name="image" id="image" data-parsley-required="true" class="form-control" value="{{$product->image}}">                      
                                                        <div class="add-group">
                                                            @if($product->image !="")
                                                                @if (strpos($product->image, 'userfiles') == false)
                                                                    <img id="img" class="img-responsive thumbnail" style="width: 200px" src="/media/{{$product->image}}"/>
                                                                @else
                                                                    <img id="img" class="img-responsive thumbnail" style="width: 200px" src="{{$product->image}}"/>
                                                                @endif
                                                            @else
                                                                <img id="img" class="img-responsive thumbnail" style="width: 200px" src="{{config('configadmin.noImg')}}"/>
                                                            @endif
                                                            <button type="button" class="bt-add-img" id="btnSelectImg"><img style="width:30px;" height="30px;" src="/assets/img/icons/icon-add.png"></button>
                                                        </div>   
                                                        <div class="clearfix"></div>  
                                                        <small class="f-s-12 text-grey-darker">Klik om een ​​afbeelding te kiezen</small>                  
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Prijs: <span style="color:red">*</span></label>
                                                    <input type="text" name="price" class="form-control" required value="{{$product->price}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Voorraad:</label>
                                                    <input type="number" name="reserve_from_stock" min="0" class="form-control" value="{{$product->reserve_from_stock}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Groep:</label>
                                                    <select name="customer_group_id" id="customer_group_id" class="form-control">
                                                        @if ($product->customer_group_id == 0)
                                                            <option value="0" selected>NULL</option>
                                                        @else
                                                            <option value="0">NULL</option>
                                                        @endif
                                                        @foreach ($groups as $group)
                                                            @if ($group->customer_group_id == $product->customer_group_id)
                                                                <option value="{{$group->customer_group_id}}" selected>{{$group->customer_group_code}}</option>
                                                            @else
                                                                <option value="{{$group->customer_group_id}}">{{$group->customer_group_code}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Categorie:</label>
                                                    <select name="category_id" id="category_id" class="form-control">
                                                        @foreach ($categories as $ctg)
                                                            @if ($ctg->category_id == $product->category_id)
                                                                <option value="{{$ctg->category_id}}" selected>{{$ctg->name}}</option>
                                                            @else
                                                                <option value="{{$ctg->category_id}}">{{$ctg->name}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">ISBN: <span style="color:red">*</span></label>
                                                    <input type="text" class="form-control" name="isbn" required value="{!! $product->isbn !!}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">SKU: </label>
                                                    <input type="text" class="form-control" name="sku" value="{!! $product->sku !!}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Druk:</label>
                                                    <input type="text" class="form-control" name="druk" value="{!! $product->druk !!}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Modulecode:</label>
                                                    <input type="text" class="form-control" name="modulecode" value="{!! $product->modulecode !!}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Moduulnaam:</label>
                                                    <input type="text" class="form-control" name="moduulnaam" value="{!! $product->moduulnaam !!}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Moduuldruk:</label>
                                                    <input type="text" class="form-control" name="moduuldruk" value="{!! $product->moduuldruk !!}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Paginas:</label>
                                                    <input type="number" min="1" class="form-control" name="paginas" value="{!! $product->paginas !!}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Afdeling:</label>
                                                    <input type="text" class="form-control" name="department" value="{!! $product->department !!}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Team:</label>
                                                    <input type="text" class="form-control" name="team" value="{!! $product->team !!}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Leverancier:</label>
                                                    <input type="text" class="form-control" name="provider" value="{!! $product->provider !!}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Auteur:</label>
                                                    <input type="text" class="form-control" name="writer" value="{!! $product->writer !!}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Order At:</label>
                                                    <input type="text" class="form-control" name="order_at" value="{!! $product->order_at !!}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Order Email Address:</label>
                                                    <input type="text" class="form-control" name="order_email_address" value="{!! $product->order_email_address !!}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="label_name" class="control-label">Leverbaar:</label>
                                                    <select name="not_delivery" id="not_delivery" class="form-control">
                                                        @if ($product->not_delivery)
                                                            <option value="1" selected>Actief</option>
                                                            <option value="0">Inactief</option>
                                                        @else
                                                            <option value="1">Actief</option>
                                                            <option value="0" selected>Inactief</option>
                                                        @endif
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-sm btn-primary">Opslaan</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div><!-- .panel -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- end .main -->
    <script src="/js/ckfinder/ckfinder.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>
    <script>
        (function($){
            $(document).ready(function() {
                $("#category_id").select2();
                $("#customer_group_id").select2();

                function selectFileWithCKFinder(elementId,elementImg) {
                    CKFinder.popup( {
                        connectorPath: '/ckfinder/connector',
                        chooseFiles: true,
                        width: 1200,
                        height: 600,
                        onInit: function(finder) {
                            finder.on('files:choose', function(evt) {
                                var file = evt.data.files.first();
                                $(elementId).attr('value',file.getUrl().replace('{{env("APP_URL")}}',''));
                                $(elementImg).attr('src',file.getUrl().replace('{{env("APP_URL")}}',''));
                            });

                            finder.on('file:choose:resizedImage', function(evt) {
                                $(elementId).attr('value',evt.data.resizedUrl.replace('{{env("APP_URL")}}',''));
                                $(elementImg).attr('src', evt.data.resizedUrl.replace('{{env("APP_URL")}}',''));
                            });
                        }
                    } );
                }

                $("#btnSelectImg").click(function () {
                    selectFileWithCKFinder('#image','#img');          
                });
            });
        })(jQuery);
	</script>
@endsection

