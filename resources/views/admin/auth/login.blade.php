
@extends('admin.layouts.loginapp')

@section('content')


<div class="main" id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-custom">
                    <div class="panel-body">
                        <div class="block-title loginPage"><h2>Inloggen</h2></div>
                        <form class="form-horizontal col-md-12" role="form" method="POST" action="{{ route('admin.login.submit') }}">
                            {{ csrf_field() }}
                            <div class="form-group">

                                <label for="label_name" class="control-label black">Email:</label>
                                <input placeholder="Voer hier je gebruikersnaam in ..." id="email" type="email" class="form-control form-lg" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="label_name" class="control-label black">Wachtwoord:</label>
                                <input id="password" type="password" class="form-control form-lg" name="password" placeholder="Voer hier je wachtwoord in ...">
                                <br>
                                <div class="checkbox checkbox-css">
                                    <input style="margin-left: 0" class="form-check-input" type="checkbox" name="remember" id="remember_checkbox">
                
                                    <label class="form-check-label" for="remember_checkbox">
                                        Remember
                                    </label>
                                </div>
                                <br>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-lg">
                                        <button class="btn btn-orange btn-block btn-lg text-uppercase loginBtnTxt" type="submit">direct inloggen</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div><!-- end .main -->



@endsection
