<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aeres</title>


    <!-- Fonts -->
    {{--<link href='https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>--}}
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500" rel="stylesheet">
    
    <!-- Bootstrap -->
    <link href="/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Plugins -->
    <link href="/assets/plugins/formstyler/css/jquery.formstyler.css" rel="stylesheet">
    <link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link href="{!! URL::asset('assets/plugins/tinymce/skins/lightgray/skin.min.css') !!}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    @stack('css')

    <!-- Styles -->
    <link href="{!! asset('assets/css/styles.css') !!}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
    {{-- <script type="text/javascript">
        var URL = {
            'base' : '{{ URL::to('/').'/' }}',
            'current' : '{{ URL::current().'/' }}',
            'full' : '{{ URL::full().'/' }}'
        };
        var search = '{{ url(route('search'))  }}';
        var productsUrl = '{{ url(route('producten.index'))  }}';
        var studentsUrl = '{{ url(route('studenten.index'))  }}';
        var ordersUrl = '{{ url(route('orders.index'))  }}';
        var batches_max_qty = '{{ url(route('batches.max_qty'))  }}';
    </script> --}}
</head>
<?php 
    $act = Route::getCurrentRoute()->getAction();  
    $routeName = Route::getCurrentRoute()->getName(); 
 ?>
<body id="<?php echo $routeName;?>">
<div class="loader"></div>
 
<div class="wrapper">
    <header class="header" id="header">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 text-center-sm">
                        <div class="logo">
                            <a href="/">
                                <img width="225" height="80" src="/assets/img/logo.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-8 text-center">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="input-group searchFormTop">
                                    <input type="text" class="form-control" id="site-search" placeholder="Zoeken ...">
                                    <div class="input-group-addon rightBorderRadius">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 pull-right">
                                <div class="input-group right">
                                    {{--<input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">--}}
                                    <a href="#" class="uerLinkTop input-group-addon leftBorderRadius">{!! Auth::guard('admin')->user()->name !!}</a>
                                    @if(Auth::guard('admin')->user())
                                    <span class="input-group-addon" id="basic-addon1"><a href="{{ URL::route('profileEdit')  }}"><span class="glyphicon glyphicon-cog"></span></a></span>
                                    @endif
                                    <span class="input-group-addon rightBorderRadius" id="basic-addon1"> <a href="{{ route('admin.logout') }}"><span class="glyphicon glyphicon-off"></span></a></span>
                                </div>

                                {{--<ul class="list-group pull-right headeRightButton">--}}
                                    {{--<li class="list-group-item"><a href="#">{!! Auth::guard('admin')->user()->name !!}</a></li>--}}
                                    {{--<li class="list-group-item gray">--}}
                                        {{--<a class="btn btn-success btn-xs" href="{{ URL::route('profileEdit')  }}"><span class="glyphicon glyphicon-cog"></span></a>--}}
                                        {{--<a class="btn btn-danger btn-xs" href="{{ url('/logout') }}"><span class="glyphicon glyphicon-off"></span></a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-middle pad10">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <nav class="navbar navbar-clear green">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav navbar-left">
                                    @if(Auth::guard('admin')->user()->isAdmin())
                                        <li @if ($routeName=='orders.index' || Route::getCurrentRoute()->uri() == '/') class="active" @endif ><a href="{!! URL::route('orders.index') !!}">BESTELLINGEN</a></li>
                                        <li @if ($routeName=='studenten.index') class="active" @endif><a href="{!! URL::route('studenten.index') !!}">STUDENTEN</a></li>
                                        <li @if ($routeName=='categories.index') class="active" @endif><a style="text-transform: uppercase" href="{!! URL::route('categories.index') !!}">categorieën</a></li>
                                        <li @if ($routeName=='producten.index') class="active" @endif><a href="{!! URL::route('producten.index') !!}">PRODUCTEN</a></li>
                                        <li @if ($routeName=='stats.index') class="active" @endif><a href="{!! URL::route('stats.index') !!}">STATISTIEKEN</a></li>
                                        {{-- <li @if ($routeName=='mail') class="active" @endif><a href="{!! URL::route('mail') !!}">MAIL</a></li> --}}
                                        <li @if ($routeName=='newsletter') class="active" @endif><a href="{!! URL::route('newsletter') !!}">NIEUWS</a></li>
                                        <li @if ($routeName=='faq' || $routeName=='cms.index') class="active" @endif><a href="{!! URL::route('cms.index') !!}">CMS</a></li>
                                        <li @if ($routeName=='excel.import') class="active" @endif><a href="{!! URL::route('excel.import') !!}">IMPORTEREN</a></li>
                                    @endif

                                    @if(Auth::guard('admin')->user()->isTeacher())
                                        <li @if ($routeName=='orders.index' || Route::getCurrentRoute()->uri() == '/') class="active" @endif ><a href="{!! URL::route('orders.index') !!}">BESTELLINGEN</a></li>
                                        <li @if ($routeName=='studenten.index') class="active" @endif><a href="{!! URL::route('studenten.index') !!}">STUDENTEN</a></li>
                                    @endif
                                </ul>

                                <a href="{!! URL::previous() !!}" class="btn btn-transparent pull-right text-right row">Terug</a>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        @if ($routeName == 'orders.index' || $routeName == 'orders.edit' || $routeName == 'batches.edit' || $routeName == 'batches.index' || $routeName == 'shipments.index' || $routeName == 'shipments.show')
            <div class="header-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <nav class="navbar navbar-clear gray">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                                    <ul class="nav navbar-nav navbar-left">
                                        @if(Auth::guard('admin')->user()->isAdmin())
                                        <li @if ($routeName=='orders.edit' || $routeName=='orders.index' || Route::getCurrentRoute()->uri() == '/') class="active" @endif ><a href="{!! URL::route('orders.index') !!}">BESTELLINGEN</a></li>
                                        <li @if ($routeName=='batches.index' || $routeName=='batches.edit') class="active" @endif ><a href="{!! URL::route('batches.index') !!}">BATCHES</a></li>
                                        @endif
                                        <li @if ($routeName=='shipments.index' || $routeName == 'shipments.show') class="active" @endif><a href="{!! URL::route('shipments.index') !!}">VERZENDINGEN</a></li>
                                    </ul>

                                    @if(!empty($lastDbTableUpdate))
                                        <div style="float:right; margin-top:15px; ">
                                            <a href="/sync/orders" class="refresh"><span class="glyphicon glyphicon-refresh"></span></a>
                                            <span class="lastUpdateTxt">laatste update:</span> {{ $lastDbTableUpdate }}
                                        </div>
                                    @endif
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if ($routeName == 'producten.index' || $routeName == 'producten.show' || $routeName == 'producten.update' || $routeName == 'producten.isbn')
            <div class="header-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <nav class="navbar navbar-clear gray">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                                    <ul class="nav navbar-nav navbar-left">
                                        <li @if ($routeName=='producten.index') class="active" @endif><a href="{!! URL::route('producten.index') !!}">Producten</a></li>
                                        <li @if ($routeName=='producten.isbn') class="active" @endif><a href="{!! URL::route('producten.isbn') !!}">Producten ISBN</a></li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </header><!-- end .header -->

    @yield('content')

    <footer class="footer" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="copyright text-center">Copyright {{date('Y')}} Aeres</div>
                </div>
            </div>
        </div>
    </footer><!-- end .footer -->
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/assets/plugins/bootstrap/js/bootstrap.js"></script>
<script src="/assets/plugins/formstyler/js/jquery.formstyler.min.js"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="/assets/js/select2.full.js"></script>
<script src="{!! URL::asset('assets/plugins/tinymce/tinymce.min.js') !!}"></script>
 <script>

     $(document).ready(function(){
         $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': "{{ csrf_token() }}"
             },
             beforeSend: function(){
                 // Handle the complete event
//                        alert('ajax before!');
             },
             complete: function(){
                 // Handle the complete event
//                        alert('ajax complete!');
             }
         });

     });


 </script>
<script src="/assets/js/main.js"></script>
@stack('scripts')
</body>
</html>
