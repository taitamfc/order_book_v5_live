<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aeres</title>


    <!-- Fonts -->
    {{--<link href='https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>--}}
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Plugins -->
    <link href="/assets/plugins/formstyler/css/jquery.formstyler.css" rel="stylesheet">
    <link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <!-- Styles -->
    <link href="/assets/css/styles.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">
    <div class="loginPageWrapp"></div>
    <header class="header" id="header">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 text-center col-md-offset-3">
                        <div class="logo">
                            <a href="#">
                                <img  src="assets/img/logo.png" alt="">
                            </a>
                        </div>
                    </div>
                    {{--<div class="col-sm-6 text-sm-right text-center-sm">--}}
                        {{--<div class="contacts">--}}
                            {{--<a href="tel:0332457324"><strong>Hulp nodig?</strong> 033 245 73 24</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
        {{--<div class="header-middle">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 text-center">--}}
                        {{--<h3>Welkom op de boekenbestelsite van Groenhorst Barneveld in samenwerking met de VDA-groep.</h3>--}}
                        {{--<p>Via deze bestelsite kun je de benodigde lesbundels, (commerciële) boeken en eventuele keuzepakketten bestellen. Ook dit jaar hebben we weer leuke schoolbenodigdheden toegevoegd aan de bestelsite. Deze vind je terug onder de keuzepakketten. </p>--}}
                        {{--<p>Je boekenbestelling wordt bij je thuis afgeleverd. Wel zo gemakkelijk!</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </header><!-- end .header -->

@yield('content')

    <footer class="footer" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="copyright text-center">Copyright {{date('Y')}} Aeres</div>
                </div>
            </div>
        </div>
    </footer><!-- end .footer -->
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/assets/plugins/bootstrap/js/bootstrap.js"></script>
<script src="/assets/plugins/formstyler/js/jquery.formstyler.min.js"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<script src="/assets/js/main.js"></script>
</body>
</html>