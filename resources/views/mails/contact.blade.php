<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="format-detection" content="telephone=no" />
<!-- disable auto telephone linking in iOS -->
<title>Order Bevestiging</title>
<style type="text/css">
/* RESET STYLES */
html {
	background-color: #fafafa;
	margin: 0;
	padding: 0;
}
body, #bodyTable, #bodyCell, #bodyCell {
	height: 100% !important;
	margin: 0;
	padding: 0;
	width: 100% !important;
	font-family: Helvetica, Arial, "Lucida Grande", sans-serif;
}
table {
	border-collapse: collapse;
}
table[id=bodyTable] {
	width: 100%!important;
	margin: auto;
	max-width: 500px!important;
	color: #7A7A7A;
	font-weight: normal;
}
img, a img {
	border: 0;
	outline: none;
	text-decoration: none;
	height: auto;
	line-height: 100%;
}
a {
	text-decoration: none !important;
	color: #BB0006;
}
h1, h2, h3, h4, h5, h6 {
	color: #5F5F5F;
	font-weight: normal;
	font-family: Helvetica;
	font-size: 20px;
	line-height: 125%;
	text-align: Left;
	letter-spacing: normal;
	margin-top: 0;
	margin-right: 0;
	margin-bottom: 10px;
	margin-left: 0;
	padding-top: 0;
	padding-bottom: 0;
	padding-left: 0;
	padding-right: 0;
}
/* CLIENT-SPECIFIC STYLES */
.ReadMsgBody {
	width: 100%;
}
.ExternalClass {
	width: 100%;
} /* Force Hotmail/Outlook.com to display emails at full width. */
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
	line-height: 100%;
} /* Force Hotmail/Outlook.com to display line heights normally. */
table, td {
	mso-table-lspace: 0pt;
	mso-table-rspace: 0pt;
} /* Remove spacing between tables in Outlook 2007 and up. */
#outlook a {
	padding: 0;
} /* Force Outlook 2007 and up to provide a "view in browser" message. */
img {
	-ms-interpolation-mode: bicubic;
	display: block;
	outline: none;
	text-decoration: none;
} /* Force IE to smoothly render resized images. */
body, table, td, p, a, li, blockquote {
	-ms-text-size-adjust: 100%;
	-webkit-text-size-adjust: 100%;
	font-weight: normal!important;
} /* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */
.ExternalClass td[class="ecxflexibleContainerBox"] h3 {
	padding-top: 10px !important;
} /* Force hotmail to push 2-grid sub headers down */
/* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */
.collapse {
	margin: 0;
	padding: 0;
}
/* ------------------------------------- 
		HEADER 
------------------------------------- */
table.head-wrap {
	width: 100%;
}
.header.container table td.logo {
	padding: 15px;
}
.header.container table td.label {
	padding: 15px;
	padding-left: 0px;
}
/* ========== Page Styles ========== */
h1 {
	display: block;
	font-size: 26px;
	font-style: normal;
	font-weight: normal;
	line-height: 100%;
}
h2 {
	display: block;
	font-size: 20px;
	font-style: normal;
	font-weight: normal;
	line-height: 120%;
}
h3 {
	display: block;
	font-size: 17px;
	font-style: normal;
	font-weight: normal;
	line-height: 110%;
}
h4 {
	display: block;
	font-size: 18px;
	font-style: italic;
	font-weight: normal;
	line-height: 100%;
}
.flexibleImage {
	height: auto;
}
.linkRemoveBorder {
	border-bottom: 0 !important;
}
table[class=flexibleContainerCellDivider] {
	padding-bottom: 0 !important;
	padding-top: 0 !important;
}
body, #bodyTable {
	background-color: #FAFAFA;
}
#emailHeader {
	background-color: #FAFAFA;
}
#email-logo-main {
	background-color: #fafafa
}
#emailBody {
	background-color: #FFFFFF;
}
#emailFooter {
	background-color: none;
}
.nestedContainer {
	background-color: #F8F8F8;
	border: 1px solid #CCCCCC;
}
.emailButton {
	background-color: #205478;
	border-collapse: separate;
}
.buttonContent {
	color: #FFFFFF;
	font-family: Helvetica;
	font-size: 18px;
	font-weight: bold;
	line-height: 100%;
	padding: 15px;
	text-align: center;
}
.buttonContent a {
	color: #FFFFFF;
	display: block;
	text-decoration: none!important;
	border: 0!important;
}
.emailCalendar {
	background-color: #FFFFFF;
	border: 1px solid #CCCCCC;
}
.emailCalendarMonth {
	background-color: #205478;
	color: #FFFFFF;
	font-family: Helvetica, Arial, sans-serif;
	font-size: 16px;
	font-weight: bold;
	padding-top: 10px;
	padding-bottom: 10px;
	text-align: center;
}
.emailCalendarDay {
	color: #205478;
	font-family: Helvetica, Arial, sans-serif;
	font-size: 60px;
	font-weight: bold;
	line-height: 100%;
	padding-top: 20px;
	padding-bottom: 20px;
	text-align: center;
}
.imageContentText {
	margin-top: 10px;
	line-height: 0;
}
.imageContentText a {
	line-height: 0;
}
#invisibleIntroduction {
	display: none !important;
} /* Removing the introduction text from the view */
/*FRAMEWORK HACKS & OVERRIDES */
span[class=ios-color-hack] a {
	color: #275100!important;
	text-decoration: none!important;
} /* Remove all link colors in IOS (below are duplicates based on the color preference) */
span[class=ios-color-hack2] a {
	color: #205478!important;
	text-decoration: none!important;
}
span[class=ios-color-hack3] a {
	color: #8B8B8B!important;
	text-decoration: none!important;
}
/* -------------------------------------
    INVOICE
------------------------------------- */
.invoice {
	margin: 0px auto;
	text-align: left;
	width: 100%;
}
.invoice td {
	padding: 5px 0;
}
.invoice .invoice-items {
	width: 100%;
}
.invoice .invoice-items td {
	border-top: #eee 1px solid;
}
.invoice .invoice-items .total td {
	border-top: 2px solid #333;
	border-bottom: 2px solid #333;
	font-weight: 700;
}
.total-left{
	display:inline-block;margin-bottom: 9px;width:150px;
}
.total-right{
	margin-bottom: 9px;display: inline-block;margin-left: 200px;
}
.total-grand-total{
	border-top:1px solid black;
}
/* A nice and clean way to target phone numbers you want clickable and avoid a mobile phone from linking other numbers that look like, but are not phone numbers.  Use these two blocks of code to "unstyle" any numbers that may be linked.  The second block gives you a class to apply with a span tag to the numbers you would like linked and styled.
			Inspired by Campaign Monitor's article on using phone numbers in email: http://www.campaignmonitor.com/blog/post/3571/using-phone-numbers-in-html-email/.
			*/
.a[href^="tel"], a[href^="sms"] {
	text-decoration: none!important;
	color: #606060!important;
	pointer-events: none!important;
	cursor: default!important;
}
.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
	text-decoration: none!important;
	color: #606060!important;
	pointer-events: auto!important;
	cursor: default!important;
}

tr.row-gray td{
	background-color: #f2f2f2;
}
tr.row-gray-strong td{
	background-color: #d9d9d9;
}
tr.row-yellow td{
	background-color: #fff2cc;
}
tr.row-yellow-strong td{
	background-color: #ffe599;
}

/* MOBILE STYLES */
@media only screen and (max-width: 480px) {
/*////// CLIENT-SPECIFIC STYLES //////*/
body {
	width: 100% !important;
	min-width: 100% !important;
} /* Force iOS Mail to render the email at full width. */
/* FRAMEWORK STYLES */
				/*
				CSS selectors are written in attribute
				selector format to prevent Yahoo Mail
				from rendering media query styles on
				desktop.
				*/
				/*td[class="textContent"], td[class="flexibleContainerCell"] { width: 100%; padding-left: 10px !important; padding-right: 10px !important; }*/
table[id="emailHeader"], table[id="email-logo-main"], table[id="emailBody"], table[id="emailFooter"], table[class="flexibleContainer"], td[class="flexibleContainerCell"] {
	width: 100% !important;
}
td[class="flexibleContainerBox"], td[class="flexibleContainerBox"] table {
	display: block;
	width: 100%;
	text-align: left;
}
/*
				The following style rule makes any
				image classed with 'flexibleImage'
				fluid when the query activates.
				Make sure you add an inline max-width
				to those images to prevent them
				from blowing out.
				*/
td[class="imageContent"] img {
	height: auto !important;
	width: 100% !important;
	max-width: 100% !important;
}
img[class="flexibleImage"] {
	height: auto !important;
	width: 100% !important;
	max-width: 100% !important;
}
img[class="flexibleImageSmall"] {
	height: auto !important;
	width: auto !important;
}
/*
				Create top space for every second element in a block
				*/
table[class="flexibleContainerBoxNext"] {
	padding-top: 10px !important;
}
/*
				Make buttons in the email span the
				full width of their container, allowing
				for left- or right-handed ease of use.
				*/
table[class="emailButton"] {
	width: 100% !important;
}
td[class="buttonContent"] {
	padding: 0 !important;
}
td[class="buttonContent"] a {
	padding: 15px !important;
}
}

/*  CONDITIONS FOR ANDROID DEVICES ONLY
			*   http://developer.android.com/guide/webapps/targeting.html
			*   http://pugetworks.com/2011/04/css-media-queries-for-targeting-different-mobile-devices/ ;
			=====================================================*/

@media only screen and (-webkit-device-pixel-ratio:.75) {
/* Put CSS for low density (ldpi) Android layouts in here */
}

@media only screen and (-webkit-device-pixel-ratio:1) {
/* Put CSS for medium density (mdpi) Android layouts in here */
}

@media only screen and (-webkit-device-pixel-ratio:1.5) {
/* Put CSS for high density (hdpi) Android layouts in here */
}

/* end Android targeting */

			/* CONDITIONS FOR IOS DEVICES ONLY
			=====================================================*/
@media only screen and (min-device-width : 320px) and (max-device-width:568px) {
}
			/* end IOS targeting */
</style>
<!--
			Outlook Conditional CSS

			These two style blocks target Outlook 2007 & 2010 specifically, forcing
			columns into a single vertical stack as on mobile clients. This is
			primarily done to avoid the 'page break bug' and is optional.

			More information here:
			http://templates.mailchimp.com/development/css/outlook-conditional-css
		-->
<!--[if mso 12]>
			<style type="text/css">
				.flexibleContainer{display:block !important; width:100% !important;}
			</style>
		<![endif]-->
<!--[if mso 14]>
			<style type="text/css">
				.flexibleContainer{display:block !important; width:100% !important;}
			</style>
		<![endif]-->
</head>
<body bgcolor="#FFF" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">

<!-- CENTER THE EMAIL // --> 
<!--
1.  The center tag should normally put all the
	content in the middle of the email page.
	I added "table-layout: fixed;" style to force
	yahoomail which by default put the content left.

2.  For hotmail and yahoomail, the contents of
	the email starts from this center, so we try to
	apply necessary styling e.g. background-color.
-->

<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
	<tr>
		<td>
			<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
				<tr>
					<td align="center" valign="top" id="bodyCell">
					  <table id="email-logo-main" align="center" bgcolor="#FFF" border="0" cellpadding="0" cellspacing="0" width="600">
						<tbody>
						  <tr>
							<td></td>
							<td class="header container" align="center" valign="top"><!-- /content -->
							  
							  <div class="content">
								<table bgcolor="#fafafa" align="center" width="100%" cellpadding="0">
								  <tbody>
									<tr>
									   <td align="center" valign="top"><div> <a href="http://aeres.webdesignvaassen.nl/" target="_blank"><img src="http://aeres.webdesignvaassen.nl/assets_fe/img/logo.png" width="100%" height="auto" title="www.aeres.webdesignvaassen.nl"></a> </div></td>
									</tr>
								  </tbody>
								</table>
							  </div>
							  
							  <!-- /content --></td>
							<td></td>
						  </tr>
						</tbody>
					  </table>
				  </tr>
				  <tr>
					  <td>
						  <table align="center" bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="600" id="emailBody">
							  
							  <!-- // MODULE ROW --> 
							  
							  <!-- MODULE ROW // -->
							  <tr>
								<td valign="top"><!-- CENTERING TABLE // -->
									<table align="center"width="600"  cellspacing="0" cellpadding="0">
										<tbody>                 
										 <tr>
										   <td class="content-block">
											   <table width="100%" cellspacing="0" cellpadding="0" class="invoice">
											   <tbody>
												  <!-- Dear {User First Name} {User Last Name} -->
												  <tr>
													  <td>
														<table align="center" width="100%" style="color: #ffffff">
															<tr>
																<h1 style="color:#ff9900;font-family:Helvetica,Arial,sans-serif;font-size:20px;margin-bottom:5px;border-bottom: 2px solid #ccc;text-transform: uppercase">
																	Contactformulier
																</h1>
															</tr>
														</table>
													  </td>
												  </tr>
												   <tr>
													  <td valign="top">
														  <table align="center" cellpadding="0" cellspacing="0" width="100%">
															  <tr>
																  <th style="background-color: #ffffff;vertical-align: top">
																	  <table align="left" cellpadding="0" cellspacing="0" width="100%">
																		 <tr><td style="height: 15px"></td></tr>
																		 <tr>
																			<td style="text-align: left;color:#000000;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight: 500;padding-bottom: 8px">
																			  <strong>Studentnummer: </strong> {{$data['student_number']}}
																			</td>
																		</tr>
																		<tr>
																			<td style="text-align: left;color:#000000;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight: 500;padding-bottom: 8px">
																			  <strong>Klas: </strong> {{$data['class']}}
																			</td>
																		</tr>
																		<tr>
																			<td style="text-align: left;color:#000000;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight: 500;padding-bottom: 8px">
																			  <strong>Naam: </strong> {{$data['full_name']}}
																			</td>
																		</tr>
																		<tr>
																			<td style="text-align: left;color:#000000;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight: 500;padding-bottom: 8px">
																			  <strong>Adres: </strong> {{$data['address']}}
																			</td>
																		</tr>

																		  <tr>
																			  <td style="text-align: left;color:#000000;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight: 500;padding-bottom: 8px">
                                                                                <strong>Postcode / Plaats: </strong> {{$data['postcode']}}
																			  </td>
																		  </tr>
																		  <tr>
																			<td style="text-align: left;color:#000000;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight: 500;padding-bottom: 8px">
																			  <strong>Telefoon: </strong>{{ $data['telephone'] }}
																			</td>
																		</tr>
																		  <tr>
																			  <td style="text-align: left;color:#000000;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight: 500;padding-bottom: 8px">
																				<strong>E-mailadres: </strong>{{ $data['email']}}
																			  </td>
																		  </tr>
																		  
																		  <tr>
																			<td style="text-align: left;color:#000000;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight: 500;padding-bottom: 8px">
																				<strong>Vraag of toelichting: </strong>{{ $data['content'] }}
																			</td>
																		</tr>
																		<tr><td style="height: 15px"></td></tr>
																	  </table>
																  </th>
															  </tr>
														  </table>
													  </td>
												   </tr>
												  <tr><td style="height: 15px"></td></tr>
												  <tr>
													  <td>
														  <table align="center" width="600" style="background-color: #ff9900;color: #ffffff">
															  <tr>
																  <td style="background-color: #ff9900;text-align: center;color: #ffffff;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight: 600">
																	  <span style="color:#ffffff">website: aeres.webdesignvaassen.nl</span> 
																  </td>
															  </tr>
														  </table>
													  </td>
												  </tr>
											   </tbody>
											  </table>
										   </td>
										 </tr>
										</tbody>
									  </table>
								</td>
							  </tr>
							</td>
					  </td>
				  </tr>
			</table>
		</td>
	</tr>
  </table>
</body>
</html>
