<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('frontend.includes.components.head')
    <style>
        /* html, body {
            height: 100%;
        } */

        .page-content {
            text-align: center;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }
        .content a{
            font-size: 18px;
            margin-bottom: 20px;
            text-transform: capitalize
        }
        .title {
            font-size: 36px;
            margin-bottom: 20px;
            text-transform: uppercase
        }
        footer{
            position: absolute;
            left: 0;
            right: 0;
            bottom: 0;
        }
    </style>
</head>

<body>
	<style>
        .inline-image{
            display:inline-block
        }
        .logo{
            margin-top: 20px;
            margin-bottom: auto;
        }
        .first-image{
            width: 40%;
            height: auto;
        }
        .second-image{
            width: 50%;
            height: auto;
            padding-top: 20px;
        }
    </style>
    <header class="header" id="header">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 text-center-sm">
                        <div class="logo">
                            <a href="/">
                                <img width="auto" height="96" src="/assets_fe/img/logo.png" alt="" class="inline-image first-image">
                                <img width="auto" height="96" src="/assets_fe/img/logo_boekenprint.png" alt="" class="inline-image second-image">
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-7 text-center-sm">
                        <div class="">
                            <ul class="header menu">
                                <li><a href="/">HOME</a></li>
                                <li><a href="/faq">VEELGESTELDE VRAGEN</a></li>
                                <li><a href="/contact">contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header><!-- end .header -->
    <div class="clearfix"></div>
    <div class="">
        <div class="container">
            <div class="page-content">
                <div class="content">
                    <img width="auto" height="" src="/assets_fe/img/404.png" alt="" class="">
                    <div class="title">pagina niet gevonden.</div>
                    
                    <a role="button" onclick="goBack()"><< ga terug</a>
                    <br>
                    <br>
                    <br>
                    <script>
                        function goBack() {
                          window.history.back();
                        }
                        </script>
                </div>
            </div>
        </div>
    </div>
	

	@include('frontend.includes.components.footer')

	<!--JavaScript-->
	@include('frontend.includes.components.page-js')
</body>
</html>
