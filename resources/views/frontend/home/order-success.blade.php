@extends('frontend.includes.student-layout')

@section('title', 'Bestelling Overzicht')

@section('header')
    @include('frontend.includes.components.header-search')
@endsection

@section('content')

{{-- <div class="header-middle">
    <div class="container">
        <div class="row row0">
            <div class="col-sm-3 col-xs-12 text-sm-left text-center-sm form-group">
                <a href="#" class="bt-add-cart btn btn-info text-uppercase">Volgende stap</a>
            </div>
        </div>
    </div>
</div> --}}
<div class="header-middle" style="margin-top: 10px">
    <div class="container">
        <div class="row row0">
            <div class="col-sm-6 text-left form-group">
                <h3>Hoe ver ben je in het proces?</h3>
            </div>
            <div class="col-sm-6 text-sm-right text-center-sm form-group">
                <a role="button" class="bt-add-cart btn btn-info text-uppercase">Volgende stap</a>
            </div>
        </div>
    </div>
</div>
<div class="main" id="main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @include('frontend.includes.components.order-step')
                <div class="block-title text-center"><h2>Bestelling Afgerond</h2></div>
                <div class="panel panel-custom">
                    <div class="panel-body panel-content">
                        <p>Bedankt voor het succesvol afronden van je bestelling. We zullen zorgen dat je de bestelde producten zo spoedig mogelijk ontvangt zodat je compleet voorbereid bent op de start van een nieuw succesvol schooljaar. Onder deze informatie is je factuur nog terug te vinden, via de knop kun je de factuur tevens downloaden.</p>
                        <div class="table-responsive">
                            <table class="table table-custom">
                                <tr>
                                    <th class="">FACTUUR ID</th>
                                    <th>BESTELDATUM</th>
                                    <th>TOTAALBEDRAG</th>
                                    <th>STATUS</th>
                                    <th>FACTUUR</th>
                                    <th class="th-flex">ACTIE</th>
                                </tr>
                                @if ($order)
                                @php
                                    $class_status = "label-warning";
                                    if($order->status == "comleted"){
                                        $order->status = "Afgerond";
                                        $class_status = "label-success";
                                    }
                                    if($order->status == "pending"){
                                        $order->status = "In Behandeling";
                                    }
                                    
                                @endphp
                                <tr>
                                    <td class="td-nowrap">{{$order->order_id}}</td>
                                    <td class="td-nowrap">{{$order->order_date}}</td>
                                    <td style="font-weight: 600">€ {{strval(number_format((float)$order->grand_total, 2, ',', ''))}}</td>
                                    <td>
                                        <label class="label {{$class_status}}" style="text-transform: uppercase">{{$order->status}}</label>
                                    </td>
                                    <td>  
                                        <a href="{{$order->invoice_pdf}}" download class="btn btn-danger btn-sm">Downloaden</a>
                                    </td>
                                    <td>
                                        <a href="{{route('student.order.info',['order_id' => $order->order_id])}}" class="btn btn-success btn-sm">Bestel Details</a>   
                                    </td>
                                </tr>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
                <br><br>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            $(".user-steps").find(".us-box").removeClass("active");
            $(".user-steps").find(".us-box[step='1']").addClass("active");
            $(".user-steps").find(".us-box[step='2']").addClass("active");
            $(".user-steps").find(".us-box[step='3']").addClass("active");
            $(".user-steps").find(".us-box[step='4']").addClass("active");

            $(".user-steps").find(".glyphicon").removeClass("glyphicon-ok-sign");
            $(".user-steps").find(".us-box[step='4']").find('.glyphicon').addClass("glyphicon-ok-sign");
        });
    </script>
@endpush



