@extends('frontend.includes.student-layout')

@section('title', 'Productkeuze')

@section('header')
    @include('frontend.includes.components.header-search')
@endsection

@section('content')
<style>
    .error-border{
        border: 3px dotted red;
    }
    .error-content{
        color:red !important;
    }
</style>
{{-- <div class="header-middle">
    <div class="container">
        <div class="row row0">
            <div class="col-sm-3 col-xs-6">
                <a href="{{route('student.home')}}" class="btn btn-info text-uppercase form-group">Vorice stap</a>
            </div>
            <div class="col-sm-3 col-xs-6 col-sm-push-6 text-right form-group">
                <a role="button" class="bt-ship-info btn btn-info text-uppercase">Volgende stap</a>
            </div>
            <div class="col-sm-6 col-sm-pull-3 text-center form-group">
                <h3>Hoe ver ben je in het proces?</h3>
            </div>
        </div>
    </div>
</div> --}}
<div class="header-middle" style="margin-top: 10px">
    <div class="container">
        <div class="row row0">
            <div class="col-sm-6 text-left form-group">
                <h3>Hoe ver ben je in het proces?</h3>
            </div>
            <div class="col-sm-6 text-sm-right text-center-sm form-group">
                <a role="button" class="bt-ship-info btn btn-info text-uppercase">Volgende stap</a>
            </div>
        </div>
    </div>
</div>
<div class="main" id="main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @include('frontend.includes.components.order-step')
                <br>
                <div class="panel panel-custom shipping-addr">
                    <div class="panel-body panel-content">
                        <h4 style="color:#009fe3">Afleveradres</h4>
                        <p>Om het jou makkelijk te maken, worden de boeken naar je thuisadres verstuurd. De onderstaande gegevens zijn bij ons bekend. Is je gewenste afleveradres anders, pas dan de onderstaande velden aan.</p>
                        <p style="display: none;">Wanneer hier je hier je e-mailadres achterlaat houden wij je op de hoogte van de status van je bestelling.</p>
                    </div>
                    <div class="panel-body panel-content" id="panel-shipping-info">
                        @include('frontend.includes.order.shipping-info')
                    </div>
                </div>
                <div class="nav-buttons">
                    <a class="prev-step-bottom" href="{{route('student.home')}}">Vorige stap</a>
                    <a class="bt-ship-info next-step-bottom" role="button">Volgende stap</a>
                    <div class="clearfix"></div>
                </div>
                <br><br>
            </div>  
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            $(".user-steps").find(".us-box[step='2']").addClass("active");
            $(".user-steps").find(".us-box[step='2']").find('.glyphicon').addClass("glyphicon-ok-sign");
            $(".bt-ship-info").on("click",function(){
                var re_email = $("#re-email").val();
                var email = $("#email").val();
                if(re_email != email){
                    $("#error-email").show();
                    $("#error-email").addClass("error-content");
                    $("#re-email").addClass("error-border");
                }else{
                    $("#ship-info").click();
                }
            });
            
        });
    </script>
@endpush


