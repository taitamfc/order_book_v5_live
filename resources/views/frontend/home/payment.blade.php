@extends('frontend.includes.student-layout')

@section('title', 'Bestelling Overzicht')

@section('header')
    @include('frontend.includes.components.header-search')
@endsection

@section('content')

<div class="header-middle">
    <div class="container">
        <div class="row row0">
            <div class="col-sm-3 col-xs-12 text-sm-left text-center-sm form-group">
                <a href="{{route('student.order.details')}}" class="bt-add-cart btn btn-info text-uppercase">Volgende stap</a>
            </div>
            <div class="col-sm-6 text-center form-group">
                <h3>Hoe ver ben je in het proces?</h3>
            </div>
        </div>
    </div>
</div>

<div class="main" id="main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @include('frontend.includes.components.order-step')
                <div class="block-title text-center"><h2>Betaling</h2></div>
                <div class="panel panel-custom">
                    <div class="panel-heading" style="display:none;">
                        <div class="title">
                            <h3>Naam</h3>
                        </div>
                        <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel1" aria-expanded="true" aria-controls="collapsePanel1"></span>
                    </div>
                    <div class="collapse in" id="collapsePanel1">
                        <div class="panel-body">
                            <br>
                            <form action="{{route('student.order.payment.submit')}}" method="post" class="form-horizontal">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="label_name" class="control-label col-sm-3">Betaalmethodes:</label>
                                    <div class="col-sm-5">
                                        <select name="method" id="method" class="form-control">
                                            @foreach ($methods as $item)
                                                <option value="{{$item->id}}" >{{$item->description}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="label_name" class="control-label col-sm-3"></label>
                                    <div class="col-sm-5">
                                        <button class="btn btn-sm btn-success" type="submit">Betaling</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <br><br>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            $(".user-steps").find(".us-box[step='4']").addClass("active");
            $(".user-steps").find(".us-box[step='4']").find('.glyphicon').addClass("glyphicon-ok-sign");
        });
    </script>
@endpush



