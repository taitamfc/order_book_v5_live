@extends('frontend.includes.student-layout')

@section('title', 'Bestelling Overzicht')

@section('header')
    @include('frontend.includes.components.header-search')
@endsection

@section('content')

{{-- <div class="header-middle">
    <div class="container">
        <div class="row row0">
            <div class="col-sm-3 col-xs-6">
                <a href="{{route('student.order.shipping')}}" class="btn btn-info text-uppercase form-group">Vorice stap</a>
            </div>
            <div class="col-sm-3 col-xs-6 col-sm-push-6 text-right form-group">
                <a href="{{route('student.order.payment')}}" class="btn btn-info text-uppercase">Volgende stap</a>
            </div>
            <div class="col-sm-6 col-sm-pull-3 text-center form-group">
                <h3>Hoe ver ben je in het proces?</h3>
            </div>
        </div>
    </div>
</div> --}}
<div class="header-middle" style="margin-top: 10px">
    <div class="container">
        <div class="row row0">
            <div class="col-sm-6 text-left form-group">
                <h3>Hoe ver ben je in het proces?</h3>
            </div>
            <div class="col-sm-6 text-sm-right text-center-sm form-group">
                <a href="{{route('student.order.payment')}}" class="btn btn-info text-uppercase">Volgende stap</a>
            </div>
        </div>
    </div>
</div>
<div class="main" id="main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @include('frontend.includes.components.order-step')
                {{-- <div class="block-title text-center"><h2>Bestelling Overzicht</h2></div> --}}
                <div class="panel panel-custom block-order-detail">
                    <div class="col-md-3 col-sm-4">
                        <h3 style="font-size: 23px;color: #009fe3;margin-top: 5px">Gegevens</h3>
                        <br>
                        <p style="margin-bottom: 5px; font-weight: 500">{{$student_info->username}}</p>
                        <p style="margin-bottom: 5px; font-weight: 500">{{$student_info->firstname .' '. $student_info->middlename .' '. $student_info->lastname}}</p>
                        <p style="margin-bottom: 5px; font-weight: 500">{{$student_info->customer_group_code}}</p>
                        <br>
                        <p style="margin-bottom: 5px; font-weight: 500">{{$student_info->street}}</p>
                        <p style="margin-bottom: 5px; font-weight: 500">{{$student_info->postcode .' '. $student_info->city}}</p>
                        <br>
                        <p style="margin-bottom: 5px; font-weight: 500">{{$student_info->email}}</p>
                        <p style="margin-bottom: 5px; font-weight: 500">{{$student_info->telephone}}</p>
                    </div>
                    <div class="col-md-9 col-sm-8">
                        <div class="lst-products">
                            @include('frontend.includes.order.product-list')
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    {{-- <div class="panel-heading">
                        <div class="title">
                            <h3>Naam</h3>
                        </div>
                        <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel1" aria-expanded="true" aria-controls="collapsePanel1"></span>
                    </div> --}}
                    {{-- <div class="collapse in" id="collapsePanel1">
                        <div class="panel-body">
                            <form action="#" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Voornaam:</label>
                                    <div class="col-sm-5">
                                        <span class="read-only form-control">{{$student_info->username}}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Tussenvoegsel:</label>
                                    <div class="col-sm-5">
                                        <span class="read-only form-control">{{$student_info->middlename}}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Achternaam:</label>
                                    <div class="col-sm-5">
                                        <span class="read-only form-control">{{$student_info->lastname}}</span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div> --}}
                </div>
               {{--  <div class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="title">
                            <h3>PERSOONLIJKE INFORMATIE</h3>
                        </div>
                        <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel2" aria-expanded="true" aria-controls="collapsePanel2"></span>
                    </div>
                    <div class="collapse in" id="collapsePanel2">
                        <div class="panel-body">
                            <form action="#" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">E-mailadres:</label>
                                    <div class="col-sm-5">
                                        <span class="read-only form-control">{{$student_info->email}}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Telefoonnummer:</label>
                                    <div class="col-sm-5">
                                        <span class="read-only form-control">{{$student_info->telephone}}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Bankrekeningnummer:</label>
                                    <div class="col-sm-5">
                                        <span class="read-only form-control">{{$student_info->bank_account}}</span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="title">
                            <h3>ADRES</h3>
                        </div>
                        <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel3" aria-expanded="true" aria-controls="collapsePanel3"></span>
                    </div>
                    <div class="collapse in" id="collapsePanel3">
                        <div class="panel-body">
                            <form action="#" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Straat:</label>
                                    <div class="col-sm-5">
                                        <span class="read-only form-control">{{$student_info->street}}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Postcode:</label>
                                    <div class="col-sm-5">
                                        <span class="read-only form-control">{{$student_info->postcode}}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Plaats:</label>
                                    <div class="col-sm-5">
                                        <span class="read-only form-control">{{$student_info->city}}</span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div> --}}
                
                <div class="nav-buttons">
                    <a class="prev-step-bottom" href="{{route('student.order.shipping')}}">Vorige stap</a>
                    <a class="next-step-bottom" href="{{route('student.order.payment')}}">Volgende stap</a>
                    <div class="clearfix"></div>
                </div>
                <br><br>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            $(".user-steps").find(".us-box[step='3']").addClass("active");
            $(".user-steps").find(".us-box[step='3']").find('.glyphicon').addClass("glyphicon-ok-sign");
        });
    </script>
@endpush



