@extends('frontend.includes.student-layout')

@section('title', 'Productkeuze')

@section('header')
    @include('frontend.includes.components.header-search')
@endsection

@section('content')
<div class="header-middle" style="margin-top: 10px">
    <div class="container">
        <div class="row row0">
            <div class="col-sm-6 text-left form-group">
                <h3>Hoe ver ben je in het proces?</h3>
            </div>
            <div class="col-sm-6 text-sm-right text-center-sm form-group">
                <a role="button" class="bt-add-cart btn btn-info text-uppercase">Volgende stap</a>
            </div>
        </div>
    </div>
</div>
<div class="main" id="main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @include('frontend.includes.components.order-step')
                {{-- <div class="block-title text-center"><h2>Welkomstbericht</h2></div> --}}
                <div class="panel panel-custom">
                    <div class="panel-body panel-content">
                        @php
                            $student = Auth::guard('student')->user();
                            $student_name = $student->firstname .' '. $student->lastname; 
                        @endphp
                        <h4>Beste {{$student_name}},</h4>
                        <p>Volgens onze gegevens zit je komend schooljaar in klas {klas}. De onderstaande boekenlijst bevat alle benodigde lesbundels, (commerciële) boeken en eventuele keuzepakketten die je het komende schooljaar nodig zult hebben. Voor het gemak staan alle boeken reeds aangevinkt (behalve de keuzepakketten), maar je kunt op titelniveau heel eenvoudig de vinkjes ook uitzetten en deze titel daarmee niet bestellen.</p>
                        <p class="black">* Let op dat je geen boeken bestelt die al in je bezit zijn. Vink uit wat je niet wenst te bestellen.</p>
                        <p>Is je klas niet correct? Neem dan contact op via aeres@boekenprint.nl</p>
                    </div>
                </div>
                {{-- <div class="block-title text-center"><h2>Producten</h2></div> --}}
                <div class="lst-products">
                    <div class="vertical-text">
                        Producten                
                    </div>
                    @include('frontend.includes.categories.product-list')
                </div>
                <div class="nav-buttons">
                    <a class="bt-add-cart next-step-bottom" role="button">Volgende stap</a>
                    <div class="clearfix"></div>
                </div>
                <br><br>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            $(".user-steps").find(".us-box").removeClass("active");
            $(".user-steps").find(".us-box[step='1']").addClass("active");

            $(".user-steps").find(".glyphicon").removeClass("glyphicon-ok-sign");
            $(".user-steps").find(".us-box[step='1']").find('.glyphicon').addClass("glyphicon-ok-sign");

            $("input.checkbox-custom").on("change",function(){
                var _this = $(this);
                var productId = _this.attr('pro-id');
                var checkboxLabel = _this.closest('.checkbox-label');
                if(_this.prop('checked')==true){
                    checkboxLabel.append('<input type="hidden" name="product_id[]" class="product-id" value="'+ productId +'">');
                }
                else{
                    checkboxLabel.find('.product-id').remove();
                }
            });

            $(".bt-add-cart").on("click",function(){
                $("#from-order-book").submit();
            });


        });
    </script>
@endpush



