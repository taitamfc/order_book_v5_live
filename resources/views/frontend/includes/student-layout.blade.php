<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	@include('frontend.includes.components.head')
</head>
<?php 
    $act = Route::getCurrentRoute()->getAction();  
    $routeName = Route::getCurrentRoute()->getName(); 
 ?>
<body id="<?php echo $routeName;?>">
	@yield('header')

	@yield('content')

	@include('frontend.includes.components.footer')

	<!--JavaScript-->
	@include('frontend.includes.components.page-js')
</body>
</html>
