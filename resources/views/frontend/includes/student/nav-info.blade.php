<div class="block account-nav">
    <div class="block-title"><strong class="title">MIJN ACCOUNT</strong></div>
    <div class="block-content">
        <ul class="items">
            <li><a href="/account/info">Persoonlijke instellingen</a></li>
            <li><a href="/account/order-histories">Bestellingen</a></li>
        </ul>
    </div>
</div> 