
<style>
/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (Image) */
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Caption of Modal Image (Image Text) - Same Width as the Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation - Zoom in the Modal */
.modal-content, #caption {
  animation-name: zoom;
  animation-duration: 0.6s;
}

@keyframes zoom {
  from {transform:scale(0)}
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}
</style>
@if (count($data) > 0)
    <form class="table-responsive" method="POST" action="{{route('student.order.addCart')}}" id="from-order-book">
    @foreach ($data as $category)
        @if (count($category->list_products) > 0)
        <div class="panel panel-custom">
            <div class="panel-heading">
                <div class="title">
                    <h3>{{$category->name}}</h3>
                </div>
                <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#{{$category->category_id}}" aria-expanded="true" aria-controls="collapsePanel1"></span>
            </div>
            <div class="collapse in" id="{{$category->category_id}}">
                <div class="panel-body">
                        {{ csrf_field() }}
                        @if ($category->category_id == 3)
                        <table class="table table-custom table-hover">
                            <tr>
                                <th class="th-two">Modulecode</th>
                                <th>Druk</th>
                                <th>Titel</th>
                                <th class="th-price">Prijs</th>
                                <th class="th-checkbox text-right">
                                    <label class="checkbox-label empty">
                                        <input type="checkbox" class="styled checkbox-custom checked-all" checked>
                                    </label>
                                </th>
                            </tr>
                            @php
                                $products = $category->list_products;
                            @endphp
                            @foreach ($products as $item)
                            @php
                                $item->price = strval(number_format((float)$item->price, 2, ',', ''));
                                $checked = "";
                                if($item->checked){
                                    $checked = "checked";
                                }
                            @endphp
                            <tr>
                                <td>{{$item->modulecode}}</td>
                                <td>{{$item->moduuldruk}}</td>
                                <td>{{$item->name}}</td>
                                <td class="text-right"><span class="pull-left">€</span> <span>{{$item->price}}</span></td>
                                <td class="text-right">
                                    <label class="checkbox-label empty">
                                        @if ($item->checked)
                                            <input type="hidden" name="product_id[]" class="checkbox-elm product-id" value="{{$item->id}}">
                                        @endif
                                        <input type="checkbox" class="checkbox-elm styled checkbox-custom" pro-id="{{$item->id}}" {{$checked}}>
                                    </label>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        @elseif($category->category_id == 4)
                        <table class="table table-custom table-hover">
                            <tr>
                                <th class="th-two">ISBN</th>
                                <th>Druk</th>
                                <th>Titel</th>
                                <th class="th-price">Prijs</th>
                                <th class="th-checkbox text-right">
                                    <label class="checkbox-label empty">
                                        <input type="checkbox" class="styled checkbox-custom checked-all" checked>
                                    </label>
                                </th>
                            </tr>
                            @php
                                $products = $category->list_products;
                            @endphp
                            @foreach ($products as $item)
                            @php
                                $item->price = strval(number_format((float)$item->price, 2, ',', ''));
                                $checked = "";
                                if($item->checked){
                                    $checked = "checked";
                                }
                                if($item->writer == "NULL"){
                                    $item->writer = "";
                                }
                            @endphp
                            <tr>
                                <td>{{$item->isbn}}</td>
                                @if ($item->moduuldruk != "NULL")
                                    <td>{{$item->moduuldruk}}</td>
                                @else
                                <td></td>
                                @endif
                                <td>{{$item->name}}</td>
                                <td class="text-right"><span class="pull-left">€</span> <span>{{$item->price}}</span></td>
                                <td class="text-right">
                                    <label class="checkbox-label empty">
                                        @if ($item->checked)
                                            <input type="hidden" name="product_id[]" class="checkbox-elm product-id" value="{{$item->id}}">
                                        @endif
                                        <input type="checkbox" class="checkbox-elm styled checkbox-custom" pro-id="{{$item->id}}" {{$checked}}>
                                    </label>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        @elseif($category->category_id == 5)
                        <table class="table table-custom table-hover">
                            <tr>
                                <th class="th-two">Modulecode</th>
                                <th>Titel</th>
                                <th class="th-price">Prijs</th>
                                <th class="th-checkbox text-right">
                                    <!--label class="checkbox-label empty">
                                        <input type="checkbox" class="styled checkbox-custom checked-all" checked>
                                    </label-->
                                </th>
                            </tr>
                            @php
                                $products = $category->list_products;
                            @endphp
                            @foreach ($products as $item)
                            @php
                                $item->price = strval(number_format((float)$item->price, 2, ',', ''));
                                $checked = "";
                                if($item->checked){
                                    $checked = "checked";
                                }
                                $show_sku = "";
                                $show_price = "";
                                $show_modulecode = "";
                                $need_bold = true;
                                $show_symbol_euro = "";
                                if(strpos($item->sku,"_") > 2){
                                    $show_sku = $item->sku;
                                    $show_modulecode = $item->modulecode;
                                    $show_price = $item->price;
                                    $need_bold = false;
                                    $show_symbol_euro = "€";
                                }
                            @endphp
                            <tr>
                                <td>{{$show_modulecode}}</td>
                                <td <?php echo $need_bold == true ? 'style="font-weight:bold"' : '' ?>>{{$item->name}}</td>
                                <td class="text-right"><span class="pull-left"><?php echo $show_symbol_euro; ?></span> <span>{{$show_price}}</span></td>
                                <td class="text-right">
                                    <?php if($show_sku != ""){ ?>
                                    <label class="checkbox-label empty">
                                        @if ($item->checked)
                                            <input type="hidden" name="product_id[]" class="checkbox-elm product-id" value="{{$item->id}}">
                                        @endif
                                        <input type="checkbox" class="checkbox-elm styled checkbox-custom" pro-id="{{$item->id}}" {{$checked}}/>
                                    </label>
                                    <?php } ?>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        @elseif($category->category_id == 6)
                        <table class="table table-custom table-hover">
                            <tr>
                                <th></th>
                                <th>Title</th>
                                <th class="th-price">Prijs</th>
                                <th class="th-checkbox text-right">
                                    <label class="checkbox-label empty">
                                        <input type="checkbox" class="styled checkbox-custom checked-all">
                                    </label>
                                </th>
                            </tr>
                            @php
                                $products = $category->list_products;
                            @endphp
                            @foreach ($products as $item)
                            @php
                                $item->price = strval(number_format((float)$item->price, 2, ',', ''));
                                $checked = "";
                                if($item->checked){
                                    $checked = "checked";
                                    
                                }
                            @endphp
                            <tr>
                                @if (strpos($item->image, 'userfiles') == false)
                                    <td><img src="<?php echo env("APP_URL") ?>/media/catalog/product{{$item->image}}" alt="{{$item->name}}" class="img-thumbnail" style="max-width: 70px;height:70px;"></td>
                                @else
                                    <td><img src="{{$item->image}}" alt="{{$item->name}}" class="img-thumbnail" style="max-width: 80px;height:auto"></td>
                                @endif
                                <td>{{$item->name}}</td>
                                <td class="text-right"><span class="pull-left">€</span> <span>{{$item->price}}</span></td>
                                <td class="text-right">
                                    <label class="checkbox-label empty">
                                        @if ($item->checked)
                                            <input type="hidden" name="product_id[]" class="checkbox-elm product-id" value="{{$item->id}}">
                                        @endif
                                        <input type="checkbox" class="checkbox-elm styled checkbox-custom" pro-id="{{$item->id}}" {{$checked}}>
                                    </label>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        @else
                        <table class="table table-custom table-hover">
                            <tr>
                                <th class="th-two">Modulecode</th>
                                <th class="th-two">Druk</th>
                                <th>Titel</th>
                                <th class="th-price">Prijs</th>
                                <th class="th-checkbox text-right">
                                    <label class="checkbox-label empty">
                                        <input type="checkbox" class="styled checkbox-custom checked-all" checked>
                                    </label>
                                </th>
                            </tr>
                            @php
                                $products = $category->list_products;
                            @endphp
                            @foreach ($products as $item)
                            @php
                                $item->price = strval(number_format((float)$item->price, 2, ',', ''));
                                $checked = "";
                                if($item->checked){
                                    $checked = "checked";
                                }
                            @endphp
                            <tr>
                                <td>{{$item->modulecode}}</td>
                                <td>{{$item->moduuldruk}}</td>
                                <td>{{$item->name}}</td>
                                <td class="text-right"><span class="pull-left">€</span> <span>{{$item->price}}</span></td>
                                <td class="text-right">
                                    <label class="checkbox-label empty">
                                        @if ($item->checked)
                                            <input type="hidden" name="product_id[]" class="checkbox-elm product-id" value="{{$item->id}}">
                                        @endif
                                        <input type="checkbox" class="checkbox-elm styled checkbox-custom" pro-id="{{$item->id}}" {{$checked}}>
                                    </label>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        @endif
                    </div>
                </div>
            </div>
            @endif
        @endforeach
    </form>
@else
    
@endif
<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- The Close Button -->
  <span class="close">{{-- &times; --}}</span>

  <!-- Modal Content (The Image) -->
  <img class="modal-content" id="img01">

  <!-- Modal Caption (Image Text) -->
  <div id="caption"></div>
</div>
@push('scripts')
    <script>
        $(document).ready(function(){
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            $("input.checked-all").on("change",function(){
                var _this = $(this);
                var tableParent = _this.closest('table');
                if($(this).is(':checked')){
                    tableParent.find("input.checkbox-elm").prop('checked',true).trigger('change').trigger('refresh');
                }else{
                    tableParent.find("input.checkbox-elm").prop('checked',false).trigger('change').trigger('refresh');
                }
            });
            $(".img-thumbnail").on("click",function(){
                var img = $(this);
                $("#myModal").show();
                modalImg.src = img.attr("src");
                captionText.innerHTML = img.attr("alt");
            });
        });
        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            $("#myModal").hide();
        }
    </script>
@endpush
