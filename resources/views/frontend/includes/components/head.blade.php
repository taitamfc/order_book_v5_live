
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>@yield('title')</title>

<!-- Fonts -->
{{-- <link href="https://fonts.googleapis.com/css2?family=Ubuntu:ital,wght@0,400;0,500;0,700;1,300&display=swap" rel="stylesheet" type='text/css'> --}}
{{-- <link href='https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'> --}}

<!-- Bootstrap -->
<link href="/assets_fe/plugins/bootstrap/css/bootstrap.css?v={{env("VERSION_STYLE")}}" rel="stylesheet">

<!-- Plugins -->
<link href="/assets_fe/plugins/formstyler/css/jquery.formstyler.css?v={{env("VERSION_STYLE")}}" rel="stylesheet">
<link href="/assets_fe/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css?v={{env("VERSION_STYLE")}}" rel="stylesheet">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<!-- Styles -->
<link href="/assets_fe/css/styles.css?v={{env("VERSION_STYLE")}}" rel="stylesheet">
<link href="/assets_fe/css/custom.css?v={{env("VERSION_STYLE")}}" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

@stack('css')