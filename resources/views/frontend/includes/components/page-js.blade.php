 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
 <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
 <!-- Include all compiled plugins (below), or include individual files as needed -->
 <script src="/assets_fe/plugins/bootstrap/js/bootstrap.js"></script>
 <script src="/assets_fe/plugins/formstyler/js/jquery.formstyler.min.js"></script>
 <script src="/assets_fe/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
 <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
 <script src="https://www.amcharts.com/lib/3/pie.js"></script>
 <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
 <script src="/assets_fe/js/main.js?v={{env("VERSION_STYLE")}}"></script>

 @stack('scripts')