<style>
.img-checked{
    right: 17px;
    top: 14px;
    position: absolute;
    margin: auto;
}
.us-box.active{
    cursor:pointer;
}
.us-box.active.completed {
    border-color: green !important;
}
.us-box.active.completed .us-title{
    color: green !important;
}
</style>
<div class="user-steps">
    <div class="row">
        <div class="col-sm-3 col-xs-6 col-xxs-12 user-steps__col">
            <a role="text" step="1" class="us-box <?php echo $step_order['step-1'] == true ? 'active completed' : ''?>" <?php echo $step_order['step-1'] == true ? "href='/'" : ''; ?>>
                <div class="us-label">Stap 1</div>
                <div class="us-title" <?php echo $step_order['step-1'] == true ? 'style="color: #136697;"' : ''?>>Productkeuze</div>
                <?php if( $step_order['step-1'] == true ){ ?>
                <img src="{{asset('assets/img/tick.png')}}" class="img-checked" style="width:18px;height:18px" />
                <?php } ?>
            </a>
        </div>
        <div class="col-sm-3 col-xs-6 col-xxs-12 user-steps__col">
            <a role="text" step="2" class="us-box <?php echo $step_order['step-2'] == true ? 'active completed' : ''?>" <?php echo $step_order['step-2'] == true ? "href='/order-shipping'" : ''; ?>>
                <div class="us-label">Stap 2</div>
                <div class="us-title" <?php echo $step_order['step-2'] == true ? 'style="color: #e16a03;"' : ''?>>Afleveradres</div>
                <?php if( $step_order['step-2'] == true ){ ?>
                <img src="{{asset('assets/img/tick.png')}}" class="img-checked" style="width:18px;height:18px" />
                <?php } ?>
            </a>
        </div>
        <div class="col-sm-3 col-xs-6 col-xxs-12 user-steps__col">
            <a role="text" step="3" class="us-box <?php echo $step_order['step-3'] == true ? 'active completed' : ''?>" <?php echo $step_order['step-3'] == true ? "href='/order-details'" : ''; ?>>
                <div class="us-label">Stap 3</div>
                <div class="us-title" <?php echo $step_order['step-3'] == true ? 'style="color: #e16a03;"' : ''?>>Bestelling overzicht</div>
                <?php if( $step_order['step-3'] == true ){ ?>
                <img src="{{asset('assets/img/tick.png')}}" class="img-checked" style="width:18px;height:18px" />
                <?php } ?>
            </a>
        </div>
        <div class="col-sm-3 col-xs-6 col-xxs-12 user-steps__col">
            <a role="text" step="4" class="us-box <?php echo $step_order['step-4'] == true ? 'active completed' : ''?>" <?php echo $step_order['step-3'] == true ? "href='/order-payment'" : ''; ?>>
                <div class="us-label">Stap 4</div>
                <div class="us-title" <?php echo $step_order['step-4'] == true ? 'style="color: #e16a03;"' : ''?>>Betaling</div>
                <?php if( $step_order['step-4'] == true ){ ?>
                <img src="{{asset('assets/img/tick.png')}}" class="img-checked" style="width:18px;height:18px" />
                <?php } ?>
            </a>
        </div>
    </div>
</div>