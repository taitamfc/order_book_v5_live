<style>
    .inline-image{
        display:inline-block
    }
    .logo{
        margin-top: 20px;
        margin-bottom: auto;
    }
    .first-image{
        width: 40%;
        height: auto;
    }
    .second-image{
        width: 50%;
        height: auto;
        padding-top: 20px;
        
    }
    </style>
<header class="header" id="header">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 text-center-sm">
                    <div class="logo">
                        <a href="/">
                            <a href="/">
                                <img width="auto" height="96" src="/assets_fe/img/logo.png" alt="" class="inline-image first-image">
                                <img width="auto" height="96" src="/assets_fe/img/logo_boekenprint.png" alt="" class="inline-image second-image">
                            </a>
                        </a>
                    </div>
                </div>
                <div class="col-sm-7 text-center">
                    <div class="row">
                        <div class="col-sm-10">
                            <ul class="header header-search menu">
                                <li><a href="/">HOME</a></li>
                                <li><a href="/faq">VEELGESTELDE VRAGEN</a></li>
                                <li><a href="/contact">contact</a></li>
                            </ul>
                        </div>
                        {{-- <div class="col-sm-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-search"></span>
                                </div>
                                <input type="text" class="form-control ui-autocomplete-input" id="site-search" autocomplete="off">
                            </div>
                        </div> --}}
                        <div class="col-sm-2">
                            {{-- <ul class="list-group">
                                @php
                                    $student = Auth::guard('student')->user();
                                    $student_name = $student->firstname .' '. $student->lastname; 
                                @endphp
                                <li class="list-group-item"><a href="#">{{$student_name}}</a></li>
                                <li class="list-group-item gray">
                                    <a class="btn btn-success btn-xs" href="{{route('student.manage.info')}}"><span class="glyphicon glyphicon-cog"></span></a>
                                    <a class="btn btn-danger btn-xs" href="{{route('student.logout')}}"><span class="glyphicon glyphicon-off"></span></a>
                                </li>
                            </ul> --}}
                            <div class="list-group">
                                <a class="btn btn-xs btn-default" href="{{route('student.manage.info')}}"><img class="icon" style="width: 20px" 
                                src="/assets/img/user_profile.png" alt=""></a>
                                <a class="btn btn-xs btn-default" href="{{route('student.logout')}}"><img class="icon" style="width: 20px" 
                                src="/assets/img/logout.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="header-middle pad10">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <nav class="navbar navbar-clear green">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-left">
                                <li><a href="#">BESTELLINGEN</a></li>
                                <li><a href="#">STUDENTEN</a></li>
                                <li><a href="#">PRODUCTEN</a></li>
                                <li class="active"><a href="#">STATISTIEKEN</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div> --}}
</header>