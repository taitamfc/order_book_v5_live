@if ($cms_pages)
<ul class="cms-page">
    @foreach ($cms_pages as $page)
    <li>
        <a href="{{route('cms.view',['slug' => $page->slug])}}">{!! $page->title !!}</a>
    </li>
    @endforeach
</ul>
@endif


