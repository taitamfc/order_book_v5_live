<style>
.inline-image{
    display:inline-block
}
.logo{
    margin-top: 20px;
    margin-bottom: auto;
}
.first-image{
    width: 40%;
    height: auto;
}
.second-image{
    width: 50%;
    height: auto;
    padding-top: 20px;
    
}
</style>
<header class="header" id="header">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 text-center-sm">
                    <div class="logo">
                        <a href="/">
                            <img width="auto" height="96" src="/assets_fe/img/logo.png" alt="" class="inline-image first-image">
                            <img width="auto" height="96" src="/assets_fe/img/logo_boekenprint.png" alt="" class="inline-image second-image">
                        </a>
                    </div>
                </div>
                <div class="col-sm-7 text-center-sm">
                    <div class="">
                        <ul class="header menu">
                            <li><a href="/">HOME</a></li>
                            <li><a href="/faq">VEELGESTELDE VRAGEN</a></li>
                            <li><a href="/contact">contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header><!-- end .header -->