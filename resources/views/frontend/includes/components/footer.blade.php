<footer class="footer" id="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="copyright text-left">
                    Copyright {{date('Y')}} Aeres
                    @include('frontend.includes.components.cms-pages')
                </div>
            </div>
        </div>
    </div>
</footer><!-- end .footer -->