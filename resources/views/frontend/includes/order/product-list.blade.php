
@if (count($data) > 0)
    @php
        $sub_total = 0;
        $total_tax_9_percent = 0;
        $total_tax_21_percent = 0;
        $btw_09 = $config_category["vat_9"];
        $btw_21 = $config_category["vat_21"];
        $slug_category_backtoschool  = $config_category["category_back_to_school"];
    @endphp
    @foreach ($data as $category)
    @if (count($category->list_products) > 0)
    <div class="panel panel-custom">
        <div class="panel-heading">
            <div class="title">
                <h3>{{$category->name}}</h3>
            </div>
            <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#{{$category->category_id}}" aria-expanded="true" aria-controls="collapsePanel1"></span>
        </div>
        <div class="collapse in" id="{{$category->category_id}}">
            <div class="panel-body">
                    @if ($category->category_id == 3)
                    <table class="table table-custom">
                        <tr>
                            <th class="th-two">Modulecode</th>
                            <th>Druk</th>
                            <th>Titel</th>
                            <th class="th-price">Prijs</th>
                            <th class="th-checkbox">&nbsp;</th>
                        </tr>
                        @php
                            $products = $category->list_products;
                        @endphp
                        @foreach ($products as $item)
                        <?php 
                        /* $tax_class_id = $item->tax_class_id; */
                        if($category->slug != $slug_category_backtoschool){
                            $total_tax_9_percent += $item->price * $btw_09;
                        }
                        else{
                            /* comment this */
                            //$total_tax_21_percent += $item->price * $btw_21;
                        }
                        /*add this*/
                        $total_tax_21_percent += $item->price * $btw_21;
                        ?>
                        @php
                            $sub_total += $item->price;
                            $item->price = strval(number_format((float)$item->price, 2, ',', ''));
                        @endphp
                        <tr>
                            <td>{{$item->modulecode}}</td>
                            <td>{{$item->moduuldruk}}</td>
                            <td>{{$item->name}}</td>
                            <td class="text-right"><span class="pull-left">€</span> <span>{{$item->price}}</span></td>
                            <td class="text-right">
                                <label class="checkbox-label empty">
                                    <input type="checkbox" class="styled checkbox-custom" disabled checked>
                                </label>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    @elseif($category->category_id == 4)
                    <table class="table table-custom">
                        <tr>
                            <th class="th-two">ISBN</th>
                            <th>Druk</th>
                            <th>Titel</th>
                            <th class="th-price">Prijs</th>
                            <th class="th-checkbox">&nbsp;</th>
                        </tr>
                        @php
                            $products = $category->list_products;
                        @endphp
                        @foreach ($products as $item)
                        <?php 
                        /* $tax_class_id = $item->tax_class_id; */
                        if($category->slug != $slug_category_backtoschool){
                            $total_tax_9_percent += $item->price * $btw_09;
                        }
                        else{
                            /* comment this */
                            //$total_tax_21_percent += $item->price * $btw_21;
                        }
                        /*add this*/
                        $total_tax_21_percent += $item->price * $btw_21;
                        ?>
                        @php
                            $sub_total += $item->price;
                            $item->price = strval(number_format((float)$item->price, 2, ',', ''));
                        @endphp
                        <tr>
                            <td>{{$item->isbn}}</td>
                            <td>{{$item->moduuldruk}}</td>
                            <td>{{$item->name}}</td>
                            <td class="text-right"><span class="pull-left">€</span> <span>{{$item->price}}</span></td>
                            <td class="text-right">
                                <label class="checkbox-label empty">
                                    <input type="checkbox" class="styled checkbox-custom" disabled checked>
                                </label>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    @elseif($category->category_id == 5)
                    <table class="table table-custom">
                        <tr>
                            <th class="th-two">Modulecode</th>
                            <th>Titel</th>
                            <th class="th-price">Prijs</th>
                            <th class="th-checkbox">&nbsp;</th>
                        </tr>
                        @php
                            $products = $category->list_products;
                        @endphp
                        @foreach ($products as $item)
                        <?php 
                        /* $tax_class_id = $item->tax_class_id; */
                        if($category->slug != $slug_category_backtoschool){
                            $total_tax_9_percent += $item->price * $btw_09;
                        }
                        else{
                            /* comment this */
                            //$total_tax_21_percent += $item->price * $btw_21;
                        }
                        /*add this*/
                        $total_tax_21_percent += $item->price * $btw_21;
                        ?>
                        @php
                            $sub_total += $item->price;
                            $item->price = strval(number_format((float)$item->price, 2, ',', ''));
                        @endphp
                        <tr>
                            <td>{{$item->modulecode}}</td>
                            <td>{{$item->name}}</td>
                            <td class="text-right"><span class="pull-left">€</span> <span>{{$item->price}}</span></td>
                            <td class="text-right">
                                <label class="checkbox-label empty">
                                    <input type="checkbox" class="styled checkbox-custom" disabled checked>
                                </label>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    @elseif($category->category_id == 6)
                    <table class="table table-custom">
                        <tr>
                            <th></th>
                            <th>Title</th>
                            <th class="th-price">Prijs</th>
                            <th class="th-checkbox">&nbsp;</th>
                        </tr>
                        @php
                            $products = $category->list_products;
                        @endphp
                        @foreach ($products as $item)
                        <?php 
                        /* $tax_class_id = $item->tax_class_id; */
                        if($category->slug != $slug_category_backtoschool){
                            $total_tax_9_percent += $item->price * $btw_09;
                        }
                        else{
                            /* comment this */
                            //$total_tax_21_percent += $item->price * $btw_21;
                        }
                        /*add this*/
                        $total_tax_21_percent += $item->price * $btw_21;
                        ?>
                        @php
                            $sub_total += $item->price;
                            $item->price = strval(number_format((float)$item->price, 2, ',', ''));
                        @endphp
                        <tr>
                            @if (strpos($item->image, 'userfiles') == false)
                            <td><img src="<?php echo env("APP_URL") ?>/media/catalog/product{{$item->image}}" alt="{{$item->name}}" class="img-thumbnail" style="max-width: 70px;height:70px;"></td>
                            @else
                                <td><img src="{{$item->image}}" alt="{{$item->name}}" class="img-thumbnail" style="max-width: 80px;height:auto"></td>
                            @endif
                            <td>{{$item->name}}</td>
                            <td class="text-right"><span class="pull-left">€</span> <span>{{$item->price}}</span></td>
                            <td class="text-right">
                                <label class="checkbox-label empty">
                                    <input type="checkbox" class="styled checkbox-custom" disabled checked>
                                </label>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    @else
                    <table class="table table-custom">
                        <tr>
                            <th class="th-two">Modulecode</th>
                            <th>Druk</th>
                            <th>Titel</th>
                            <th class="th-price">Prijs</th>
                            <th class="th-checkbox">&nbsp;</th>
                        </tr>
                        @php
                            $products = $category->list_products;
                        @endphp
                        @foreach ($products as $item)
                        <?php 
                        /* $tax_class_id = $item->tax_class_id; */
                        if($category->slug != $slug_category_backtoschool){
                            $total_tax_9_percent += $item->price * $btw_09;
                        }
                        else{
                            /* comment this */
                            //$total_tax_21_percent += $item->price * $btw_21;
                        }
                        /*add this*/
                        $total_tax_21_percent += $item->price * $btw_21;
                        ?>
                        @php
                            $sub_total += $item->price;
                            $item->price = strval(number_format((float)$item->price, 2, ',', ''));
                        @endphp
                        <tr>
                            <td>{{$item->modulecode}}</td>
                            <td>{{$item->moduuldruk}}</td>
                            <td>{{$item->name}}</td>
                            <td class="text-right"><span class="pull-left">€</span> <span>{{$item->price}}</span></td>
                            <td class="text-right">
                                <label class="checkbox-label empty">
                                    <input type="checkbox" class="styled checkbox-custom" disabled checked>
                                </label>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    @endif
                </div>
            </div>
        </div>
    @endif
@endforeach

<div class="panel panel-custom">
    <div class="collapse in" id="collapsePanel1">
        <div class="panel-body">
            <table class="table table-custom table-total">
                    <tbody>
                        <tr>
                            <td></td>
                            <td style="width:150px"></td>
                        </tr>
                        <tr>
                            <td style="text-align: right">Subtotaal incl BTW</td>
                            <td style="text-align: right">€ {{strval(number_format((float)$sub_total, 2, ',', ''))}}</td>
                        </tr>
                        
                        <tr>
                            <td style="text-align: right">Verzending en verwerking</td>
                            <td style="text-align: right">€ 10,00</td>
                        </tr>
                        <?php 
                            $grand_total = $sub_total + 10;
                        ?>
                        <tr>
                            <td style="text-align: right"><strong>Te betalen</strong></td>
                            <td style="text-align: right"><strong>€ {{strval(number_format((float)$grand_total, 2, ',', ''))}}</strong></td>
                        </tr>
                        <tr>
                            <td style="text-align: right">Waarvan BTW(9%)</td>
                            <td style="text-align: right">€ {{strval(number_format((float)$total_tax_9_percent, 2, ',', ''))}}</td>
                        </tr>
                        <tr>
                            <td style="text-align: right">Waarvan BTW(21%)</td>
                            <td style="text-align: right">€ {{strval(number_format((float)$total_tax_21_percent, 2, ',', ''))}}</td>
                        </tr>
                    </tbody>
                </table>    
            </div>
        </div>
    </div>  
@else
    
@endif
