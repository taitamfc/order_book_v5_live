<form action="{{route('student.order.addShipInfo')}}" method="post">
    <button type="submit" class="hidden" id="ship-info"></button>
    {{csrf_field()}}
    <div class="form-group">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <span>Leerlingnummer:</span>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <span class="read-only form-control">{{$student_info->username}}</span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <span>Voornaam:</span>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <span class="read-only form-control">{{$student_info->firstname}}</span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <span>Tussenvoegsel:</span>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <span class="read-only form-control">{{$student_info->middlename}}</span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <span>Achternaam:</span>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <span class="read-only form-control">{{$student_info->lastname}}</span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <span>Klas:</span>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <span class="read-only form-control">{{$student_info->customer_group_code}}</span>
            </div>
        </div>
    </div>
    <br>
    <div class="form-group">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <span>Adres:</span>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <input type="text" class="form-control" required name="street" value="{{$student_info->street}}">
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <span>Plaats:</span>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <input type="text" class="form-control" required name="city" value="{{$student_info->city}}">
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <span>Postcode:</span>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <input type="text" class="form-control" required name="postcode" value="{{$student_info->postcode}}">
            </div>
        </div>
    </div>
    <div class="form-group" style="display: none;">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <span>Bankrekeningnummer:</span>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <input type="text" class="form-control" required name="bank_account" value="{{$student_info->bank_account}}">
            </div>
        </div>
    </div>
    <br>
    <div class="form-group">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <span>E-mailadres:</span>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <input type="text" class="form-control" required name="email" id="email" value="{{$student_info->email}}">
            </div>
        </div>
    </div>
	<div class="form-group">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <span>Bevestig e-mailadres:</span>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <input type="text" class="form-control" required id="re-email" name="re-email" value="">
                <span id="error-email" style="display:none">Het e-mailadres komt niet overeen met het bovenstaande e-mailadres.</span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <span>Telefoonnummer:</span>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <input type="text" class="form-control" required name="telephone" value="{{$student_info->telephone}}">
            </div>
        </div>
    </div>
    <br>
</form>

