@extends('frontend.includes.student-layout')

@section('title', 'Inloggen')

@section('header')
    @include('frontend.includes.components.header')
@endsection

@section('content')
<div class="top-line">
    <div class="container">
        <div class="row">
            <div class="top-content-login">
                <div class="col-lg-5 login-top-text">
                    <h3>
                        Welkom op de boekenbestelsite van <br>
                        AERES MBO Barneveld in samenwerking <br>
                        met Totdrukwerk.
                    </h3>
                    </p>
                    <p>Via deze bestelsite kun je de benodigde lesbundels, (commerciële) boeken en eventuele keuzepakketten bestellen. Ook dit jaar hebben <br>we weer leuke schoolbenodigdheden toegevoegd aan de bestelsite. <br>Deze vind je terug onder de kantoorartikelen.
                    </p>
                    <p>Je boekenbestelling wordt bij je thuis afgeleverd. <br><b>Wel zo gemakkelijk!</b></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main inloggen-content" id="main">
    <div class="container">
        <div class="row">
            <div class="">
                <div class="panel panel-custom">
                    <div class="block-title"><h2>{!! ($cms_page->title) !!}</h2></div>
                    <div class="panel-body panel-content panel-content-other">
                        <div class="content">
                            @if ($cms_page)
                                {!! html_entity_decode($cms_page->content) !!}
                            @else
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                            @endif            
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <br>
       <br>
       <br>
    </div>
</div>
@endsection

@push('scripts')
    
@endpush
