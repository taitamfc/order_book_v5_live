@extends('frontend.includes.student-layout')

@section('title', 'MIJN ACCOUNT')

@section('header')
    @include('frontend.includes.components.header-search')
@endsection

@section('content')
<div class="container account-info">
    <div class="row justify-content-center">
        <div class="col-md-3 col-sm-4 col-xs-12">
            @include('frontend.includes.student.nav-info')
        </div>
        <div class="col-md-9 col-sm-8 col-xs-12">
            @if ($errors->any() || Session::has('error'))
                <div class="alert alert-danger">
                    <ul>
                        @if(Session::has('error'))
                            <li>{{Session::get('error')}}</li>
                        @endif
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
			@if (Session::has('success'))
                <div class="alert alert-success">
                    <ul>
                        @if(Session::has('success'))
                            <li>{{Session::get('success')}}</li>
                        @endif
                    </ul>
                </div>
            @endif
            <div class="block-content">
                <div class="my-account"><h4 class="legend">Bestelt Informatie</h4>
                    <div class="lst-products">
                        @if (count($order->items) > 0)
                        @php
                            $sub_total = 0;
                        @endphp
                        <div class="panel panel-custom">
                            <div class="panel-heading">
                                <div class="title">
                                    <h3>Bestelt Artikelen</h3>
                                </div>
                                <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel1" aria-expanded="true" aria-controls="collapsePanel1"></span>
                            </div>
                            <div class="collapse in" id="collapsePanel1">
                                <div class="panel-body">
                                    <table class="table table-custom">
                                        <tr>
                                            <th class="">Modulecode</th>
                                            <th class="">isbn</th>
                                            <th class="">Druk</th>
                                            <th>Titel</th>
                                            <th class="th-price">Prijs</th>
                                        </tr>
                                        @foreach ($order->items as $item)
                                        @php
                                            $sub_total += $item->item_price;
                                        @endphp
                                        <tr>
                                            @if ($item->modulecode != "NULL")
                                            <td>{{$item->modulecode}}</td>
                                            @else
                                            <td></td>
                                            @endif
                                            @if ($item->isbn != "NULL")
                                            <td>{{$item->isbn}}</td>
                                            @else
                                            <td></td>
                                            @endif
                                            @if ($item->moduuldruk != "NULL")
                                            <td>{{$item->moduuldruk}}</td>
                                            @else
                                            <td></td>
                                            @endif
                                            <td>{{$item->name}}</td>
                                            <td class="text-right"><span class="pull-left">€</span> <span>{{strval(number_format((float)$item->item_price, 2, ',', ''))}}</span></td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="4" style="text-align: center"><strong>Totaal ({{count($order->items)}})</strong></td>
                                            <td class="text-right"><span class="pull-left">€</span> <span><strong>{{strval(number_format((float)$sub_total + 10, 2, ',', ''))}}</strong></span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="panel panel-custom">
                        <div class="panel-heading">
                            <div class="title">
                                <h3>Betalingsinformatie</h3>
                            </div>
                            <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel1" aria-expanded="true" aria-controls="collapsePanel1"></span>
                        </div>
                        <div class="collapse in" id="collapsePanel1">
                            <div class="panel-body">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Betalingsstatus:</label>
                                        <div class="col-sm-5">
                                            <span class="read-only form-control">{{$order->payment->status}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Totaalbedrag:</label>
                                        <div class="col-sm-5">
                                            <span class="read-only form-control">€ {{strval(number_format((float)$order->payment->amount, 2, ',', ''))}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Betalingswijze:</label>
                                        <div class="col-sm-5">
                                            <span class="read-only form-control">{{$order->payment->method}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Gemaakt:</label>
                                        <div class="col-sm-5">
                                            <span class="read-only form-control">{{$order->payment->created_at}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Verloopt op:</label>
                                        <div class="col-sm-5">
                                            <span class="read-only form-control">{{$order->payment->expires_at}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">betaald op:</label>
                                        <div class="col-sm-5">
                                            <span class="read-only form-control">{{$order->payment->paid_at}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">link afrekenen:</label>
                                        <div class="col-sm-5">
                                            <a class="btn btn-sm btn-primary" href="{{$order->payment->link_checkout}}">LINK</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-custom">
                        <div class="panel-heading">
                            <div class="title">
                                <h3>Naam</h3>
                            </div>
                            <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel1" aria-expanded="true" aria-controls="collapsePanel1"></span>
                        </div>
                        <div class="collapse in" id="collapsePanel1">
                            <div class="panel-body">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Voornaam:</label>
                                        <div class="col-sm-5">
                                            <span class="read-only form-control">{{$order->username}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Tussenvoegsel:</label>
                                        <div class="col-sm-5">
                                            <span class="read-only form-control">{{$order->middlename}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Achternaam:</label>
                                        <div class="col-sm-5">
                                            <span class="read-only form-control">{{$order->lastname}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-custom">
                        <div class="panel-heading">
                            <div class="title">
                                <h3>PERSOONLIJKE INFORMATIE</h3>
                            </div>
                            <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel2" aria-expanded="true" aria-controls="collapsePanel2"></span>
                        </div>
                        <div class="collapse in" id="collapsePanel2">
                            <div class="panel-body">
                                <form action="#" method="post" class="form-horizontal">
                                    <div class="form-group">
                                        <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">E-mailadres:</label>
                                        <div class="col-sm-5">
                                            <span class="read-only form-control">{{$order->email}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Telefoonnummer:</label>
                                        <div class="col-sm-5">
                                            <span class="read-only form-control">{{$order->telephone}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Bankrekeningnummer:</label>
                                        <div class="col-sm-5">
                                            <span class="read-only form-control">{{$order->order_bank_account}}</span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-custom">
                        <div class="panel-heading">
                            <div class="title">
                                <h3>ADRES</h3>
                            </div>
                            <span class="glyphicon glyphicon-menu-down" data-toggle="collapse" data-target="#collapsePanel3" aria-expanded="true" aria-controls="collapsePanel3"></span>
                        </div>
                        <div class="collapse in" id="collapsePanel3">
                            <div class="panel-body">
                                <form action="#" method="post" class="form-horizontal">
                                    <div class="form-group">
                                        <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Straat:</label>
                                        <div class="col-sm-5">
                                            <span class="read-only form-control">{{$order->street}}</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Postcode:</label>
                                        <div class="col-sm-5">
                                            <span class="read-only form-control">{{$order->postcode}}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="label_name" class="control-label col-sm-3 col-sm-offset-1">Plaats:</label>
                                        <div class="col-sm-5">
                                            <span class="read-only form-control">{{$order->city}}</span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            var pathname = window.location.pathname;
            var menuItem = $(".account-nav").find('a[href="/account/order-histories"]').closest('li');
            menuItem.addClass('active');
        });
    </script>
@endpush



