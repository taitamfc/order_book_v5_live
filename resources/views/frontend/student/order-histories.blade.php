@extends('frontend.includes.student-layout')

@section('title', 'MIJN ACCOUNT')

@section('header')
    @include('frontend.includes.components.header-search')
@endsection

@section('content')
<div class="container account-info">
    <div class="row justify-content-center">
        <div class="col-md-3 col-sm-4 col-xs-12">
            @include('frontend.includes.student.nav-info')
        </div>
        <div class="col-md-9 col-sm-8 col-xs-12">
            @if ($errors->any() || Session::has('error'))
                <div class="alert alert-danger">
                    <ul>
                        @if(Session::has('error'))
                            <li>{{Session::get('error')}}</li>
                        @endif
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
			@if (Session::has('success'))
                <div class="alert alert-success">
                    <ul>
                        @if(Session::has('success'))
                            <li>{{Session::get('success')}}</li>
                        @endif
                    </ul>
                </div>
            @endif
            <div class="block-content">
                <div class="my-account"><h4 class="legend">Bestellingen</h4>
                    <div class="panel panel-custom">
                        <div class="panel-body panel-content">
                            <div class="table-responsive">
                                <table class="table table-custom">
                                    <tr>
                                        <th class="">FACTUUR ID</th>
                                        <th>BESTELDATUM</th>
                                        <th>TOTAALBEDRAG</th>
                                        <th>STATUS</th>
                                        <th>FACTUUR</th>
                                        <th class="th-flex">ACTIE</th>
                                    </tr>
                                    @if ($orders)
                                        @foreach ($orders as $order_item)
                                            @php
                                                $class_status = "label-warning";
                                                if($order_item->status == "comleted"){
                                                    $order_item->status = "Afgerond";
                                                    $class_status = "label-success";
                                                }
                                                if($order_item->status == "pending"){
                                                    $order_item->status = "In Behandeling";
                                                }
                                            @endphp
                                            <tr>
                                                <td class="td-nowrap">{{$order_item->order_id}}</td>
                                                <td class="td-nowrap">{{$order_item->created_at}}</td>
                                                <td style="font-weight: 600">€ {{strval(number_format((float)$order_item->grand_total, 2, ',', ''))}}</td>
                                                <td>
                                                    <label class="label {{$class_status}}" style="text-transform: uppercase">{{$order_item->status}}</label>
                                                </td>
                                                @if ($order_item->invoice_pdf)
                                                <td>  
                                                    <a href="{{$order_item->invoice_pdf}}" download class="btn btn-danger btn-sm">Downloaden</a>
                                                </td>
                                                @else
                                                <td>  
                                                </td>
                                                @endif
                                                <td>
                                                    <a href="{{route('student.order.info',['order_id' => $order_item->order_id])}}" class="btn btn-success btn-sm">Bestel Details</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div><!-- .panel -->
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
@endsection

@push('scripts')
    <script>
        (function($){
            $(document).ready(function(){
                var pathname = window.location.pathname;
                var menuItem = $(".account-nav").find('a[href="/account/order-histories"]').closest('li');
                menuItem.addClass('active');
            });
        })(jQuery);
    </script>
@endpush