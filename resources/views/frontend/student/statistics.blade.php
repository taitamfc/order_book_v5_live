@extends('frontend.includes.student-layout')

@section('title', 'Statistieken')

@section('header')
    @include('frontend.includes.components.header-search')
@endsection

@section('content')


<div class="main" id="main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="title">
                            <h3>Statistieken</h3>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form action="#" method="post">
                            <div class="row row10">
                                <div class="col-md-3 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                    <div class="input-group date" data-provide="datepicker">
                                        <input type="text" class="form-control" value="01 / 01 / 2016">
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                    <div class="input-group date" data-provide="datepicker">
                                        <input type="text" class="form-control" value="01 / 01 / 2016">
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row row10">
                                <div class="col-md-3 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                    <select class="form-control">
                                        <option value="">Algemene assistentie</option>
                                        <option value="">Algemene assistentie1</option>
                                        <option value="">Algemene assistentie2</option>
                                    </select>
                                </div>
                                <div class="col-md-3 col-sm-4 col-xs-6 col-xxs-12 form-group">
                                    <select class="form-control">
                                        <option value="">Indeed</option>
                                        <option value="">Indeed1</option>
                                        <option value="">Indeed2</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div id="chartdiv"></div>
                                </div>
                                <div class="col-lg-6">
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <span>Totaal aantal bestellingen:</span>
                                            <br>
                                            <span class="black medium-font">811</span>
                                        </li>
                                        <li class="list-group-item">
                                            <span>Studenten nog geen bestelling gedaan:</span>
                                            <br>
                                            <span class="black medium-font">407</span>
                                        </li>
                                        <li class="list-group-item">
                                            <span>Gemiddeld bedrag per bestelling:</span>
                                            <br>
                                            <span class="black medium-font">€ 139,47</span>
                                        </li>
                                        <li class="list-group-item">
                                            <span>Totaal bedrag uit bestellingen:</span>
                                            <br>
                                            <span class="black medium-font">€ 113.110,17</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-custom small-font">
                                    <tr>
                                        <th>ID</th>
                                        <th>Product</th>
                                        <th>Categorie</th>
                                        <th>Leverancier</th>
                                        <th class="td-nowrap">Aantal besteld</th>
                                        <th class="td-nowrap">Totale opbrengst</th>
                                    </tr>
                                    <tr>
                                        <td>593</td>
                                        <td class="td-nowrap">Algemene assistentie</td>
                                        <td class="td-nowrap">Lesbundels</td>
                                        <td><a href="#" class="link">Indeed</a></td>
                                        <td>37</td>
                                        <td>€ 240,50</td>
                                    </tr>
                                </table>
                            </div>
                        </form>
                    </div>
                </div><!-- .panel -->
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')


@endpush