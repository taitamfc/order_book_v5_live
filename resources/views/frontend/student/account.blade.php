@extends('frontend.includes.student-layout')

@section('title', 'MIJN ACCOUNT')

@section('header')
    @include('frontend.includes.components.header-search')
@endsection

@section('content')
@php
    $full_name = $student->firstname . ' ' . $student->middlename . ' ' . $student->lastname;
@endphp

<div class="container account-info">
    <div class="row justify-content-center">
        <div class="col-md-3 col-sm-4 col-xs-12">
            @include('frontend.includes.student.nav-info')
        </div>
        <div class="col-md-9 col-sm-8 col-xs-12">
            @if ($errors->any() || Session::has('error'))
                <div class="alert alert-danger">
                    <ul>
                        @if(Session::has('error'))
                            <li>{{Session::get('error')}}</li>
                        @endif
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
			@if (Session::has('success'))
                <div class="alert alert-success">
                    <ul>
                        @if(Session::has('success'))
                            <li>{{Session::get('success')}}</li>
                        @endif
                    </ul>
                </div>
            @endif
            <div class="welcome-msg">
                <p class="hello"><strong>Hallo, {{$full_name}}!</strong></p>
                <p>Uw persoonlijke pagina biedt u een overzicht van uw meest recente activiteiten. U kunt o.a. bestellingen inzien en accountgegevens bijwerken. Klik hieronder op een link om aan de slag te gaan!</p>
            </div>
            <div class="block-content">
                <div class="my-account"><h4 class="legend">Account Informatie</h4>
                    <form action="{{route('student.manage.editInfo')}}" method="post" id="form-validate" autocomplete="off">
                        <div class="row">
                            @csrf()
                            <input type="hidden" id="customer_id" name="customer_id" value="{{$student->customer_id}}">
                            <div class="form-group col-md-12 no-padding">
                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-full form-group">
                                    <label for="username">Username</label>
                                    <label for="username" class="form-control">{{$student->username}}</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-12 no-padding">
                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-full form-group">
                                    <label for="firstname" class="required">Voornaam</label>
                                    <input type="text" id="firstname" name="firstname" value="{{$student->firstname}}" required title="Voornaam" maxlength="255" class="form-control required-entry">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-full form-group">
                                    <label for="middlename" class="required">Tussenvoegsel</label>
                                    <input type="text" id="middlename" name="middlename" value="{{$student->middlename}}" title="Tussenvoegsel" maxlength="255" class="form-control required-entry">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-full form-group">
                                    <label for="lastname" class="required">Achternaam</label>
                                    <input type="text" id="lastname" name="lastname" value="{{$student->lastname}}" required title="Achternaam" maxlength="255" class="form-control required-entry">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-full form-group">
                                    <label for="email" class="required">E-mailadres</label>
                                    <input type="text" name="email" id="email" value="{{$student->email}}" title="E-mailadres" required class="form-control required-entry validate-email">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-full form-group">
                                    <label for="email" class="required">Telefoonnummer</label>
                                    <input type="text" name="telephone" id="telephone" value="{{$student->telephone}}" title="Telefoonnummer" required class="form-control required-entry">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-full form-group">
                                    <label for="email" class="required">Bankrekeningnummer</label>
                                    <input type="text" name="bank_account" id="bank_account" value="{{$student->bank_account}}" title="Bankrekeningnummer" required class="form-control required-entry">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="street" class="required">Adres</label>
                                <input type="text" name="street" id="street" value="{{$student->street}}" title="Adres" required class="form-control required-entry">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="city" class="required">Plaats</label>
                                <input type="text" name="city" id="city" value="{{$student->city}}" title="Plaats" required class="form-control required-entry">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="postcode" class="required">Postcode</label>
                                <input type="text" name="postcode" id="postcode" value="{{$student->postcode}}" title="Postcode" required class="form-control required-entry">
                            </div>
                        </div>
                                
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <span>
                                    <label class="checkbox-inline" for="change_password">
                                        <input type="checkbox" name="change_password" id="change_password" value="0" title="Wachtwoord wijzigen" class="checkbox">
                                        Wachtwoord wijzigen
                                    </label>
                                </span>
                            </div>
                        </div>
                        
                        <div class="fieldset block-change-pass" style="display: none">
                            <h4>Wachtwoord wijzigen</h4>	
                            <div class="row">
                                <div>
                                    <div class="col-md-12 form-group">
                                        <label for="current_password" class="required">Huidig wachtwoord</label>
                                        <input type="password" title="Huidig wachtwoord" class="form-control required-entry" name="current_password" id="current_password">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12 no-padding">
                                    <div class="col-md-6">
                                        <label for="password" class="required">Nieuw wachtwoord</label>
                                        <input type="password" title="Nieuw wachtwoord" class="form-control validate-password required-entry" name="password" id="password">
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <label for="confirmation" class="required">Bevestig nieuw wachtwoord</label>
                                        <input type="password" title="Bevestig nieuw wachtwoord" class="form-control validate-cpassword required-entry" name="password_confirmation" id="password_confirmation">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <input type="submit" data-loading-text="Loading..." class="btn btn-primary push-bottom" value="Opslaan">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
@endsection

@push('scripts')
    <script>
        (function($){
            $(document).ready(function(){
                var pathname = window.location.pathname;
                var menuItem = $(".account-nav").find('a[href="/account/info"]').closest('li');
                menuItem.addClass('active');

                $("#change_password").on("click",function(){
                    if($(this).is(":checked")){
                        $(this).attr('value',1)
                        $(".block-change-pass").find("input").attr('required',true);
                        $(".block-change-pass").slideDown();
                    }
                    else{
                        $(this).attr('value',0)
                        $(".block-change-pass").slideUp();
                        $(".block-change-pass").find("input").removeAttr('required');
                    }
                });
            });
        })(jQuery);
    </script>
@endpush
