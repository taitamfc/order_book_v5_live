@extends('frontend.includes.student-layout')

@section('title', 'Inloggen')

@section('header')
    @include('frontend.includes.components.header')
@endsection

@section('content')
<div class="top-line">
    <div class="container">
        <div class="row">
            <div class="top-content-login">
                <div class="col-lg-5 login-top-text">
                    <h3>
                        Welkom op de boekenbestelsite van <br>
                        AERES MBO Barneveld in samenwerking <br>
                        met Totdrukwerk.
                    </h3>
                    </p>
                    <p>Via deze bestelsite kun je de benodigde lesbundels, (commerciële) boeken en eventuele keuzepakketten bestellen. Ook dit jaar hebben <br>we weer leuke schoolbenodigdheden toegevoegd aan de bestelsite. <br>Deze vind je terug onder de kantoorartikelen.
                    </p>
                    <p>Je boekenbestelling wordt bij je thuis afgeleverd. <br><b>Wel zo gemakkelijk!</b></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main inloggen-content" id="main">
    <div class="container">
        <div class="row">
            <div class="">
                <div class="panel panel-custom">
                    <div class="block-title"><h2>Contact</h2></div>
                    <div class="panel-body">
                        @if (session('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('error') }}
                            </div>
                        @endif
                        @if (session('status'))
                        <div class="alert alert-success" role="alert"> 
                            {{ session('status') }}
                        </div>
                        @endif
                        <form action="{{route('student.contact.post')}}" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        <label for="label_name" class="control-label black">Studentnummer:<span style="color: red">*</span></label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" name="student_number" required class="form-control form-lg" placeholder="" value="{{ old('student_number') }}" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        <label for="label_name" class="control-label black">Klas:<span style="color: red">*</span></label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" name="class" required class="form-control form-lg" placeholder="" value="{{ old('class') }}" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        <label for="label_name" class="control-label black">Naam:<span style="color: red">*</span></label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" name="full_name" required class="form-control form-lg" placeholder="" value="{{ old('full_name') }}" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        <label for="label_name" class="control-label black">Adres:<span style="color: red">*</span></label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" name="address" required class="form-control form-lg" placeholder="" value="{{ old('address') }}" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        <label for="label_name" class="control-label black">Postcode / Plaats:<span style="color: red">*</span></label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" name="postcode" required class="form-control form-lg" placeholder="" value="{{ old('postcode') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        <label for="label_name" class="control-label black">Telefoonnummer:<span style="color: red">*</span></label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" name="telephone" required class="form-control form-lg" placeholder="" value="{{ old('telephone') }}" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        <label for="label_name" class="control-label black">E-mailadres:<span style="color: red">*</span></label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="email" name="email" required class="form-control form-lg" placeholder="" value="{{ old('email') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        <label for="label_name" class="control-label black">Vraag of toelichting:<span style="color: red">*</span></label>
                                    </div>
                                    <div class="col-md-10">
                                        <textarea name="content" cols="30" rows="10" required class="form-control">{{ old('content') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group form-lg">
                                        <button class="btn btn-success btn-block btn-lg text-uppercase" type="submit">Versturen</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {{-- <div class="col-md-6 box-right">
                <div class="panel panel-custom">
                    <div class="block-title"><h2>Actueel</h2></div>
                    <div class="panel-body panel-content panel-content-other">
                        <div class="content lst-news">
                            @if ($news)
                                {!! html_entity_decode($news->content) !!}
                            @else
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                            @endif            
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
        <div class="row">
            <div class="blocks">
                <div class="col-md-4 form-group form-lg">
                    <a href="#">
                        <img src="/assets_fe/img/iPad-winactie.png" class="img-responsive img-bordered" alt="">
                    </a>
                </div>
                <div class="col-md-4 form-group form-lg">
                    <a href="{{route('student.faq')}}">
                        <img src="/assets_fe/img/login-2.png" class="img-responsive" alt="FAQs">
                    </a>
                </div>
                <div class="col-md-4 form-group form-lg">
                    <a href="#">
                        <img src="/assets_fe/img/login-3.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    
@endpush
