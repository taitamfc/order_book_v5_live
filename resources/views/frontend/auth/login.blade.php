@extends('frontend.includes.student-layout')

@section('title', 'Inloggen')

@section('header')
    @include('frontend.includes.components.header')
@endsection

@section('content')
<div class="top-line">
    <div class="container">
        <div class="row">
            <div class="top-content-login">
                <div class="col-lg-5 login-top-text">
                    <h3>
                        Welkom op de boekenbestelsite van <br>
                        AERES MBO Barneveld in samenwerking <br>
                        met Totdrukwerk.
                    </h3>
                    </p>
                    <p>Via deze bestelsite kun je de benodigde lesbundels, (commerciële) boeken en eventuele keuzepakketten bestellen. Ook dit jaar hebben <br>we weer leuke schoolbenodigdheden toegevoegd aan de bestelsite. <br>Deze vind je terug onder de kantoorartikelen.
                    </p>
                    <p>Je boekenbestelling wordt bij je thuis afgeleverd. <br><b>Wel zo gemakkelijk!</b></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main inloggen-content" id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-6 box-left">
                <div class="panel panel-custom">
                    <div class="block-title"><h2>Inloggen en bestellen</h2></div>
                    <div class="panel-body">
                        @if (session('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('error') }}
                            </div>
                        @endif
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif
                        <form action="{{route('student.login.submit')}}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="label_name" class="control-label black">Studentnummer:</label>
                                <input type="text" name="username" required class="form-control form-lg" placeholder="Voer hier je studentnummer in ..." {{ old('username') }}>
                            </div>
                            <div class="form-group">
                                <label for="label_name" class="control-label black">Wachtwoord:</label>
                                <input type="password" name="password" required class="form-control form-lg" placeholder="Voer hier je wachtwoord in ...">
                            </div>
                            <div class="form-group">
                                <div class="checkbox checkbox-css" style="margin-left: 25px">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember_checkbox">
                                    <label class="form-check-label" for="remember_checkbox">
                                        Onthoud me
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-lg">
                                        <button class="btn btn-success btn-block btn-lg text-uppercase" type="submit">direct inloggen</button>
                                    </div>
                                </div>
                                <div class="col-sm-6 text-sm-right">
                                    {{-- <a class="link-gray link-lg" href="/password/reset">Wachtwoord vergeten?</a> --}}
                                    <a class="link-gray link-lg" href="/faq">Wachtwoord vergeten?</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6 box-right">
                <div class="panel panel-custom">
                    <div class="block-title"><h2>Actueel</h2></div>
                    <div class="panel-body panel-content panel-content-other">
                        <div class="content lst-news">
                            @if ($news)
                                {!! html_entity_decode($news->content) !!}
                            @else
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                            @endif            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="blocks">
                <div class="col-md-4 form-group form-lg">
                    <a href="#">
                        <img src="/assets_fe/img/iPad-winactie.png" class="img-responsive img-bordered" alt="">
                    </a>
                </div>
                <div class="col-md-4 form-group form-lg">
                    <a href="{{route('student.faq')}}">
                        <img src="/assets_fe/img/login-2.png" class="img-responsive" alt="FAQs">
                    </a>
                </div>
                <div class="col-md-4 form-group form-lg">
                    <a href="#">
                        <img src="/assets_fe/img/login-3.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    
@endpush
