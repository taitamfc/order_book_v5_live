<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class ProfileEditRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::guard('admin')->user();
        return [
            'email'                 => 'required|email|unique:users,email,'.$user->id,
            'new_password'          => '',
            'new_password_repeat'   => 'required_with:new_password|same:new_password',
        ];
    }
}
