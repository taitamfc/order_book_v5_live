<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('admin/login', 'Admin\Auth\AuthController@login')->name('admin.login');
Route::post('admin/login','Admin\Auth\AuthController@postLogin')->name('admin.login.submit');

Route::group(['middleware' => 'admin'], function () {
    Route::get('admin/logout','Admin\Auth\AuthController@logout')->name('admin.logout');

    Route::get('admin/sync', 'Admin\SyncController@index');
    Route::get('admin/sync/orders', 'Admin\SyncController@orders');
    Route::get('admin/sync/all', 'Admin\SyncController@all');
    Route::get('admin/sync/{name}', 'Admin\SyncController@one');
    Route::get('admin/cron/send-emails', 'Admin\CronController@send_emails');
});

Route::group(['groupName' => 'orders'], function () {

    Route::group(['middleware' => 'admin'], function () {
        Route::get('admin','Admin\OrdersController@index')->name('admin');

        Route::resource('admin/bestellingen','Admin\OrdersController', ['names' => getRouteResourceNames('orders') ]);
        //Route::post('studenten/update/{id}','StudentsController@update')->name('studentUpdate');
        Route::get('admin/batches/max_qty', 'Admin\BatchesController@max_qty')->name('batches.max_qty');
        Route::get('admin/batches/to_txt_file/{id}', 'Admin\BatchesController@to_txt_file')->name('batches.to_txt_file');

        Route::resource('batches','Admin\BatchesController');
        Route::get('admin/batches/destroy/{id}', 'Admin\BatchesController@destroy')->name('batches.destroy');

        Route::get('admin/batches/{id}/export', 'Admin\BatchesController@exportBatch')->name('batches.export');
        Route::get('admin/batches/{id}/{product_id}', 'Admin\BatchesController@pdf')->name('batches.pdf');

        Route::post('admin/batches/edit_qty', 'Admin\BatchesController@editQty')->name('batches.edit_qty');
        Route::post('admin/batches/set_complete', 'Admin\BatchesController@setComplete')->name('batches.set_complete');
        Route::post('admin/batches/remove_print', 'Admin\BatchesController@removePrint')->name('batches.remove_print');

        Route::post('admin/shipments/set_shipped', 'Admin\ShipmentsController@setShipped')->name('shipments.set_shipped');
        Route::post('admin/shipments/set_shipped/all', 'Admin\ShipmentsController@setAllShipped')->name('shipments.set_shipped.all');
        Route::post('admin/shipments/pdf/{id}', 'Admin\ShipmentsController@pdf')->name('shipments.pdf');
        Route::get('admin/shipments/reset/{id}', 'Admin\ShipmentsController@reset')->name('shipments.reset');
        Route::get('admin/shipments/send/email/{id}', 'Admin\ShipmentsController@sendEmail')->name('shipments.send.email');
    });

    Route::resource('admin/verzendingen','Admin\ShipmentsController', ['names' => getRouteResourceNames('shipments') ]);
});


Route::group(['middleware' => 'admin'], function () {
    Route::get('admin/producten/index','Admin\ProductsController@index')->name('producten.index');
    Route::get('admin/producten/show/{id}','Admin\ProductsController@show')->name('producten.show');
    Route::get('admin/producten_isbn','Admin\ProductsController@isbn')->name('producten.isbn');
    
    Route::resource('admin/statistieken','Admin\StatsController', ['names' => getRouteResourceNames('stats') ]);

    Route::get('admin/mail', 'Admin\MailController@edit')->name('mail');
    Route::post('admin/mail/update', 'Admin\MailController@update')->name('mailUpdate');

    Route::get('admin/studenten','Admin\StudentsController@index')->name('studenten.index');
    Route::get('admin/studenten/create','Admin\StudentsController@create')->name('studenten.create');
    Route::post('admin/studenten/store','Admin\StudentsController@store')->name('studenten.store');
    Route::get('admin/studenten/show/{id}','Admin\StudentsController@show')->name('studenten.show');
    Route::post('admin/studenten/update/{id}','Admin\StudentsController@update')->name('studenten.update');

    Route::get('admin/search','Admin\GlobalSearchController@search')->name('search');
    Route::get('admin/profiel','Admin\ProfileController@edit')->name('profileEdit');
    Route::post('admin/profiel/update','Admin\ProfileController@update')->name('profileUpdate');
});


Route::group(['middleware' => 'admin'], function () {
    Route::get('admin/excel/import','Admin\ExcelController@import')->name('excel.import');
    Route::post('admin/excel/import-submit','Admin\ExcelController@submitImport')->name('excel.import.submit');
});


////////////////////////////////////////////// STUDENT ORDER BOOK //////////////////////////////////////////////////
Route::group(['middleware' => 'student'], function () {
    Route::get('/', 'HomeController@index')->name('student.home');
    Route::get('/logout','AuthController@logout')->name('student.logout');

    Route::post('/add-cart', 'HomeController@addCart')->name('student.order.addCart');
    Route::post('/add-ship-info', 'HomeController@addShipInfo')->name('student.order.addShipInfo');
    Route::get('/order-shipping', 'HomeController@orderShipping')->name('student.order.shipping');
    Route::get('/order-details', 'HomeController@orderDetails')->name('student.order.details');
    
    Route::get('/order-payment', 'HomeController@orderPayment')->name('student.order.payment');

    Route::get('/order-histories', 'StudentController@orderHistories')->name('student.order.histories');
    Route::get('/statistics', 'StudentController@statistics')->name('student.statistics');
    

    

});

Route::get('/login', 'AuthController@login')->name('student.login');
Route::post('/login', 'AuthController@postLogin')->name('student.login.submit');

