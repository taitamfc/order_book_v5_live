<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MolliePayment;
use App\Order;
use App\OrderItem;
use Carbon\Carbon;

class MollieWebhookController extends Controller {
    public function handle(Request $request) {
        try{
            if (!$request->has('id')) {
                return response('ERROR', 404)->header('Content-Type', 'text/plain');
            }
            $current_date_time = Carbon::now();
            $payment = Mollie::api()->payments()->get($request->id);
    
            if ($payment->isPaid()) {
                // update status order
                $order_id = $payment->metadata->order_id;
    
                $order_info = Order::find('order_id',$order_id);
                if(!$order_info){
                    return response('ERROR', 404)->header('Content-Type', 'text/plain');
                }
                $order_info->status = "completed";
                $order_info->updated_at = $current_date_time;
                $order_info->save();
                $mollie_payment = MolliePayment::where('payment_id',$id)->first();
                if(!$mollie_payment){
                    return response('ERROR', 404)->header('Content-Type', 'text/plain');
                }
                $mollie_payment->status = $payment->status;
                $mollie_payment->paid_at = $payment->paidAt;
                $mollie_payment->updated_at = $current_date_time;
                $mollie_payment->save();
                return response('OK', 200)->header('Content-Type', 'text/plain');
            }
        }
        catch(Exception $e){
            return response('ERROR', 500)->header('Content-Type', 'text/plain');
        }
    }
}
