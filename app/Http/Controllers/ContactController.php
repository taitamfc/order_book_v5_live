<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\Contact;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function index(){
        return view('frontend.contact.index');
    }  

    public function post(Request $request){
        $request->validate([
            'student_number' => 'required',
            'class' => 'required',
            'address' => 'required',
            'postcode' => 'required',
            'full_name' => 'required',
            'email' => 'required|email',
            'telephone' => 'required',
            'content' => 'required'
        ],[]);
        $params = $request->all();
        // if(Mail::to(env('ADMIN_EMAIL'))->send(new Contact($params))){
        //     return back()->with(['error' => '']);
        // }
        // else{
        //     return back()->with('status','Uw verzoek is verzonden.');
        // }
    } 
}
