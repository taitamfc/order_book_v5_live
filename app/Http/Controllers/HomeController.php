<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Helpers\MolliePaymentHelpers;
use Mollie\Laravel\Facades\Mollie;

use Auth;

use App\Student;
use App\Category;
use App\Cart;
use App\CartItems;
use App\Order;
use App\Mail\Invoice;
use App\Cms;
use Illuminate\Support\Facades\Mail;
use PDF;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\Interfaces\CmsInterface;

class HomeController extends Controller
{
    protected $_model_category = null;
    protected $_model_cart = null;
    protected $_model_cart_items = null;
    protected $_cms_repository = null;
    public function __construct(Category $category, Cart $cart, CartItems $cart_items,CmsInterface $cms_repository)
    {  
        $this->_model_category = $category;
        $this->_model_cart = $cart;
        $this->_model_cart_items = $cart_items;
        $this->_cms_repository = $cms_repository;
    }

    public function checkCart(){
        $model_cart = new Cart();
        $cart_of_student = $model_cart->getCartOfStudent();
        if($cart_of_student){
            return true;
        }
        return false;
    }


    public function index(){
        $checkedForStep = array(
            'step-1' => false,
            'step-2' => false,
            'step-3' => false,
            'step-4' => false
        );
        return view('frontend.home.index',array("step_order" => $checkedForStep));
    }  

    public function addCart(Request $request){
        $params = $request->all();
        
        $model_cart = $this->_model_cart;
        $add_cart = $model_cart->addCart($params);
        if(!$add_cart){
            return back()->with('error', ['error!']);
        }
        return \redirect()->route('student.order.shipping');
    }  
    
    public function orderShipping(){
        if(!$this->checkCart()){
            return \redirect()->route('student.home');
        }
        $checkedForStep = array(
            'step-1' => true,
            'step-2' => false,
            'step-3' => false,
            'step-4' => false
        );
        return view('frontend.home.shipping',array("step_order" => $checkedForStep));
    }

    public function addShipInfo(Request $request){
        $params = $request->all();
        $email = $params["email"];
		$re_email = $params["re-email"];
		if($email != $re_email){
			 return back()->with('error', ['Het e-mailadres komt niet overeen met het bevestig e-mailadres!']);
		}
        $model_cart = $this->_model_cart;
        $add_ship_info = $model_cart->addShipInfo($params);
        if(!$add_ship_info){
            return back()->with('error', ['error!']);
        }
        return \redirect()->route('student.order.details');
    }

    public function orderDetails(){
        if(!$this->checkCart()){
            return \redirect()->route('student.home');
        }
        $model_student = new Student();
        $student_info = $model_student->getStudentInfo();

        $model_cart = new Cart();
        $cart_of_student = $model_cart->getCartOfStudent();
        if($cart_of_student && $cart_of_student->street){
            $student_info->street = $cart_of_student->street;
            $student_info->city = $cart_of_student->city;
            $student_info->email = $cart_of_student->email;
            $student_info->telephone = $cart_of_student->telephone;
            $student_info->postcode = $cart_of_student->postcode;
            $student_info->bank_account = $cart_of_student->bank_account;
        }
        $checkedForStep = array(
            'step-1' => true,
            'step-2' => true,
            'step-3' => false,
            'step-4' => false
        );
        return view('frontend.home.order-details',['student_info' => $student_info , "step_order" => $checkedForStep]);
    }   

    public function orderPayment(){
        if(!$this->checkCart()){
            return \redirect()->route('student.home');
        }
        $methods = Mollie::api()->methods()->allActive();
        $checkedForStep = array(
            'step-1' => true,
            'step-2' => true,
            'step-3' => true,
            'step-4' => false
        );
        return view('frontend.home.payment',['methods' => $methods, "step_order" => $checkedForStep]);
    } 

    public function orderPaymentSubmit(Request $request){
        $params = $request->all();
        $helper_mollie_payment = new MolliePaymentHelpers();
        $payment = $helper_mollie_payment->preparePayment($params);
        if($payment){
            return redirect($payment->getCheckoutUrl(), 303);
        }
        return back()->with('error', ['error!']);
    }

    public function orderSuccess($id){
        $checkedForStep = array(
            'step-1' => true,
            'step-2' => true,
            'step-3' => true,
            'step-4' => true
        );

        if($id){
            $order_id = $id;
            $model_order = new Order();
            $order_info = $model_order->getOrderEmail($order_id);

            if(!$order_info->invoice_pdf){
                $current_timestamp = Carbon::now()->timestamp;
                $file_name = $order_id.$current_timestamp.".pdf";
                $order_info->invoice_pdf = $file_name;
                //Create pdf in here
                $array_patch = $this->createdf($order_info);

                $order_info->invoice_pdf = $array_patch;
            }

            if(!$order_info->is_send_email){
                $this->sendConfirmEmail($order_info);
                Order::where('order_id', $order_info->order_id)
                ->update([
                    'is_send_email' => 1
                ]);
            }
            return view('frontend.home.order-success',['order' => $order_info,"step_order" => $checkedForStep]);
        }
        return view('frontend.home.order-success')->with('error', ['Kan bestelling niet vinden!']);
    }

    public function sendConfirmEmail($data){
        try{
            $data->is_admin = false;
            //Mail::to($data->email)->send(new Invoice($data));
            $data->is_admin = true;
            //Mail::to(env('ADMIN_EMAIL'))->send(new Invoice($data));
            return true;
        }catch(Exception $e){
            return false;
        }
    }

    public function createdf($data){
        set_time_limit(300);

        $pdf = PDF::loadView('frontend.dompdf.invoice', ['data' => $data]);

        $file_name = $data->invoice_pdf;
        // get path to save file pdf
        $path = Storage::disk('public')->path("pdf");
        
        // save file pfd
        $pdf->Output($path.'/'.$file_name, 'F');
        $pdf->save($path.'/'.$file_name);
        $full_path = $path.'/'.$file_name;
        Order::where('order_id', $data->order_id)
        ->update([
            'invoice_pdf' => '/storage/pdf/'.$file_name
        ]);
        return '/storage/pdf/'.$file_name;
    }

    public function cms($slug){
        if(!isset($slug)){
            abort(404);
        }

        $cms_page = $this->_cms_repository->getBySlug($slug);

        return View('frontend.cms.index',['cms_page' => $cms_page]);

    }
}
