<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Faq;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    public function index(){
        $faq = Faq::where('is_enabled','=',1)->first();
        return view('frontend.home.faq',['faq' => $faq]);
    }  
}
