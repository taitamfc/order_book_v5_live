<?php

namespace App\Http\Controllers\Admin;

use App\Group;
use App\Libraries\MagentoApi;
use App\MagentoQuantityUpdate;
use App\Product;
use Illuminate\Http\Request;

use DB;
use App\Order;
use App\OrderItem;
use Illuminate\Support\Facades\Artisan;
/* use mPDF; */
use Mail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ShipmentsController extends Controller
{
    public function index(Request $request)
    {

        $orders = Order::with('items', 'items.product', 'student.group', 'items_count', 'items_shipped_count')
                        ->where('batch_id','>','0')
                        ->whereHas('batchProducts', function ($query) {
                            $query->where('qty_available','>',0);
                        })
                        /*->whereHas('items.product', function($query){
                            $query->whereRaw('(select sum(qty_ordered) from  order_items oi where oi.batch_id = order_items.batch_id and oi.product_id = order_items.product_id) =
                                   (select qty_available from batch_products bp where bp.batch_id = order_items.batch_id and bp.product_id = order_items.product_id)');
                        })*/;

        if($request->get('shipped_all') == 1){
            //$orders->has('items_shipped', '<=', 0);
            $orders->has('items_nonshipped', '>', 0);
        }
        elseif($request->get('shipped_all') == 2){
            $orders->has('items_nonshipped', '<=', 0);
        }

        if ($request->id) {
            $orders->where('increment_id',$request->id);
        } 
        if ($request->batch_id) {
            $orders->where('batch_id',$request->batch_id);
        } 
        if ($request->username) {
            $orders->whereHas('student',function ($query) use ($request) {
                $query->where('username', $request->username);
            });
        }
        if ($request->name) {
            $orders->where(function($query) use ($request) {
                $query->where('firstname','like','%'.$request->name.'%');
                $query->orWhere('lastname','like','%'.$request->name.'%');
            });
        }

        if($request->is_complete){
            if($request->is_complete == 2){
                /*$orders->whereHas('batchProducts', function ($query) {
                    //$query->where('is_qty_complete', 1);
                    //$query->havingRaw('sum(qty_available) = count(batch_product_id)');

                });*/

                $orders->whereHas('batchProducts', function ($query){
                    $query->whereRaw(DB::raw('product_id IN (SELECT product_id FROM order_items WHERE order_items.order_id=orders.order_id)'))->where(function ($q){
                        $q->whereNull('is_complete')->orWhere('is_complete', 1);
                    });
                }, '>', 0);
            }
            elseif($request->is_complete == 1){
                /*$orders->whereHas('batchProducts', function ($query) {
                    $query->where('is_qty_complete', 0);
                });*/
                //$orders->has('batchNotComplete', '<=', 0);

                $orders->whereHas('batchProducts', function ($query){
                    $query->whereRaw(DB::raw('product_id IN (SELECT product_id FROM order_items WHERE order_items.order_id=orders.order_id)'))->where(function ($q){
                        $q->where('is_complete', 1);
                    });
                }, '<=', 0);
            }
        }

        if($request->product){
            $orders->whereHas('items', function($query) use($request){
               $query->where('product_id', $request->product);
            });
        }

        if($request->group){
            $orders->whereHas('student', function($query) use($request){
                $query->where('group_id', $request->group);
            });
        }

        $sql = $orders->toSql();

        if($request->export_postnl){
            $orders = $orders->get();
        }else {
            $orders = $orders->paginate(50);
        }

        $export = false;
        if($request->export){
            $mpdf = new \Mpdf\Mpdf();
            //$mpdf->SetFont('arial');

            foreach ($orders as $order){
                $items = $order->items()->with('product')->whereRaw('(select sum(qty_ordered) from  order_items oi where oi.batch_id = order_items.batch_id and oi.product_id = order_items.product_id) =
                                   (select qty_available from batch_products bp where bp.batch_id = order_items.batch_id and bp.product_id = order_items.product_id)');

                $items = $items->get();

                $qty_shipped =  $order->items()->where('qty_shipped','>', 0)->count();

                if($items->count() && $qty_shipped < $order->items->count()){
                    $export = true;
                    $mpdf->AddPage();
                    $content = view('admin.shipments.export', ['order' => $order, 'items' => $items])->render();
                    $mpdf->WriteHTML($content);
                }
            }

            if($export){
                $mpdf->Output('Verzendingen.pdf', 'D');
            }
        }

        if($request->export_postnl){
            $this->exportPostNl($orders);
        }

        if($request->username && $orders->count() == 1){
            return redirect()->route('shipments.show', ['id' => $orders->first()->order_id]);
        }

        $products = Product::all();
        $groups = Group::all();

        return view('admin.shipments.index', ['orders' => $orders, 'products' => $products, 'groups' => $groups]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $order = Order::findOrFail($id);

        $order->items = $order->items->groupBy('product.category.slug');

        //Get ready to be sent items
        $send_items = OrderItem::with('product')->where('order_id', $id);
        $send_items->whereRaw('(select sum(qty_ordered) from  order_items oi where oi.batch_id = order_items.batch_id and oi.product_id = order_items.product_id) =
                               (select qty_available from batch_products bp where bp.batch_id = order_items.batch_id and bp.product_id = order_items.product_id)');
        $send_items = $send_items->get();

        $send_items_arr = [];
        if(count($send_items) > 0){
            foreach($send_items as $item){
                $send_items_arr[] = $item->product_id;
            }
        }

        $groups = Group::all();

        return view('admin.shipments.show', [
            'order' => $order,
            'send_items' => $send_items_arr,
            'groups' => $groups
        ]);
    }

    public function pdf(Request $request, $id)
    {
        $order = Order::findOrFail($id);

        $magento = new MagentoApi();

        $order->items = $order->items->groupBy('product.category.slug');

        foreach ($order->items as $key=>$items){
            foreach ($items as $item){
                if(in_array($item->item_id, $request->get('send-items', []))){
                    $item->qty_shipped = $item->qty_ordered;
                    $item->save();

                    $product = Product::where('id', $item->product_id)->first();
                    $product->qty = $product->qty - $item->qty_ordered;
                    $product->reserve_from_stock = $product->reserve_from_stock - $item->qty_ordered;
                    $product->save();

                    MagentoQuantityUpdate::create(['product_id' => $item->product_id, 'quantity' => $product->qty]);

                    $magento->updateQuantityProduct($item->product_id, $product->qty);
                }
            }
        }

        $items_count = $order->items()->count();
        $items_shipped = $order->items()->where('qty_shipped', '>', 0)->count();

        //create shipment
        if($items_count === $items_shipped && is_null($order->shipment_increment_id)){

            $res = $magento->createOrderShipment($order->increment_id);
            if(intval($res) > 0){
                Order::where('order_id', $id)->update(['shipment_increment_id' => $res]);
            }
            Order::where('order_id', $id)->update(['shipment_increment_id' => $res]);
        }

        $resource = view('admin.shipments.pdf', ['order' => $order, 'send_items' => $request->get('send-items', [])])->render();

        $pdf = new \Mpdf\Mpdf();
        $pdf->WriteHTML($resource);
        $filename = 'Bestelling_'.$id.'_'.time();
        $pdf->output($filename.".pdf",'D');
    }

    public function setShipped(Request $request)
    {
        $item = OrderItem::where('item_id',$request->item_id)->first();

        if ($request->val) {
            $item->qty_shipped = $item->qty_ordered;
        } else {
            $item->qty_shipped = 0;
        }
        $item->save();

    }    

    public function setAllShipped(Request $request)
    {

        $items = OrderItem::where('order_id',$request->order_id)->get();

        foreach ($items as $item) {
            if ($request->val) {
                $item->qty_shipped = $item->qty_ordered;
            } else {
                $item->qty_shipped = 0;
            }
            $item->save();
        }

    }

    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $order->track_number = $request->track_number;
        $order->track_number2 = $request->track_number2;
        $order->save();
        return redirect()->route('shipments.show', $id);
    } 

    public function sendEmail($id)
    {
        $order = Order::find($id);

        $order->items = $order->items->groupBy('product.category.name');

        Mail::send('shipments.email', ['order' => $order], function ($m) use ($order) {
            $m->to($order->email)->subject('Bestelling verzonden ['.$order->increment_id.']');
        });
        return redirect()->route('shipments.show', $id);
    }

    public function reset(Request $request, $id){
        $order_items = OrderItem::where('order_id', $id)->where('qty_shipped', '>', 0)->get();

        $magento = new MagentoApi();

        foreach ($order_items as $item){
            $item->qty_shipped = 0;
            $item->save();

            $product = Product::where('id', $item->product_id)->first();
            $product->qty = $product->qty + $item->qty_ordered;
            $product->reserve_from_stock = $product->reserve_from_stock + $item->qty_ordered;
            $product->save();

            MagentoQuantityUpdate::create(['product_id' => $item->product_id, 'quantity' => $product->qty]);

            $magento->updateQuantityProduct($item->product_id, $product->qty);
        }

        return redirect()->route('shipments.show', $id);
    }

    protected function exportPostNl($orders){
        if(! count($orders))
            return false;

        $head = [
            'YourReference',
            'CompanyName',
            'Surname',
            'FirstName',
            'CountryCode',
            'Street',
            'HouseNo',
            'HouseNoSuffix',
            'Postcode',
            'City',
            'Email',
            'MobileNumber',
            'ProductCode',
            'CODAmount',
            'CODReference',
            'InsuredValue',
        ];

        $data = [];

        foreach ($orders as $order){
            //get street, number, sufix number
            preg_match_all('~(([a-zA-z,\s]+)\s+)([0-9]+)([a-zA-z0-9\s]+)?~', $order->street, $match);
            $data[] = [
                'YourReference' => $order->student ? $order->student->username : null,
                'CompanyName'   => null,
                'Surname' => $order->lastname,
                'FirstName' => $order->firstname,
                'CountryCode'   => 'NL',
                'Street' => (isset($match[2][0])) ? trim($match[2][0]) : null,
                'HouseNo' => (isset($match[3][0])) ? trim($match[3][0]) : null,
                'HouseNoSuffix' => (isset($match[4][0])) ? trim($match[4][0]) : null,
                'Postcode' => $order->postcode,
                'City'  => $order->city,
                'Email' => $order->email,
                'MobileNumber'  => $order->telephone,
                'ProductCode'   => '3085',
                'CODAmount'  => null,
                'CODReference'  => null,
                'InsuredValue'  => null,
            ];

        }

        header( 'Content-Type: text/csv' );
        header( 'Content-Disposition: attachment;filename=Groenhorst_Verzendingen_'.date('Y-m-d-His').'.csv');
        $out = fopen('php://output', 'w');
        //set header
        fputcsv($out, $head, ';');
        foreach ($data as $val)
            fputcsv($out, $val, ';');
        fclose($out);
        die();
    }
}
