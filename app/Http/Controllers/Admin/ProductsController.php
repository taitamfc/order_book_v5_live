<?php

namespace App\Http\Controllers\Admin;

use App\Batch;
use App\Category;
use App\Group;
use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::orderBy('id','ASC');

        $filter[] = '';

        if(Input::has('id') && Input::get('id') != '')
        {
            $products->where('id','=',trim(Input::get('id')));
            $filter['id'] = Input::get('id');
        }

        if(Input::has('name') && Input::get('name') != '')
        {
            $products->where('name','LIKE','%'.trim(Input::get('name')).'%');
            $filter['name'] = trim(Input::get('name'));
        }

        if(Input::has('category') && Input::get('category') != '')
        {
            $products->where('category_id','=',trim(Input::get('category')));
            $filter['category_id'] = trim(Input::get('category_id'));
        }

        if(Input::has('below_minimum') && Input::get('below_minimum') != '')
        {
            $products->where('qty','<',Input::get('below_minimum'));
            $filter['below_minimum'] = trim(Input::get('below_minimum'));
        }

        $categories = Category::all();
        /* $products = $products->paginate(100); */

        $products = $products->get();

        return view('admin.products.index',['products' => $products,'categories' => $categories,'filter' => $filter]);
    }

    public function getListProducts(Request $request){


        return \response()->json([],200);
    }






    public function isbn(Request $request){
        $products = Product::orderBy('id','ASC');
        if(Input::has('isbn') && Input::get('isbn') != '')
        {
            $products->where('isbn', trim(Input::get('isbn')));
            $filter['isbn'] = Input::get('isbn');
        }

        if(Input::has('name') && Input::get('name') != '')
        {
            $products->where('name','LIKE','%'.trim(Input::get('name')).'%');
            $filter['name'] = trim(Input::get('name'));
        }

        if(Input::has('category') && Input::get('category') != '')
        {
            $products->where('category_id','=',trim(Input::get('category')));
            $filter['category_id'] = trim(Input::get('category_id'));
        }

        if($request->below_minimum)
        {
            $products->where('qty','<',$request->below_minimum);
            $filter['below_minimum'] = trim($request->below_minimum);
        }

        $products->select(DB::raw('id, name, category_id, isbn, provider, SUM(min_qty) as min_qty, SUM(qty) AS qty, SUM(reserve_from_stock) AS reserve_from_stock, GROUP_CONCAT(id) AS products '));
        $products->where('isbn', '<>', '');
        $products->groupBy('isbn');

        $filter[] = '';

        $categories = Category::all();
        $batches = Batch::all();
        $products = $products->paginate(100);
        //dd($products);
        return view('admin.products.isbn',['products' => $products,'categories' => $categories,'batches' => $batches, 'filter' => $filter]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model_product = new Product();
        $product = $model_product->getProductById($id);
        $categories = Category::get();
        $groups = Group::all();
        return view('admin.products.show',['product' => $product,'categories' => $categories,'groups' => $groups]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $model_product = new Product();
        $params = $request->all();
        $product = $model_product->updateProduct($params);
        if($product){
            return \redirect()->back()->with('status','Update succes!');
        }
        else{
            return \redirect()->back()->with('error','Update mislukt!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
