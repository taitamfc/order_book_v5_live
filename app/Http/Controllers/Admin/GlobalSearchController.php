<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\Product;
use App\Student;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

class GlobalSearchController extends Controller
{
    public function search()
    {

        $products = Product::where('name','LIKE','%'.trim(Input::get('value')).'%')->get();
        $students = Student::where('firstname','LIKE','%'.trim(Input::get('value')).'%')->orWhere('lastname','LIKE','%'.trim(Input::get('value')).'%')->get();
        $orders = Order::where('firstname','LIKE','%'.trim(Input::get('value')).'%')
                ->orWhere('lastname','LIKE','%'.trim(Input::get('value')).'%')->orWhere('increment_id','LIKE','%'.trim(Input::get('value')).'%')->get();



        $result['products'] = $products;
        $result['students'] = $students;
        $result['orders'] = $orders;
        return response()->json($result);

    }
}
