<?php

namespace App\Http\Controllers\Admin;

use Maatwebsite\Excel\Facades\Excel;
use App\Excel\ImportProducts;
use App\Excel\ImportCommerceProducts;
use App\Excel\ImportStudents;
use App\Group;
use App\Category;
use Log;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ExcelController extends Controller
{
    //
    public function import(){
        $customer_groups = Group::all();
        $categories = Category::where('parent_id',2)->get();
        return view('admin.excel.import',['groups' => $customer_groups,'categories' => $categories]);
    }


    public function submitImport(Request $request){
        $request->file('import')->move(public_path('upload'), $request->file('import')->getClientOriginalName());

        $path = public_path('upload') . '/' . $request->file('import')->getClientOriginalName();
        $group = $request->get("group");
        $category = $request->get("category");
        /* $rows = Excel::load($path, 'UTF-8')->get(); */
        $rows = Excel::selectSheetsByIndex(0)->load($path, 'UTF-8')->get();
        $arr = array();
        foreach($rows as $index => $row)
        {
            if ($index == 0) {
                if($request->get("import-type") == "1" || $request->get("import-type") == "3"){
                    $result_check = $this->checkFileImportProducts($row);
                    if(count($result_check['rows_error']) > 0){
                        return back()->with('result', $result_check);
                    }
                }
                if($request->get("import-type") == "2"){
                    $result_check = $this->checkFileImportStudents($row);
                    if(count($result_check['rows_error']) > 0){
                        return back()->with('result', $result_check);
                    }
                }
                continue;
            }
            if($group){
                if($group == "0"){
                    if($category){
                        if($category == "0"){
                            $arr[] = $row;
                        }
                        else if($row->category_id == $category){
                            $arr[] = $row;
                        }
                    }
                    else{
                        $arr[] = $row;
                    }

                    /* $arr[] = $row; */
                }
                else if($row->customer_groups_value == $group){
                    if($category){
                        if($category == "0"){
                            $arr[] = $row;
                        }
                        else if($row->category_id == $category){
                            $arr[] = $row;
                        }
                    }
                    else{
                        $arr[] = $row;
                    }
                    /* $arr[] = $row; */
                }
            }
            else{
                if($category){
                    if($category == "0"){
                        $arr[] = $row;
                    }
                    else if($row->category_id == $category){
                        $arr[] = $row;
                    }
                }
                else{
                    $arr[] = $row;
                }
                /* $arr[] = $row; */
            }

            /* if($category){
                if($category == "0"){
                    $arr[] = $row;
                }
                else if($row->category_id == $category){
                    $arr[] = $row;
                }
            }
            else{
                $arr[] = $row;
            } */
        }
        $result = null;
        
        // import products
        if($request->get("import-type") == "1"){
            $model_import = new ImportProducts();
            $result = $model_import->collection($arr,$group);
        }

        // import comrece products | category_id = 4 | product_type_id = 92
        if($request->get("import-type") == "3"){
            $model_import = new ImportCommerceProducts();
            $result = $model_import->collection($arr,$group);
        }

        // import student
        if($request->get("import-type") == "2"){
            $model_import = new ImportStudents();
            $result = $model_import->collection($arr,$group);
        }
        return back()->with('result', $result);
    }

    public function checkFileImportProducts($row){
        $error = false;
        $invalid_columns = array();
        foreach($row as $index => $key){
            if($index == "klas" 
                || $index == "titel"
                || $index == "prijs"
                || $index == "short_description"
                || $index == "sku"
                || $index == "thumbnail"
                || $index == "weight"
                || $index == "moduulcode"
                || $index == "moduuldruk"
                || $index == "moduulnaam"
                || $index == "druk"
                || $index == "isbn"
                || $index == "schrijver"
                || $index == "uitgeverij"
                || $index == "vakgroep"
                || $index == "team"
                || $index == "product_type_value"
                || $index == "pagina"
                || $index == "category_id"
                || $index == "qty"
                || $index == "row_number"
                || $index == "auteur"
                || $index == "not_delivery"
                || $index == "order_at"
                || $index == "order_email_address"
                
                || $index == "custom_column_1"
                || $index == "custom_column_2"
                || $index == "custom_column_3"
                || $index == "optional"){
                continue;
            } else{
                $error = true;
                $invalid_columns[] = $index;
            }
        }
        if($error){
            $result_array = [
                'total_rows' => 0,
                'success' => 0,
                'duplicate' => 0,
                'rows_duplicate' => [],
                'error' => 'ongeldige kolom',
                'rows_error' => $invalid_columns
            ];
        }
        else{
            $result_array = [
                'total_rows' => 0,
                'success' => 0,
                'duplicate' => 0,
                'rows_duplicate' => [],
                'error' => 0,
                'rows_error' => []
            ];
        }
        return $result_array;
    }

    public function checkFileImportStudents($row){
        $error = false;
        $invalid_columns = array();
        foreach($row as $index => $key){
            if($index == "id"
                || $index == "achternaam"
                || $index == "klas"
                || $index == "tussen"
                || $index == "roepnaam"
                || $index == "studentnr"
                || $index == "telefoon"
                || $index == "woonplaats"
                || $index == "postcode"
                || $index == "adres"
                || $index == "nulzes"
                || $index == "wachtwoord"
                || $index == "soort"
                || $index == "email"
                || $index == "row_number"){
                continue;
            } else{
                $invalid_columns[] = $index;
                $error = true;
            }
        }
        if($error){
            $result_array = [
                'total_rows' => 0,
                'success' => 0,
                'duplicate' => 0,
                'rows_duplicate' => [],
                'error' => 'ongeldige kolom',
                'rows_error' => $invalid_columns
            ];
        }
        else{
            $result_array = [
                'total_rows' => 0,
                'success' => 0,
                'duplicate' => 0,
                'rows_duplicate' => [],
                'error' => 0,
                'rows_error' => []
            ];
        }
        return $result_array;
    }
}
