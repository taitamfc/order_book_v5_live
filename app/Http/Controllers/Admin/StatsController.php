<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\Product;
use App\Student;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Libraries\MagentoApi;
use App\Http\Controllers\Controller;

class StatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

//        ------------------------------Total amount of orders made-------------------------------------

        if(Input::has('date_from') && !Input::has('date_until'))
        {
            $ordersAmount = Order::where('created_at','>=',Carbon::parse(Carbon::createFromFormat('d/m/Y',Input::get('date_from'))->format('Y-m-d')));

        }elseif(Input::has('date_until') && !Input::has('date_from'))
        {
            $ordersAmount = Order::where('created_at','<=',Carbon::parse(Carbon::createFromFormat('d/m/Y',Input::get('date_until'))->format('Y-m-d')));

        }elseif(Input::has('date_from') && Input::has('date_until'))
        {
            $ordersAmount = Order::where('created_at','>=',Carbon::parse(Carbon::createFromFormat('d/m/Y',Input::get('date_from'))->format('Y-m-d')))
                                ->where('created_at','<=',Carbon::parse(Carbon::createFromFormat('d/m/Y',Input::get('date_until'))->format('Y-m-d'))->addDay(1));
        }else
        {
            $ordersAmount = Order::whereYear('created_at','=',date('Y'));
        }

        $ordersAmount = $ordersAmount->count();

//        ---------------------Amount of students that have an account but didn't make an order yet.--------------------

        $studentsWithoutOrder = Student::where('is_active','1');

        $studentsWithoutOrder->whereNotExists(function($query){
            $query->select(DB::raw(1))->from('orders')->whereRaw('students.customer_id = orders.customer_id');
        });
        $studentsWithoutOrder = $studentsWithoutOrder->count();

//        ---------------------------------Average amount (€) per order.--------------------------------------

        $averagePerOrder = Order::select(DB::raw('avg(grand_total) as orders_avg'));


        if(Input::has('date_from') && !Input::has('date_until'))
        {
            $averagePerOrder->where('created_at','>=',Carbon::parse(Carbon::createFromFormat('d/m/Y',Input::get('date_from'))->format('Y-m-d')));

        }elseif(Input::has('date_until') && !Input::has('date_from'))
        {
            $averagePerOrder->where('created_at','<=',Carbon::parse(Carbon::createFromFormat('d/m/Y',Input::get('date_until'))->format('Y-m-d')));

        }elseif(Input::has('date_from') && Input::has('date_until'))
        {
            $averagePerOrder->where('created_at','>=',Carbon::parse(Carbon::createFromFormat('d/m/Y',Input::get('date_from'))->format('Y-m-d')))
                            ->where('created_at','<=',Carbon::parse(Carbon::createFromFormat('d/m/Y',Input::get('date_until'))->format('Y-m-d'))->addDay(1));
        }else
        {
            $averagePerOrder->whereYear('created_at','=',date('Y'));
        }

        $averagePerOrder = $averagePerOrder->get();

//        ---------------------------------------Total amount of orders made.-------------------------------------------

        $ordersSum = DB::table('order_items');

        if(Input::has('date_from') && !Input::has('date_until'))
        {
            $ordersSum->where('created_at','>=',Carbon::parse(Carbon::createFromFormat('d/m/Y',Input::get('date_from'))->format('Y-m-d')));

        }elseif(Input::has('date_until') && !Input::has('date_from'))
        {
            $ordersSum->where('created_at','<=',Carbon::parse(Carbon::createFromFormat('d/m/Y',Input::get('date_until'))->format('Y-m-d')));

        }elseif(Input::has('date_from') && Input::has('date_until'))
        {
            $ordersSum->where('created_at','>=',Carbon::parse(Carbon::createFromFormat('d/m/Y',Input::get('date_from'))->format('Y-m-d')))
                ->where('created_at','<=',Carbon::parse(Carbon::createFromFormat('d/m/Y',Input::get('date_until'))->format('Y-m-d'))->addDay(1));
        }else
        {
            $ordersSum->whereYear('created_at','=',date('Y'));
        }

        $ordersSum = $ordersSum->sum('price');

//        ----------------------------------Products for select field-----------------------------

        $products = Product::all();

//        ------------------------Providers for select field-------------------------------------

        $providers = Product::groupBy('provider')->distinct()->get();//dd($providers);


//        ----------------------------Products statistic----------------------------------------

        $productsForTable =  [];

        if(Input::has('product') || Input::has('provider'))
        {
            if(Input::has('product')){
                $productsForTable = Product::where('products.id','=',Input::get('product'));
            }
            if(Input::has('provider')){
                $productsForTable = Product::where('provider','=',trim(Input::get('provider')));
            }
            //$productsForTable->join('order_items','products.product_id','=','order_items.product_id');
            $data_cond = '';
            if(Input::has('date_from')) {
                $data_cond .= ' AND order_items.created_at >= "'. Carbon::parse(\Carbon\Carbon::createFromFormat('d/m/Y',Input::get('date_from')))->format('Y-m-d') .'"';
            }
            if(Input::has('date_until')) {
                $data_cond .= ' AND order_items.created_at <= "'. Carbon::parse(\Carbon\Carbon::createFromFormat('d/m/Y',Input::get('date_until')))->format('Y-m-d') .'"';
            }

            $productsForTable->select('products.*',
                DB::raw("(SELECT sum(qty_ordered) FROM order_items WHERE order_items.product_id = products.id {$data_cond}) AS total_items,
                         (SELECT sum(qty_ordered * price) FROM order_items WHERE order_items.product_id = products.id {$data_cond}) AS total_sum"));
            $productsForTable = $productsForTable->get(); //dd($productsForTable);
        }


//        Chart values
//        ---------------------- Chart--------------------------------

        $ordersSent = Order::join('order_items','orders.order_id','=','order_items.order_id');
        $ordersSent->where('order_items.qty_shipped','!=','0');

        $ordersNotSent = Order::join('order_items','orders.order_id','=','order_items.order_id');
        $ordersNotSent->where('order_items.qty_shipped','==','0');
        $ordersNotSent->where('order_items.qty_shipped','==','null');



        $ordersNotInBatch = Order::join('order_items','orders.order_id','=','order_items.order_id');

        if(Input::has('date_from') || Input::has('date_until')) {
            if (Input::has('date_from')) {
                $ordersNotSent->where('order_items.created_at', '>=', Carbon::parse(Carbon::createFromFormat('d/m/Y', Input::get('date_from'))->format('Y-m-d')));
                $ordersSent->where('order_items.created_at', '>=', Carbon::parse(Carbon::createFromFormat('d/m/Y', Input::get('date_from'))->format('Y-m-d')));
                $ordersNotInBatch->where('order_items.created_at', '>=', Carbon::parse(Carbon::createFromFormat('d/m/Y', Input::get('date_from'))->format('Y-m-d')));

            }

            if (Input::has('date_until')) {
                $ordersNotSent->where('order_items.created_at', '<=', Carbon::parse(Carbon::createFromFormat('d/m/Y', Input::get('date_until'))->format('Y-m-d')));
                $ordersSent->where('order_items.created_at', '<=', Carbon::parse(Carbon::createFromFormat('d/m/Y', Input::get('date_until'))->format('Y-m-d')));
                $ordersNotInBatch->where('order_items.created_at', '<=', Carbon::parse(Carbon::createFromFormat('d/m/Y', Input::get('date_until'))->format('Y-m-d')));

            }
        }else{
            $ordersNotSent->whereYear('order_items.created_at','=',date('Y'));
            $ordersSent->whereYear('order_items.created_at','=',date('Y'));
            $ordersNotInBatch->whereYear('order_items.created_at','=',date('Y'));
        }

        $ordersNotSent = $ordersNotSent->whereNotNull('order_items.batch_id')->count();
        $ordersSent = $ordersSent->whereNotNull('order_items.qty_shipped')->count();
        $ordersNotInBatch = $ordersNotInBatch->where('order_items.batch_id','IS NULL','null')->where('order_items.batch_id','==','0')->count();

        return view('admin.stats.index',[
            'ordersAmount' => $ordersAmount,
            'studentsWithoutOrder' => $studentsWithoutOrder,
            'averagePerOrder' => $averagePerOrder,
            'ordersSum' => $ordersSum,
            'products' => $products,
            'providers' => $providers,
            'productsForTable' => $productsForTable,
            'ordersSent' => $ordersSent,
            'ordersNotSent' => $ordersNotSent,
            'ordersNotInBatch' => $ordersNotInBatch
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
