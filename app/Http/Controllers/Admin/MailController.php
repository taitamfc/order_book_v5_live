<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\MailEditRequest;
use Illuminate\Support\Facades\Hash;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Auth;
use App\Mail;
use App\Student;
use App\Http\Controllers\Controller;

class MailController extends Controller
{
    public function edit()
    {
	    $mail = Mail::findOrFail(1);
        return view('admin.mail.edit', ['mail' => $mail]);
    }

    public function update(MailEditRequest $request)
    {
	    $mail = Mail::findOrFail(1);
        $mailData = $request->only(['content','send_date']);

        $mail->update($mailData);
        Flash::success('De mail is succesvol bijgewerkt / ingesteld.');
        return back();
    }
}
