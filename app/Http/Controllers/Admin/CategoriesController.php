<?php

namespace App\Http\Controllers\Admin;

use App\Batch;
use App\Category;
use App\Group;
use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('category_id','ASC');

        $filter[] = '';

        if(Input::has('category_id') && Input::get('category_id') != '')
        {
            $categories->where('category_id','=',trim(Input::get('category_id')));
            $filter['category_id'] = Input::get('category_id');
        }

        if(Input::has('name') && Input::get('name') != '')
        {
            $categories->where('name','LIKE','%'.trim(Input::get('name')).'%');
            $filter['name'] = trim(Input::get('name'));
        }

        if(Input::has('is_active') && Input::get('is_active') != '')
        {
            $categories->where('is_active','=',trim(Input::get('is_active')));
            $filter['is_active'] = trim(Input::get('is_active'));
        }

        $categories = $categories->paginate(10);
        return view('admin.categories.index',['categories' => $categories,'filter' => $filter]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /* $all_products = Product::selectRaw('products.*,categories.name as category_name')
                                ->leftJoin('categories','categories.category_id','=','products.category_id')
                                ->get(); */                    
        /* return view('admin.categories.create',['all_products' => $all_products]); */

        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $params = $request->all();
        $model_category = new Category();
        $category_add = $model_category->createCategory($params);

        if($category_add){
            return \redirect()->route('categories.show',['id' => $category_add->category_id]);
        }
        else{
            return \redirect()->back()->with('error','Mislukt!'); 
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model_category = new Category();
        $category = Category::where('category_id',$id)->first();
        $list_products = $model_category->getListProductOfCategory($id);
        $all_products = Product::selectRaw('products.*,categories.name as category_name')
                                ->leftJoin('categories','categories.category_id','=','products.category_id')
                                ->get();

        return view('admin.categories.show',['category' => $category,'list_products' => $list_products,'all_products' => $all_products]);
    }


    public function getProductsHasChecked($id)
    {
        $model_category = new Category();
        $list_products = $model_category->getListProductOfCategory($id);
        $all_products = Product::selectRaw('products.*,categories.name as category_name')
                                ->leftJoin('categories','categories.category_id','=','products.category_id')
                                ->get();
        foreach($all_products as $index => $product){
            $product->checked = 0;
            $product->selected = "";
            foreach($list_products as $pro){
                if($pro->id == $product->id){
                    $product->selected = "selected";
                    $product->checked = 1;
                }
            }
        }
        return \response()->json($all_products);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $model_category = new Category();
        $params = $request->all();
        $category = $model_category->updateCategoy($params);
        if($category){
            return \redirect()->back()->with('status','Update succes!');
        }
        else{
            return \redirect()->back()->with('error','Update mislukt!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(!$request->get('category_id')){
            return \redirect()->back()->with('error','Verwijderen mislukt!');
        }
        else{
            $model_category = new Category();
            $delete_category = $model_category->deleteCategory($request->get('category_id'));
            if($delete_category){
                return \redirect()->route('categories.index')->with('status','Verwijderen succes!');;
            }
            else{
                return \redirect()->back()->with('error','Verwijderen mislukt!');
            }
        }
    }

    public function assignProducts(Request $request){
        $data = $request->all();

        $category_id = $data["category_id"];
        $userId = 0;
        $productId = 0;
        if(isset($data["product_id"])){
            $productId = $data["product_id"];
        }
        $arrayProductId = [];
        if(is_string($productId)){
            $arrayProductId = explode(",",$productId);
        }
        $assign_product = false;
        if( $arrayProductId ){
            $mode_product = new Product();
            $assign_product = $mode_product->updateProductAssignCategory($category_id,$arrayProductId);
        }
        

        if($assign_product){
            return \redirect()->back()->with('status','Product koppelen aan categorie succes!');
        }
        else{
            return \redirect()->back()->with('error','Product koppelen aan categorie mislukt!');
        }

    }
}
