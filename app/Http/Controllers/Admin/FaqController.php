<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Auth;
use App\Faq;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    //
    protected $_model_faq = null;
    public function __construct(Faq $faq){
        $this->_model_faq = $faq;
    }

    public function edit(){ 
        $model_faq = $this->_model_faq;
        $faq_info = $model_faq->getFaq();
        return view('admin.faq.edit',['faq' => $faq_info]);
    }

    public function update(Request $request){
        $request->validate([
            'content' => 'required'
        ],[

        ]);
        
        $params = $request->all();
        $model_faq = $this->_model_faq;
        $update_faq = $model_faq->updateFaq($params);
        if($update_faq['error']){
            return back()->with(['error' => $update_faq['error_msg']]);
        }
        else{
            return back()->with('success',$update_faq['error_msg']);
        }
    }
}
