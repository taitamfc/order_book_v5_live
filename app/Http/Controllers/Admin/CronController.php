<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use DB;
use App\Http\Requests;
use App\Mail;
use App\Student;
use App\Http\Controllers\Controller;

class CronController extends Controller
{

	public function send_emails()
	{
		$date = date('Y-m-d');
		$mails = Mail::where('send_date', $date)->get();
		if($mails)
		{
			foreach($mails as $mail)
			{
				//Get all student that don't have an order placed on the given date
				$students = Student::has('orders', '=', 0)
								->where('sent_email_date', '!=', $date)
								->take(10)
								->get();
				foreach($students as $student)
				{
					$email_content = str_replace('{name}', $student->firstname .' '. $student->lastname, $mail->content);

					Mail::raw($email_content, function ($m) use ($student) {
	                    $m->from('noreply@groenhorst.com', 'Groenhorst - Notificatie');
	                    $m->to('TESTBEST4U_'. $student->email, $student->firstname .' '. $student->lastname)
			                    ->subject('Groenhorst - Notificatie');
	                });

					//Mark as sent
					$student = Student::find($student->customer_id);
					$student->sent_email_date = $date;
					$student->save();
				}
			}
		}
	}
}