<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Http\Requests;
use App\Libraries\MagentoApi;
use Flash;

ini_set('max_execution_time', -1);

set_time_limit(3600);

class SyncController extends Controller
{
    public function index() {


    	$obj = new MagentoApi();

		$obj->syncOrdersAndOrderItems();

    }

    public function all() {

    	$obj = new MagentoApi();

		$obj->syncGroups();
		$obj->syncCategories();
		$obj->syncOrdersAndOrderItems();
		
		// longest to run
		$obj->syncProducts();
		$obj->syncStudents();
    		
    }

    public function orders() {
        $obj = new MagentoApi();

        if($obj->syncOrdersAndOrderItems()) {
            Flash::success("Bijgewerkt");
        }

        return redirect()->route('orders.index');
    }

    public function one($name) {
    	$obj = new MagentoApi();

    	$name = 'sync'.ucwords($name);

		$obj->{$name}();
    }

}
