<?php

namespace App\Http\Controllers\Admin\Auth;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Hash;
/* use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers; */

class AuthController extends Controller
{
    /* use AuthenticatesAndRegistersUsers, ThrottlesLogins; */
    
    protected $redirectTo = '/admin';
    protected $guard = "admin";

    public function checkLogin(){
        if (Auth::guard($this->guard)->check()) {
            return true;
        }
        return false;
    }

    public function __construct(){
        
    }

    public function login(){
        if($this->checkLogin()){
            return redirect('admin');   
        }
        return view('admin.auth.login');
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function postLogin (Request $request){
        
        $this->validate($request, [
            'email'    => 'required|email|exists:users|min:5|max:191',
            'password' => 'required|string|min:4|max:255',
        ]);
    
        if(Auth::guard($this->guard)->attempt($request->only('email','password'),$request->get('remember'))){
            //Authentication passed...
            return redirect()
                ->intended(route('admin'));
        }
        //Authentication failed...
        return $this->loginFailed();
    }

    private function loginFailed(){
        return redirect()
            ->back()
            ->withInput()
            ->with('error','Login failed, please try again!');
    }
 
    public function logout()
    {
        //logout the admin...
        Auth::guard($this->guard)->logout();
        return redirect()
            ->route('admin.login')
            ->with('status','Admin has been logged out!');
    }
}
