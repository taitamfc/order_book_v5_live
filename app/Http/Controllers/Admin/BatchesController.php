<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Group;
use App\Product;
use Illuminate\Http\Request;

use DB;
use App\Order;
use App\OrderItem;
use App\Batch;
use App\BatchProduct;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailToFactory;
use App\Jobs\SendEmailToFactory;

/* use mPDF; */
use Flash;
use Mpdf\Mpdf;

use App\Http\Controllers\Controller;
class BatchesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $batches = Batch::with([
            'orders' => function ($query) use ($request) {
                $query->groupBy('order_id');
            },
            'items',
            'batchProducts',

        ]);
        if ($request->id) {
            $batches->where('batch_id', $request->id);
        } elseif (isset($request->is_complete)) {
            $cond = $request->is_complete ? '=' : '!=';
            $batches->whereRaw('(SELECT sum(qty_available) FROM batch_products WHERE batch_id = batches.batch_id) '. $cond .'(SELECT sum(qty_ordered) FROM order_items WHERE batch_id = batches.batch_id)');

            /*
            $batches->whereHas('batchProducts', function ($query) use ($request) {
                if ($request->is_complete) {
                  $query->where('is_complete', 1);
                  $query->orWhere('is_qty_complete', 1);
                } else {
                   $query->where('is_complete', 0);
                   $query->where('is_qty_complete', 0);
                }
            });
            */
        }

        $batches = $batches->paginate(50);

        return view('admin.batches.index', ['batches' => $batches]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function editQty(Request $request)
    {
        $product = BatchProduct::where('batch_id', $request->batch_id)
                    ->where('product_id', $request->product_id)
                    ->first();

        $totalOrdered = OrderItem::where('batch_id', $request->batch_id)
                               ->where('product_id', $request->product_id)
                               ->sum('qty_ordered');

        $product->is_qty_complete = $request->qty == $totalOrdered ? 1 : 0;

        $product->qty_available = $request->qty;
        $product->save();

        return redirect()->back();

    }

    public function setComplete(Request $request)
    {

        if($request->has('isbn')){
            $this->setCompleteByIsbn($request->get('batch_id'), $request->get('isbn'), $request->get('is_complete'));
            die();
        }

        $product = BatchProduct::where('batch_id', $request->get('batch_id'))
                    ->where('product_id', $request->get('product_id'))
                    ->with('product')
                    ->first();

        $old_completed = $product->is_complete;
        $old_qty = $product->qty_available;

        $product->is_complete = ($request->get('is_complete') == '-1') ? null : $request->get('is_complete');
        if($product->product->product_type == '91'){
            $product->is_printed = 1;
        }

        if($request->get('is_complete') == 0) {
            $product->qty_available = $request->get('in_batch');

            if($old_completed !== 0)
                Product::where('product_id', $request->get('product_id'))->increment('reserve_from_stock', $request->get('in_batch'));
        }
        else{

            $product->qty_available = 0;

            if($old_completed === 0){
                Product::where('product_id', $request->get('product_id'))->decrement('reserve_from_stock', (is_null($request->get('in_batch'))) ? $old_qty: $request->get('in_batch'));
            }
        }

        $product->save();
        exit;
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $batch = Batch::findOrFail($id);

//        ----------------------Get orders-------------------------

        $orders = Order::select('orders.*')->where('batch_id','=', $id)
            ->join('students','orders.customer_id','=','students.customer_id')
            ->join('groups','students.group_id','=','customer_group_id');

        if($request->order_id)
        {
            $orders->where('increment_id','LIKE','%'.$request->order_id);
        }

        if($request->student_code)
        {
            $orders->where('students.username', 'like', '%'.$request->student_code.'%');
        }

        if($request->student_name)
        {
            $orders->where('students.firstname','LIKE','%'.$request->student_name.'%');
            $orders->orWhere('students.lastname','LIKE','%'.$request->student_name.'%');
        }

        if($request->group)
        {
            $orders->where('groups.customer_group_id','=',$request->group);
        }

        if($request->from) {
            $orders->whereDate('orders.updated_at', '>=', date('Y-m-d', strtotime(str_replace('/','-',$request->from))));
        }
        if($request->until) {
            $orders->whereDate('orders.updated_at', '<=', date('Y-m-d', strtotime(str_replace('/','-',$request->until))));
        }

        $orders = $orders->get();


//        --------------------------------Get items------------------------------------------
    
        $items = OrderItem::where('order_items.batch_id',$id)
            ->join('products','order_items.product_id','=','products.id')
            ->join('batch_products', function($join){
                $join->on('order_items.product_id', '=', 'batch_products.product_id');
                $join->on('order_items.batch_id', '=', 'batch_products.batch_id');
            });
        if($request->id)
        {
            $items->where('order_items.product_id','=',trim($request->id));

        }
        if($request->category)
        {
            $items->where('products.category_id','=',trim($request->category));
        }

        if($request->is_complete)
        {
            if($request->is_complete == 0){
                $items->whereNull('batch_products.is_complete');
                $items->where('batch_products.is_printed', 0);
            }else{
                $items->Where(function ($query) {
                    $query->whereNotNull('batch_products.is_complete')
                        ->orWhere('batch_products.is_printed', '>', 0);
                });
                //$items->whereNotNull('batch_products.is_complete');
                //$items->where('batch_products.is_printed', '>', 0);
            }
        }

        if($request->qty_available)
        {
            if($request->qty_available){
                $items->where('batch_products.qty_available', '>', 0); //in_batch
            }else{
                $items->where('batch_products.qty_available', '<=', 0);
            }
        }

	    if($request->provider)
	    {
		    $items->where('provider', 'like', '%' . $request->provider . '%');
	    }

        $items->groupBy('unq_ancestor')->select(DB::raw('order_items.*, products.isbn, GROUP_CONCAT(DISTINCT batch_product_id SEPARATOR \',\') batches_id, sum(qty_ordered) as in_batch, products.name, CASE WHEN products.isbn = "" THEN products.id ELSE products.isbn END AS unq_ancestor'))
                          ->with(['product','product.category' ,'batchProducts' => function ($query) use ($id) {
                                $query->where('batch_id', $id);
                          }]);
        $all_products = $items;
        $products = $all_products->get();
        if($request->product)
        {
            $items->where('order_items.product_id','=',trim($request->product));
        }

        $items = $items->get();
        //dd($items);
        foreach ($items as $item){
            if(! empty($item->isbn)){
                $sum_available = BatchProduct::whereRaw('batch_product_id IN ('.$item->batches_id.')')->sum('qty_available');
                $item->available = intval($sum_available);
            }
            else{
                $item->available = intval($item->batchProducts->qty_available);
            }
        }
        //print_r($items);

        $categories = Category::where('is_active','=','1')->get();
        $groups = Group::all();

        $providers = Product::select('provider')->distinct()
            ->where('provider', '!=', '')
            ->join('batch_products', 'batch_products.product_id', '=', 'products.id')
	        ->where('batch_products.batch_id', $id)
            ->get();

        return view('admin.batches.edit', [
            'batch' => $batch,
            'orders' => $orders,
            'items' => $items,
	        'categories' => $categories,
	        'products' => $products,
	        'groups' => $groups,
	        'providers' => $providers
        ]);
    }

    public function pdf($id, $product_id) {
      
        $batch = Batch::findOrFail($id);

        $orders = Order::where('batch_id','=', $id)->join('students','orders.customer_id','=','students.customer_id')
            ->join('groups','students.group_id','=','customer_group_id')->get();

        $items = OrderItem::where('batch_id',$id)
                          ->where('product_id', $product_id)
                          ->select(DB::raw('order_items.*, sum(qty_ordered) as in_batch'))
                          ->with(['product','product.category' ,'batchProducts' => function ($query) use ($id) {
                                $query->where('batch_id', $id);
                          }])
                          ->get();
        $resource = view('admin.batches.pdf', ['batch' => $batch, 'orders' => $orders, 'items' => $items])->render();
        $pdf = new MPDF();
        $pdf->WriteHTML($resource);
        $filename = 'Batch_'.$id.'_'.time();
        $pdf->output($filename.".pdf",'D');

        //return view('admin.batches.pdf');
//        return $resource; 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function max_qty(){
        $result['status'] = '';
            $item = BatchProduct::where('product_id',Input::get('product_id'))->where('batch_product_id','=',Input::get('batch_product_id'))->first();
//            if($item->qty_available < Input::get('in_batch')){
                $item->update(['qty_available' => Input::get('in_batch')]);
                $result['status'] = 'success';
                $result['in_batch'] = Input::get('in_batch');

//            }

        return response()->json( $result );
    }

    public function to_txt_file($id){

        $items = OrderItem::where('order_items.batch_id',$id)
            ->join('products','order_items.product_id','=','products.id')
            ->join('batch_products', function($join){
                $join->on('order_items.product_id', '=', 'batch_products.product_id');
                $join->on('order_items.batch_id', '=', 'batch_products.batch_id');
            })->where('products.product_type_id','=','91');

        $items->select(DB::raw('order_items.*, products.name'))
            ->with(['product','product.category' ,'batchProducts' => function ($query) use ($id) {
                $query->where('batch_id', $id);
            }])->orderBy('order_items.order_id');


        $all_products = $items;
        $products = $all_products->get();

        $grouped = $items->groupBy('order_items.product_id')->select(DB::raw('order_items.*, sum(qty_ordered) as in_batch, products.name'))
            ->with(['product','product.category' ,'batchProducts' => function ($query) use ($id) {
                $query->where('batch_id', $id);
            }])->orderBy('order_items.order_id');
        $products_grouped = $grouped->get();

        foreach ($products_grouped as $product){
            $product->batchProducts->qty_available = $product->in_batch;
            $product->batchProducts->is_complete = 0;
            $product->batchProducts->is_printed = 1;
            $product->batchProducts->save();
        }

//        ========================


        $content = \View::make('admin.batches.txt')->with('products', $products);
        $filename = 'naar_printer.txt';
        $headers = array(
            'Content-Type' => 'text/plain',
            'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            /* 'Content-Length' => sizeof($content), */
        );
        return \Response::make($content, 200, $headers);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*
    public function destroy($id)
    {
        Batch::destroy($id);
        Order::where('batch_id',$id)->update(['batch_id' => '0']);
        OrderItem::where('batch_id',$id)->update(['batch_id' => '0']);
        return redirect()->route('batches.index');
    }
    */

    /**
     * @param Request $request
     */
    public function removePrint(Request $request){
        BatchProduct::where('batch_id', $request->get('batch_id'))
            ->where('product_id', $request->get('product_id'))
            ->update(['is_printed' => 0, 'is_complete' => null, 'qty_available' => 0]);
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function exportBatch(Request $request, $id){
        $batch = Batch::findOrFail($id);

        $orders = Order::where('batch_id','=', $id)->join('students','orders.customer_id','=','students.customer_id')
            ->join('groups','students.group_id','=','customer_group_id')->get();

        $items = OrderItem::where('order_items.batch_id',$id)
            ->join('products','order_items.product_id','=','products.id')
            ->join('batch_products', function($join){
                $join->on('order_items.product_id', '=', 'batch_products.product_id');
                $join->on('order_items.batch_id', '=', 'batch_products.batch_id');
            })->where('products.product_type_id','<>','91')->where(function($query){
                $query->whereNull('batch_products.is_complete')->orWhere('batch_products.is_complete', '<=', 0);
            });


        if($request->has('provider'))
        {
            $items->where('provider', 'like', '%' . $request->get('provider') . '%');
        }

        $items->groupBy('unq_ancestor')->select(DB::raw('order_items.*, sum(qty_ordered) as in_batch, products.name, products.isbn, CASE WHEN products.isbn = "" THEN products.id ELSE products.isbn END AS unq_ancestor'))
            ->with(['product','product.category' ,'batchProducts' => function ($query) use ($id) {
                $query->where('batch_id', $id);
            }]);

        $items = $items->get();

        foreach ($items as $key=>$item){
            if(! empty($item->isbn)){
                $this->setCompleteByIsbn($item->batch_id, $item->isbn, 1);
            }
            else{
                $old_complete = $item->batchProducts->is_complete;
                $item->batchProducts->qty_available = 0;
                $item->batchProducts->is_complete = 1;
                $item->batchProducts->save();

                if($old_complete === 0)
                    Product::where('product_id', $item->product_id)->decrement('reserve_from_stock', $item->in_batch);
            }
        }

        $resource = view('admin.batches.pdf', ['batch' => $batch, 'orders' => $orders, 'items' => $items])->render();
        /* $mpdf = new \Mpdf\Mpdf(); */
        $pdf = new \Mpdf\Mpdf();
        $pdf->WriteHTML($resource);
        $filename = 'Batch_'.$id.'_'.time();
        $pdf->output($filename.".pdf",'D');
    }

    protected function setCompleteByIsbn($batch_id, $isbn, $complete){
        //CHECK ME
        $batches = BatchProduct::where('products.isbn', $isbn)->where('batch_products.batch_id', $batch_id)
            ->join('products','batch_products.product_id','=','products.id')
            ->join('order_items', function($join){
                $join->on('order_items.product_id', '=', 'batch_products.product_id');
                $join->on('order_items.batch_id', '=', 'batch_products.batch_id');
            })
            ->select(DB::raw('batch_products.*, sum(qty_ordered) as in_batch, product_type_id'))
            ->groupBy('batch_products.product_id')
            ->get();

        foreach ($batches as $batch){
            $old_completed = $batch->is_complete;
            $batch->is_complete = ($complete == '-1') ? null : $complete;
            if($batch->product_type_id == '91'){
                $batch->is_printed = 1;
            }

            if($complete == 0) {
                $batch->qty_available = $batch->in_batch;

                if($old_completed !== 0)
                    Product::where('id', $batch->product_id)->increment('reserve_from_stock', $batch->in_batch);
            }
            else{

                $batch->qty_available = 0;

                if($old_completed === 0){
                    Product::where('id', $batch->product_id)->decrement('reserve_from_stock', $batch->in_batch);
                }
            }
            $batch->save();
        }
    }

    /**
     * send email to factory
     * Param: product_id
     * 
     */
    public function sendEmailToFactory(Request $request){
        if(!$request->batch_ids){
            Flash::error('product niet gevonden!');
            return back();
        }

        $batch_ids = explode(",", $request->batch_ids);

        $product_commer = Product::selectRaw('products.*,qty_available')
                            ->join('batch_products','batch_products.product_id','=','products.id')
                            ->where('products.category_id',4)
                            ->whereIn('batch_products.batch_id',$batch_ids)
                            ->get();

        $emails = $product_commer->pluck('order_email_address')->unique();
        
        $email_arr = array();
        foreach($emails as $email){
            $lst_products = array();
            foreach($product_commer as $pro){
                if($email == $pro->order_email_address){
                    $lst_products[] = $pro;
                }
            }
            $email_arr[$email] = $lst_products;
        }

        /* foreach($email_arr as $email => $listProducts){
            $data_email = $email;
            $data_products = $listProducts;
        } */

        try
        {
            SendEmailToFactory::dispatch($batch_ids,$email_arr);
            /* Mail::to($product->order_email_address)->send(new EmailToFactory($product)); */
            Flash::success("Stuur e-mail succes!");
            return back();
        }
        catch(Exception $e){
            Flash::error("e-mail verzenden mislukt!");
            return back();
        }
        
    }
}
