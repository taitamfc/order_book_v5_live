<?php

namespace App\Http\Controllers\Admin;

use App\Group;
use App\Libraries\MagentoApi;
use App\Product;
use App\Student;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use App\Http\Controllers\Controller;
class StudentsController extends Controller
{
    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $students = Student::with('group', 'order_count')->where('is_active','1');

        $filter = [];

        if(Input::get('student_id') && Input::get('student_id') != '')
        {
            $students->where('username', 'like','%'.trim(Input::get('student_id')).'%');

            $filter['student_id'] =  Input::get('student_id');
        }

        if(Input::get('student_name') && Input::get('student_name') != '')
        {
            $students->where(DB::raw('CONCAT(firstname, " ", lastname)'),'like','%'.trim(Input::get('student_name')).'%');//->orWhere('lastname','like','%'.trim(Input::get('student_name')).'%');

            $filter['student_name'] =  Input::get('student_name');
        }

        if(Input::get('group') && Input::get('group') != 'null')
        {
           $students->where('group_id','=',trim(Input::get('group')));
            $filter['group'] =  Input::get('group');
        }

        if(Input::has('product') && Input::get('product')  != 'null')
        {

            $students->whereExists(function($query){

                $query->select(DB::raw(1))->from('orders')->whereRaw('students.customer_id = orders.customer_id')
                    ->join('order_items','orders.order_id','=','order_items.order_id')->where('order_items.product_id','=',trim(Input::get('product')));

            });

            $filter['product'] =  Input::get('product') ;

        }

        if(Input::get('have_orders')  && Input::get('have_orders') == '1')
        {

            $students->whereExists(function($query){
                $query->select(DB::raw(1))->from('orders')->whereRaw('students.customer_id = orders.customer_id');
            });
            $filter['have_orders'] =  Input::get('have_orders');


        }elseif(Input::has('have_orders') && Input::get('have_orders') == 0 && Input::get('have_orders') != 'null')
        {
            $students->whereNotExists(function($query){
                $query->select(DB::raw(1))->from('orders')->whereRaw('students.customer_id = orders.customer_id');
            });

            $filter['have_orders'] =  Input::get('have_orders');

        }

        $students = $students->orderBy('students.customer_id','ASC')->paginate(100);

//        For Groups search filter
        $groups = Group::all();
//       Get products for search filter
        $products = Product::all();

        return view('admin.students.index',['students' => $students,'groups' => $groups,'products' => $products,'filter' => $filter]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

//        $customers = new SoapClient('http://totdrukwerk.best4uconcept.nl/api/soap/?wsdl');
//
//        $session = $customers->login('beheer', '2tcx72mod2y4');
//
//        $result = $customers->call($session, 'customer.list');
//
////        Import Groups
//
////        $studentGroup = $customers->call($session, 'customer_group.list');
////           foreach($studentGroup as $group)
////           {
////               Group::create([
////                   'group_id' => $group['customer_group_id'],
////                   'group_name' => $group['customer_group_code'],
////               ]);
////           }
////
////            dd($studentGroup);
//
//
//        foreach($result as $student)
//        {
//
//
//            $studentInfo =  $customers->call($session, 'customer_address.list',$student['customer_id']);
//            $studentGroup = $customers->call($session, 'customer_group.list');
//
//            if(isset($studentInfo[0]['region'])){ $state = $studentInfo[0]['region'];}else {$state = '';}
//            if(isset($studentInfo[0]['telephone'])){ $phone = $studentInfo[0]['telephone'];}else {$phone = '';}
//
//
//            Student::FirstOrCreate([
//                'student_id' => $student['customer_id'],
//                'name' => $studentInfo[0]['firstname'].' '.$studentInfo[0]['lastname'],
//                'group_id' => $studentGroup[$student['group_id']]['customer_group_id'],
//                'email' => $student['email'],
//                'phone' => $phone,
//                'made_order' => '1',
//                'zip' => $studentInfo[0]['postcode'],
//                'country' => $studentInfo[0]['country_id'],
//                'state' => $state,
//
//            ]);
//
//
//        }
//
//        $customers->endSession($session);

        $groups = Group::where('customer_group_code', '<>', 'NOT LOGGED IN')->get();

        return view('admin.students.create', ['groups' => $groups]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|min:4|regex:/^\d+$/',
            'email' => 'required|email|max:255|unique:students,email',
            'password' => 'required|min:6|confirmed',
            'firstname' => 'required',
            'lastname' => 'required',
            'group_id' => 'required',
            'street' => 'required',
            'postcode' => 'required',
            'telephone' => 'required',
            'city' => 'required',
        ]);

        $magentoApi = new MagentoApi();

        $user_data = $request->only(['username', 'firstname', 'middlename', 'lastname', 'email', 'group_id']);

        $customer = array_merge($user_data, [
            'taxvat'    => $request->get('bank_account'),
            'password'  => $request->get('password'),
            'store_id'   => 1,
            'website_id'   => 1,
        ]);

        $user_id = $magentoApi->createCustomer($customer);
        if(intval($user_id) > 0){

            $user_address = [
                'customerId'    => $user_id,
                'addressdata'   => [
                    'firstname'    => $request->get('firstname'),
                    'lastname'    => $request->get('lastname'),
                    'middlename'    => $request->get('middlename'),
                    'street'    => [$request->get('street')],
                    'postcode'    => $request->get('postcode'),
                    'telephone'    => $request->get('telephone'),
                    'city'    => $request->get('city'),
                    'country_id'    => 'NL',
                    'is_default_billing' => true
                ]
            ];

            $magentoApi->createCustomerAddress($user_address);

            $user_data = array_merge($user_data, [
                'customer_id'    => 124324,
                'bank_account'  => $request->get('bank_account'),
                'street'  => $request->get('street'),
                'postcode'  => $request->get('postcode'),
                'city'  => $request->get('city'),
                'telephone'  => $request->get('telephone'),
                'password' => Hash::make($request->get('password'))
            ]);

            $student = new Student($user_data);

            if($student->save()){
                $request->session()->flash('success', 'De student is succesvol aangemaakt.');
            }
            else{
                $request->session()->flash('error', 'De student kon niet worden aangemaakt.');
            }

        }
        else{

            $errors = [
                'Username already exists'   => 'Er is reeds een student met dit nummer aangemaakt.'
            ];
            $request->session()->flash('error', (array_key_exists($user_id, $errors)) ? $errors[$user_id] : $user_id);
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order_items = DB::table('orders')
            ->join('order_items','orders.order_id','=','order_items.order_id')
            ->join('products','order_items.product_id','=','products.id')
            ->join('categories','categories.category_id','=','products.category_id')
            ->select('orders.increment_id', 'order_items.*', 'categories.name AS category', 'products.category_id', 'products.name',
                        DB::raw('IF(qty_ordered = qty_shipped, 1, 0) AS all_shipped'))
            ->where('orders.customer_id', $id)
            ->get()->toArray();

        $order_items = array_reduce($order_items, function($res, $item){
            $res[$item->increment_id][$item->category][] = $item;
            return $res;
        });

        $student = Student::where('customer_id',$id)->first();

        $groups = Group::all();

        return view('admin.students.show',[
            'student' => $student,
            'orders_categorized' => $order_items,
            'groups' => $groups,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(! Auth::guard('admin')->user()->isAdmin())
            return redirect()->back();

        $student = Student::findOrFail($id);
        if($request->has('group_id') && $student->group_id != $request->get('group_id')){
            $student->group_id = $request->get('group_id');
            if($student->save()){
                $magento = new MagentoApi();
                $update = ['group_id' => $student->group_id];
                $res = $magento->updateCustomer(3736, $update);
                $request->session()->flash('success', 'De student is succesvol bijgewerkt.');
            }
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
