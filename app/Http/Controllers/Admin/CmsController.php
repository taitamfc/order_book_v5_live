<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\CmsInterface;

class CmsController extends Controller
{
    //
    private $_cmsRepository = null;
    public function __construct(CmsInterface $cmsRepository){
        $this->_cmsRepository = $cmsRepository;
    }

    public function index(){
        $cmsList = $this->_cmsRepository->getAll();
        return View('admin.cms.index',['cmsList' => $cmsList]);
    }

    public function show($id){
        $cms = $this->_cmsRepository->getById($id);
        return View('admin.cms.detail',['cms' => $cms]);
    }

    public function create(){
        return View('admin.cms.create');
    }

    public function store(Request $request){
        $requestData = $request->all();
        // modify somehow
        $requestData['slug'] = str_slug($requestData['slug']);
        $request->replace($requestData);

        $request->validate([
            'title' => 'required',
            'slug' => 'required|unique:cms'
        ],[

        ]);

        $params = $request->all();
        $params['title'] = strip_tags($params['title']);
        $params['slug'] = str_slug($params['slug']);
        $params['content'] = strip_tags($params['content']);

        $result = $this->_cmsRepository->storeCms($params);
        
        return \redirect()->route('cms.index');
    }

    public function update(Request $request){
        $requestData = $request->all();
        // modify somehow
        $requestData['slug'] = str_slug($requestData['slug']);
        $request->replace($requestData);
        
        $cms = $this->_cmsRepository->checkSlug($requestData['id'],$requestData['slug']);
        if(count($cms) > 0){
            return back()->withErrors(['slug is al in gebruik.']);
        }

        $request->validate([
            'id' => 'required',
            'title' => 'required'
        ],[

        ]);

        $params = $request->all();
        $params['title'] = strip_tags($params['title']);
        $params['slug'] = str_slug($params['slug']);
        $params['content'] = strip_tags($params['content']);

        $result = $this->_cmsRepository->updateCms($params);

        return \redirect()->route('cms.show',['id' => $params['id']]);
    }
}
