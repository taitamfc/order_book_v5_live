<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ProfileEditRequest;
use Illuminate\Support\Facades\Hash;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function edit()
    {
        return view('admin.profile.edit');
    }

    public function update(ProfileEditRequest $request)
    {
        $user = Auth::guard('admin')->user();
        $profileData = $request->only(['name','email']);

        if($request->new_password)
        {
            $user->password = Hash::make($request->new_password);
        }

        $user->update($profileData);
        Flash::success('Profiel is bijgewerkt');
        return back();


    }
}
