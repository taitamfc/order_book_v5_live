<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Auth;
use App\Newsletter;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class NewsletterController extends Controller
{
    //
    protected $_model_newsletter = null;
    public function __construct(Newsletter $newsletter){
        $this->_model_newsletter = $newsletter;
    }

    public function edit(){ 
        $model_newsletter = $this->_model_newsletter;
        $newsletter_info = $model_newsletter->getNewsletter();
        return view('admin.newsletter.edit',['newsletter' => $newsletter_info]);
    }

    public function update(Request $request){
        $request->validate([
            'content' => 'required'
        ],[

        ]);
        
        $params = $request->all();
        $model_newsletter = $this->_model_newsletter;
        $update_newsletter = $model_newsletter->updateNewsletter($params);
        if($update_newsletter['error']){
            return back()->with(['error' => $update_newsletter['error_msg']]);
        }
        else{
            return back()->with('success',$update_newsletter['error_msg']);
        }
    }
}
