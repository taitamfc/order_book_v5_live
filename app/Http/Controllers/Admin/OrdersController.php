<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Batch;
use DB;
use App\Group;
use App\Order;
use App\OrderItem;
use App\Product;
use App\BatchProduct;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Flash;

class OrdersController extends Controller
{

    public function __construct()
    {
        /* $this->middleware('role:admin')->except('index'); */
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        /* if(Auth::user()->IsGuest())
            return redirect()->route('shipments.index');

        if(Auth::user()->isStudent()){
            return redirect()->route('studenten.index');
        } */

        if ($request->order_ids && $request->get('batch') == 1) {
            $batch = new Batch();
            $batch->save();

            foreach ($request->order_ids as $id) {

                Order::where('order_id','=',$id)->update(['batch_id' => $batch->batch_id]);
                OrderItem::where('order_id','=',$id)->update(['batch_id' => $batch->batch_id]);
            }

            $products = OrderItem::groupBy('product_id')->where('batch_id', $batch->batch_id)->orderBy('created_at', 'DESC')->get();

            foreach ($products as $product) {
                BatchProduct::create(['batch_id' => $batch->batch_id, 'product_id' => $product->product_id]);
            }
            Flash::success("De batch, met ID '".$batch->batch_id."', is succesvol aangemaakt.");

            return redirect()->route('orders.index');
        }

        $orders = Order::where('batch_id','0')
			            ->join('students','orders.customer_id','=','students.customer_id')
			            ->join('groups','students.group_id','=','customer_group_id');

                        
        if(Input::has('order_id') || Input::has('studentcode') || Input::has('naam') || Input::has('customer_group_code') || Input::has('ordersupdate') || Input::has('orderStatus'))
        {
            if(Input::has('order_id'))
            {
                $orders = $orders->orderBy('order_id', Input::get('order_id'));
            }
	        if(Input::has('studentcode'))
            {
                $orders = $orders->orderBy('students.username', Input::get('studentcode'));
            }
	        if(Input::has('naam'))
            {
                $orders = $orders->orderBy('orders.firstname', Input::get('naam'));
                //$orders = $orders->orderBy('orders.lastname', Input::get('naam'));
            }
	        if(Input::has('customer_group_code'))
            {
                $orders = $orders->orderBy('customer_group_code', Input::get('customer_group_code'));
            }
	        if(Input::has('ordersupdate'))
            {
                $orders = $orders->orderBy('created_at', Input::get('ordersupdate'));
            }
        }else{
            $orders = $orders->orderBy('orders.created_at','ASC');
        }

        $filter = [];

        if($request->status)
        {
            $orders->where('status','=',$request->status);
            $filter['status'] = $request->status;
        }
        else{
            $orders->where('status','=','complete');
        }

        if($request->group)
        {
           $orders->where('groups.customer_group_id','=',$request->group);
            $filter['group'] = $request->group;
        }

        if($request->from){
            $orders->whereDate('orders.created_at','>=',date('Y-m-d', strtotime(str_replace('/', '-', $request->from))));
            $filter['from'] = $request->from;
        }
        if($request->until){
            $orders->whereDate('orders.created_at','<=',date('Y-m-d',strtotime(str_replace('/', '-', $request->until))));
            $filter['until'] = $request->until;
        }

        if ($request->id) {
            $orders->where('increment_id', 'like', '%'.$request->id.'%');
            $filter['id'] = $request->id;
        
        } elseif ($request->username) {

            $orders->whereHas('student', function ($query) use ($request) {
                $query->where('username', 'like', '%'.$request->username.'%');
            });

            $filter['username'] = $request->username;

        } elseif ($request->group_id) {

            $orders->whereHas('student', function ($query) use ($request) {
                $query->where('group_id', $request->group_id);
            });

            $filter['group_id'] = $request->group_id;

        } elseif ($request->name) {

            $orders->where('orders.firstname', 'like', '%'.$request->name.'%');
            $orders->orWhere('orders.lastname', 'like', '%'.$request->name.'%');
            $filter['name'] = $request->name;

        }

        if($request->per_page)
        {
            $perPage = $request->per_page;
        }else
        {
            $perPage = 100;
        }

        $orders = $orders->select('orders.*','orders.updated_at as ordersupdate', 'groups.customer_group_code')->paginate($perPage);

        Carbon::setLocale(config('app.locale'));
        $lastDbTableUpdate = Order::orderBy('updated_at','desc')->value('updated_at');

        $lastDbTableUpdate = new Carbon($lastDbTableUpdate);
        $lastDbTableUpdate = $lastDbTableUpdate->format("d / m / Y - H:i");

        $groups = Group::all();

        return view('admin.orders.index', ['orders' => $orders, 'groups' => $groups,'filter' => $filter, 'lastDbTableUpdate' => $lastDbTableUpdate]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);

        $order->items = $order->items->groupBy('product.category.slug');

        return view('admin.orders.show', ['order' => $order]);
    }


	public function update(Request $request)
    {
        $order = Order::find($request->id);
        //$order->username = $request->username;

        if($request->status){
            $order->status = $request->status;
        }
        
        $order->firstname = $request->firstname;
        $order->middlename = $request->middlename;
        $order->lastname = $request->lastname;
        $order->email = $request->email;
        $order->telephone = $request->telephone;
        $order->street = $request->street;
        $order->postcode = $request->postcode;
        $order->city = $request->city;
        if($order->save()){
            Flash::success("Succes!");
        }

        return redirect()->route('orders.show', $request->id);
    }
}
