<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Student;
use App\Order;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    //
    protected $_model_student = null;
    protected $_model_order = null;
    public function __construct(Student $student, Order $order){
        $this->_model_student = $student;
        $this->_model_order = $order;
    }

    public function info(){ 
        $model_student = $this->_model_student;
        $student_info = $model_student->getStudentInfo();
        return view('frontend.student.account',['student' => $student_info]);
    }

    public function editInfo(Request $request){
        $change_password = $request->get("change_password");
        if(!$change_password){
            $request->validate([
                'customer_id' => 'required',
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required',
                'email' => 'email',
                'telephone' => 'required',
                'bank_account' => 'required',
                'street' => 'required',
                'city' => 'required',
                'postcode' => 'required',
            ],[
    
            ]);
        }else{
            $request->validate([
                'customer_id' => 'required',
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required',
                'telephone' => 'required',
                'bank_account' => 'required',
                'street' => 'required',
                'city' => 'required',
                'current_password' => 'required',
                'password' => 'required',
                'password' => 'confirmed',
                'password_confirmation' => 'required',
            ],[
    
            ]);
        }
        
        $params = $request->all();
        $params['change_password'] = $change_password;
        $model_student = $this->_model_student;
        $update_student = $model_student->updateStudent($params);
        if($update_student['error']){
            return back()->with(['error' => $update_student['error_msg']]);
        }
        else{
            return back()->with('success',$update_student['error_msg']);
        }
    }

    public function orderHistories(){
        $model_order = $this->_model_order;
        $orders = $model_order->getOrdersOfStudent();
        return view('frontend.student.order-histories',['orders' => $orders]);
    }

    public function orderInfo($order_id){
        if(!$order_id){
            return back()->with(['error' => 'Bestellingen niet gevonden!']); 
        }
        $model_order = $this->_model_order;
        $order_info = $model_order->getOrder($order_id);
        return view('frontend.student.order-info',['order' => $order_info]);
    }
    
    public function statistics(){
        return view('frontend.student.statistics');
    }
}
