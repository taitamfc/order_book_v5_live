<?php

namespace App\Http\Controllers\Helpers;


use Mollie\Laravel\Facades\Mollie;
use App\MolliePayment;
use App\Cart;
use App\CartItems;
use App\Order;
use App\OrderItem;
use App\Batch;
use App\BatchProduct;
use DB;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class MolliePaymentHelpers extends Controller
{
    //
    public function preparePayment($params)
    {   
        DB::beginTransaction();
        try{
            $grand_total = 0;
            $shipping_fee = 10;

            $current_date_time = Carbon::now();
            // create order
            $model_order = new Order();
            $model_cart = new Cart();
            $cart_info = $model_cart->getCartInfo();
            
            $order_add = new Order();
            $order_add->status = 'pending';
            $order_add->customer_id = $cart_info->student_id;
            $order_add->grand_total = 0;
            $order_add->bank_account = $cart_info->bank_account;
            $order_add->firstname = $cart_info->firstname;
            $order_add->middlename = $cart_info->middlename;
            $order_add->lastname = $cart_info->lastname;
            $order_add->email = $cart_info->email;
            $order_add->telephone = $cart_info->telephone;
            $order_add->city = $cart_info->city;
            $order_add->street = $cart_info->street;
            $order_add->postcode = $cart_info->postcode;
            $order_add->created_at = $current_date_time;
            $order_add->updated_at = $current_date_time;
            $order_add->save();

            $order_items = [];
            foreach($cart_info->items as $item){
                $order_item_add = new OrderItem();
                $grand_total += $item->price;
                $order_add->grand_total += $item->price;
                $order_item_add->order_id = $order_add->order_id;
                $order_item_add->product_id = $item->id;
                $order_item_add->qty_ordered = 1;
                $order_item_add->price = $item->price;
                $order_item_add->save();
                $order_items[] = $order_item_add;
            }

            $order_add->grand_total += 10;
            $order_add->save();

            // delete cart
            $delete_cart_items = CartItems::where('cart_id',$cart_info->id)->delete();
            $delete_cart = Cart::where('id',$cart_info->id)->delete();
            // end create order

            $grand_total = \strval(number_format((float)$order_add->grand_total, 2, '.', ''));
            $payment = Mollie::api()->payments()->create([
                'amount' => [
                    'currency' => 'EUR',
                    'value' => $grand_total,
                ],
                "metadata" => [
                    "order_id" => \strval($order_add->order_id),
                ],
                'method' => $params['method'],
                'description' => "[OrderId '.$order_add->order_id.']",
                'webhookUrl' => route('student.webhooks.mollie'),
                'redirectUrl' => route('student.order.success',['id' => $order_add->order_id]),
            ]);

            $payment = Mollie::api()->payments()->get($payment->id);

            $mollie_model = new MolliePayment();
            $mollie_model->payment_id = $payment->id;
            $mollie_model->order_id = $payment->metadata->order_id;
            $mollie_model->method = $payment->method;
            $mollie_model->status = $payment->status;
            $mollie_model->created_at = $payment->createdAt;
            $mollie_model->paid_at = $payment->paidAt;
            $mollie_model->canceled_at = $payment->canceledAt;
            $mollie_model->expires_at = $payment->expiresAt;
            $mollie_model->failed_at = $payment->failedAt;
            $mollie_model->profileId = $payment->profileId;
            $mollie_model->sequenceType = $payment->sequenceType;
            $mollie_model->amount = $payment->amount->value;
            $mollie_model->currency = $payment->amount->currency;
            $mollie_model->link_checkout = $payment->_links->checkout->href;
            $mollie_model->link_get = $payment->_links->self->href;
            
            if($mollie_model->save()){
                DB::commit();
                return $payment;
            }else{
                DB::rollBack();
                return false;
            }
        }
        catch(ModelNotFoundException $exception){
            DB::rollBack();
            return false;
        }
    }
}
