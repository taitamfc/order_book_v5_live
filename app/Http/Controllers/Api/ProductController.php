<?php

namespace App\Http\Controllers\Api;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ProductRepository;
use App\Repositories\RepositoryInterface;
class ProductController extends Controller{

    /**
     * Repository Product
     *
     * @var [type]
     */
    protected $_productRepository = null;

    public function __construct(RepositoryInterface $productRepository){
        $this->_productRepository = $productRepository;
    }
    /**
     * Function get All Product
     *
     * @return void
     */
    public function show(){
        $data = $this->_productRepository->getAll();
        return response()->json($data);
    }

    public function getFeature(){
        $data = $this->_productRepository->getFeatureProduct();
        return response()->json($data);
    }

    /**
     * function assign products from table products to table groups_products
     *
     * @return void
     */
    public function assignToGroup(){
        $data = $this->_productRepository->assignToGroupProductTable();
        return response()->json($data);
    }

}