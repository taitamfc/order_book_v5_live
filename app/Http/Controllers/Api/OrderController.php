<?php

namespace App\Http\Controllers\Api;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\OrderRepository;
use App\Repositories\RepositoryInterface;
class OrderController extends Controller{

    /**
     * Repository Product
     *
     * @var [type]
     */
    protected $_orderRepository = null;

    public function __construct(RepositoryInterface $orderRepository){
        $this->_orderRepository = $orderRepository;
    }
    /**
     * Function get All Product
     *
     * @return void
     */
    public function show(){
        $data = $this->_orderRepository->getAll();
        return response()->json($data);
    }
}