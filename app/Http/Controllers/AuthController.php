<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Newsletter;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class AuthController extends Controller
{
    //
    
    protected $redirectTo = '/';
    protected $guard = "student";

    public function checkLogin(){
        if (Auth::guard($this->guard)->check()) {
            return true;
        }
        return false;
    }

    public function __construct(){
        
    }

    public function login(){
        if($this->checkLogin()){
            return redirect('/');   
        }

        $news = Newsletter::where('is_enabled','=',1)->first();
        return view('frontend.auth.login',['news' => $news]);
    }


    public function postLogin (Request $request){
        
        $this->validate($request, [
            'username'    => 'required|max:191',
            'password' => 'required|string|min:4|max:255',
        ]);
    
        if(Auth::guard($this->guard)->attempt($request->only('username','password'),$request->get('remember'))){
            //Authentication passed...
            return redirect()
                ->intended(route('student.home'));
        }
        //Authentication failed...
        return $this->loginFailed();
    }

    private function loginFailed(){
        return redirect()
            ->back()
            ->withInput()
            ->with('error','Aanmelden mislukt. Probeer het alstublieft opnieuw!');
    }
 
    public function logout()
    {
        Auth::guard($this->guard)->logout();
        return redirect()
            ->route('student.login')
            ->with('status','U bent uitgelogd!');
    }
}
